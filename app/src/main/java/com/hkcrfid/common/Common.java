package com.hkcrfid.common;
import android.os.Environment;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
public class Common
{
    public static String appUrl="http://192.168.43.85:8080/DemoTest/";
    public static String rootFoleder= Environment.getExternalStorageDirectory().toString()+"/Jewerly12";
    public static String pleaseCheckNwConnection="Please check network connection!";
    public static String appPreference="Jewerley";
    public static String appPackage="com.hkcrfid.jewerly";
    public static final int SCAN_INTERVAL = 60;
    public static String getUtcFormat(Date date)
    {
        SimpleDateFormat utc = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String dateUtc=utc.format(date);
        if(dateUtc!=null && dateUtc.length()<15)
        {
            dateUtc=dateUtc+"000";
        }else if(dateUtc!=null && dateUtc.length()<16)
        {
            dateUtc=dateUtc+"00";
        }else if(dateUtc!=null && dateUtc.length()<17)
        {
            dateUtc=dateUtc+"0";
        }
        return "DT"+dateUtc;
    }

/*
    public static String getUtcFormat1(Date date)
    {
        SimpleDateFormat utc = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        String dateUtc=utc.format(date);
        if(dateUtc!=null && dateUtc.length()<15)
        {
            dateUtc=dateUtc+"000";
        }else if(dateUtc!=null && dateUtc.length()<16)
        {
            dateUtc=dateUtc+"00";
        }else if(dateUtc!=null && dateUtc.length()<17)
        {
            dateUtc=dateUtc+"0";
        }
        return utc.format(new Date());
    }*/

    public static Date getDateFromUtcFormat(String dateUtcFormat){
        SimpleDateFormat utc = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        Date date=null;
        try
        {
          //  System.out.println("dateUtcFormat"+dateUtcFormat);
            if(dateUtcFormat!=null)
                date=utc.parse(dateUtcFormat.replaceAll("DT", ""));
        } catch (ParseException e)
        {
            e.printStackTrace();
        }
       // System.out.println("date"+date);
        return date;
    }
}
