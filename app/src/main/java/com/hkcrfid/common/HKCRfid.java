package com.hkcrfid.common;

import com.hkcrfid.jewerly.UfhData;

/**
 * Created by Shree on 10/10/2017.
 */

public class HKCRfid {
    static int SCAN_INTERVAL = 10;
    static boolean Scanflag = false;
    public static void openDevice() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                if (!UfhData.isDeviceOpen()) {
                    int tty_speed = 57600;
                    byte addr = (byte) 0xff;
                    int result = UfhData.UhfGetData.    OpenUhf(tty_speed, addr, 4, 0, null);
                    if (result == 0) {
                        UfhData.UhfGetData.GetUhfInfo();

                    }
                }
            }
        }).start();
    }


    public  static synchronized String[] startReading()
    {
        UfhData.scanResult6cEpc.put("rfid",null);
        UfhData.read6c();

        String[] list = UfhData.scanResult6cEpc.get("rfid");
/*
        if(list!=null && list.length>0) {

            for (int i = 0; i < list.length; i++)
            {
                System.out.println("epc" + list[i]);
            }
        }
        */
        return list;
    }


    public static void closeDevice()
    {
        System.out.println("Check isOpen device For Close connection.");
        if(UfhData.isDeviceOpen())
        {
            System.out.println("Rfid Close device");
            UfhData.UhfGetData.CloseUhf();
        }
    }
    public static void startSound()
    {
        UfhData.Set_sound(true);
    }
    public static void stopSound()
    {
        UfhData.Set_sound(false);
    }
}