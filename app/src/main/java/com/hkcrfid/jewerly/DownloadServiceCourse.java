package com.hkcrfid.jewerly;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.provider.MediaStore;
import android.view.View;

import com.hkcrfid.common.Common;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;


public class DownloadServiceCourse extends Service implements DownloadResultReceiver.Receiver{
   
   /** indicates how to behave if the service is killed */
   int mStartMode;
   String counter="image"+(new java.util.Date().getTime());
   /** interface for clients that bind */
   IBinder mBinder;     
   private DownloadResultReceiver mReceiver;
   private BroadcastReceiver bReceiver;
   public static Activity activity=null;
   /** indicates whether onRebind should be used */
   boolean mAllowRebind;
  
   /** Called when the service is being created. */
   @Override
   public void onCreate() {
System.out.println("DownloadServiceCourse===>>");
       Common.appPackage=getApplicationContext().getPackageName();
       System.out.println("Common.appPackage"+Common.appPackage);
	   System.out.println("onCreate serivce ");
	   mReceiver = new DownloadResultReceiver(new Handler());
       mReceiver.setReceiver(this);
       
       final String url = "All_Contets";
       
     
   }
   
   

   
   
   @Override
   public void onReceiveResult(int resultCode, Bundle resultData) {
       System.out.println("onReceiveResult===>>"+resultCode);
       switch (resultCode) {
           case DownloadService.STATUS_RUNNING:
            String[] results = resultData.getStringArray("result");
           	Intent i = new Intent(Common.appPackage+".download").putExtra("result",results);
    		this.sendBroadcast(i);
         		   
               break;
           case DownloadService.STATUS_FINISHED:
               /* Hide progress & extract result from bundle */
               

                results = resultData.getStringArray("result");
                i = new Intent(Common.appPackage+".download").putExtra("result",results);
        		this.sendBroadcast(i);
             	

               break;
           case DownloadService.STATUS_ERROR:
               /* Handle the error */
               
        	 results = resultData.getStringArray("result");
             i = new Intent(Common.appPackage+".download").putExtra("result",results);
       		this.sendBroadcast(i);
               break;
       }
   }

   /** The service is starting, due to a call to startService() */
   @Override
   public int onStartCommand(Intent intent, int flags, int startId) {
	   
	   System.out.println("onStartCommand===>>");
	   IntentFilter intentFilter = new IntentFilter(
 				Common.appPackage+".synchronization");
  
 		bReceiver = new BroadcastReceiver() {
  
 			@Override
 			public void onReceive(Context context, Intent intent) {
 				//extract our message from intent
 				String action_type =null;
 				System.out.println("BroadcastReceiver"+intent.getAction());
 				if(intent.hasExtra("action_type")) {
                    action_type = intent.getStringExtra("action_type");

                }
 				System.out.println("action_type===>>"+action_type);


 				if(action_type.equals("image")&& activity!=null)
                {

                    View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);

                    Bitmap  bm= getScreenShot(rootView);
                    counter="image"+(new java.util.Date().getTime());
                    File file =store(bm,counter);
                    shareImage(file);

                }else {
                    Intent intentMy = new Intent(Intent.ACTION_SYNC, null, context, DownloadService.class);
                    intentMy.putExtra("action_type", action_type);
                    intentMy.putExtra("receiver", mReceiver);
                    intentMy.putExtra("requestId", 101);
                    startService(intentMy);

                }
  
 			}
 		};
 		//registering our receiver
 		System.out.println("bReceiver serivce ");
 		this.registerReceiver(bReceiver, intentFilter);
      return mStartMode;
   }

   public void unEegister()
   {
	   System.out.println("bReceiver unEegister serivce ==>>>>");
	   if(bReceiver!=null )
	   {
	   this.unregisterReceiver(bReceiver);
	   bReceiver=null;
	   }
   }
   /** A client is binding to the service with bindService() */
   @Override
   public IBinder onBind(Intent intent) {
	   System.out.println("onBind===>>");
      return mBinder;
   }

   /** Called when all clients have unbound with unbindService() */
   @Override
   public boolean onUnbind(Intent intent) {
	   System.out.println("onUnbind===>>");
      return mAllowRebind;
   }

   /** Called when a client is binding to the service with bindService()*/
   @Override
   public void onRebind(Intent intent) {
	   System.out.println("onRebind===>>");
   }
   
   
   

   /** Called when The service is no longer used and is being destroyed */
   @Override
   public void onDestroy() {

	  
	   unEegister();
	   System.out.println("calll stop self");
	   this.stopSelf();
	   super.onDestroy();
	   System.out.println(" onDestroy+++++++++++++++++>>>");
   }
   @Override
   public boolean stopService(Intent name) {
	   unEegister();
       // TODO Auto-generated method stub
	   System.out.println("stop Service===========>>>");
       System.out.println("stop Service===========>>>");
       return super.stopService(name);

   }
   
   public static boolean isServiceRunning(String serviceClassName,Context context){
       final ActivityManager activityManager = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
       final List<RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);

       for (RunningServiceInfo runningServiceInfo : services) {
    	   
           if (runningServiceInfo.service.getClassName().contains(serviceClassName)){
               System.out.println(" isServiceRunning===========>>>"+true);
               return true;
           }
       }
       System.out.println(" isServiceRunning===========>>>"+false);
       return false;
    }

    public static Bitmap getScreenShot(View view) {
        View screenView = view.getRootView();
        screenView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(screenView.getDrawingCache());
        screenView.setDrawingCacheEnabled(false);
        return bitmap;
    }

    public static File store(Bitmap bm, String fileName){
         String dirPath = Environment.getDataDirectory().getAbsolutePath() + "/Screenshots";

        System.out.println("dirPath"+dirPath);
        File dir = new File(dirPath);

        if(!dir.exists())
            dir.mkdirs();
       // File file = new File(fileName+".png");
       File file = new File(Environment.getExternalStorageDirectory(), (fileName+".png"));

/*
        String imageFileName = "Image.png";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File file = new File(storageDir,imageFileName);
        */
  //      File file = new File(activity.getFilesDir(),("image.png"));


System.out.println("file.getAbsolutePath()"+file.getAbsolutePath());

        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 10, fOut);
            fOut.flush();
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }

    private  void shareImage(File file){
        System.out.println(".file.length()()"+file.length());
       // File sourceFile = new File("//mnt/sdcard/file.apk");
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        activity.startActivity(intent);
        /*
        Uri uri = Uri.fromFile(file);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/png");

        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, "");

        intent.putExtra(Intent.EXTRA_STREAM, uri);
        try {
            activity.startActivity(Intent.createChooser(intent, "Share Screenshot"));
        } catch (ActivityNotFoundException e) {

        }
        */
    }

    public void share_bitMap_to_Apps(View relative_me_other) {

        Intent i = new Intent(Intent.ACTION_SEND);

        i.setType("image/*");
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
    /*compress(Bitmap.CompressFormat.PNG, 100, stream);
    byte[] bytes = stream.toByteArray();*/


        i.putExtra(Intent.EXTRA_STREAM, getImageUri(activity, getBitmapFromView(relative_me_other)));
        try {
            activity.startActivity(Intent.createChooser(i, "My Profile ..."));
        } catch (android.content.ActivityNotFoundException ex) {

            ex.printStackTrace();
        }


    }
    public static Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(),      view.getHeight(), Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        else
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}