package com.hkcrfid.jewerly;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hkcrfid.bean.ItemDetailsBean;
import com.hkcrfid.bean.ItemLocationBean;
import com.hkcrfid.common.Common;
import com.hkcrfid.common.HKCRfid;

import java.util.Date;
import java.util.List;


public class LocationRegistration extends AppCompatActivity
{
    String mode="";
    Context context=this;
    EditText inputSearch,selecttedLocation;
    ListView asset_list_view;
    ArrayAdapter<String> adapter;
    String locationListArray[] = {};
    List<ItemLocationBean> listLocation=null;
    TextView epcIDTV,epcIDConfirmTV;
    SharedPreferences sharedpreferences;
    ItemLocationBean selecttedLocationBean=null;
    SqlLite sqlLite;
    Button regSaveBt,regClearBt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_location_regitration);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

//        toolbar.setLogo(R.drawable.location_reg);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        sharedpreferences = getApplicationContext().getSharedPreferences(Common.appPreference, MODE_PRIVATE);
        sqlLite=new SqlLite(context);
        epcIDTV=(TextView) findViewById(R.id.epcIDTV);
        epcIDConfirmTV=(TextView) findViewById(R.id.epcIDConfirmTV);
        selecttedLocation=(EditText) findViewById(R.id.selecttedLocation);
        selecttedLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLocationSelection();
            }
        });

        regSaveBt=(Button) findViewById(R.id.regSaveBt);
        regSaveBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo save regi details
                //1 check tagid not registered with other asset
                //2 epc and coinfirm epc is same.

                    if (selecttedLocationBean==null)
                    {
                        Toast.makeText(getApplicationContext(),"Please select item.",Toast.LENGTH_SHORT).show();
                    }else  if(epcIDConfirmTV.getText().toString().length()<=20)
                    {
                        Toast.makeText(getApplicationContext(),"Please Scan EPC.",Toast.LENGTH_SHORT).show();
                    }else if(!epcIDConfirmTV.getText().toString().equals(epcIDTV.getText().toString()))
                    {
                        Toast.makeText(getApplicationContext(),"EPC and ConfirmEPC not match",Toast.LENGTH_SHORT).show();
                    }else{
                        ItemDetailsBean itemDetailsBeanReg=new ItemDetailsBean();
                        itemDetailsBeanReg.setItemEpcId(epcIDConfirmTV.getText().toString());
                        List<ItemDetailsBean>listAsset=sqlLite.getItemDetailsIsReg(itemDetailsBeanReg);
                        if(listAsset!=null && listAsset.size()>0)
                        {
                            Toast.makeText(getApplicationContext(), "Not registered, EPC registered with Item " + listAsset.get(0).getItemCode() + ".", Toast.LENGTH_LONG).show();
                        }else
                        {
                            selecttedLocationBean.setItemLocationEpcId(epcIDConfirmTV.getText().toString());
                            List<ItemLocationBean> list = sqlLite.getItemLocationIsReg(selecttedLocationBean);
                            if (list == null || list.size() <= 0)
                            {
                                selecttedLocationBean.setNewItemLocationReg("Y");
                                selecttedLocationBean.setCreatedOn(Common.getUtcFormat(new Date()));
                                selecttedLocationBean.setCreatedBy(sharedpreferences.getInt("loginId", 0));
                                sqlLite.updateItemLocation(selecttedLocationBean);
                                Toast.makeText(getApplicationContext(), "Successfully EPC registered with " + selecttedLocationBean.getItemLocationName() + ".", Toast.LENGTH_SHORT).show();
                                selecttedLocationBean = null;
                                epcIDConfirmTV.setText("");
                                epcIDTV.setText("");
                                selecttedLocation.setText("");
                                mode = "";
                            }else if (list.size() > 0)
                            {
                                dialogUnregiterPreviousAssignOther(list.get(0));
                            }
                        }
                    }
            }
        });

        regClearBt=(Button) findViewById(R.id.regClearBt);
        regClearBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo cleate tag details
                mode="epcReg1";
                epcIDConfirmTV.setText("");
                epcIDTV.setText("");

            }
        });


    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent myIntent = new Intent(LocationRegistration.this, HomeActivity.class);
        startActivity(myIntent);
        finish();
        return true;
    }

    public void searchLocation()
    {
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                ItemLocationBean itemLocationBean=new ItemLocationBean();
                itemLocationBean.setItemLocationName(cs.toString());
                listLocation=sqlLite.getItemLocationByLocationName(itemLocationBean,null);
                if(listLocation!=null) {
                    locationListArray = new String[listLocation.size()];
                    for(int i=0;i<listLocation.size();i++)
                    {
                        locationListArray[i]=listLocation.get(i).getItemLocationName();
                    }
                    adapter = new ArrayAdapter<String>(context, R.layout.search_list_item, R.id.search_asset, locationListArray);
                    asset_list_view.setAdapter(adapter);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
    }


    public void dialogUnregiterPreviousAssignOther(final ItemLocationBean itemLocationBeanRegistered)
    {
        mode="Reassign EPC";
        final Dialog dialog = new Dialog(context);
        TextView messageTV;
        final CheckBox unregiterPrevious;
        Button updateBT,cancelBT;
        dialog.setContentView(R.layout.unregister_previous_assign_other);
        dialog.setTitle("Reassign EPC!");
        messageTV  =(TextView) dialog.findViewById(R.id.messageTV);
        unregiterPrevious=(CheckBox) dialog.findViewById(R.id.unregiterPrevious);
        updateBT   =(Button) dialog.findViewById(R.id.updateBT);
        cancelBT   =(Button) dialog.findViewById(R.id.cancelBT);
        messageTV.setText("Scanned EPC registered with "+itemLocationBeanRegistered.getItemLocationName()+".\n Do you want to reassign with "+selecttedLocationBean.getItemLocationName());



        updateBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(unregiterPrevious.isChecked())
                {
                    itemLocationBeanRegistered.setItemLocationEpcId(null);
                    itemLocationBeanRegistered.setNewItemLocationReg("Y");

                    itemLocationBeanRegistered.setModifyOn(Common.getUtcFormat(new Date()));
                    itemLocationBeanRegistered.setModifyBy(sharedpreferences.getInt("loginId", 0));

                    sqlLite.updateItemLocation(itemLocationBeanRegistered);
                    selecttedLocationBean.setCreatedOn(Common.getUtcFormat(new Date()));
                    selecttedLocationBean.setCreatedBy(sharedpreferences.getInt("loginId", 0));
                    sqlLite.updateItemLocation(selecttedLocationBean);
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(),"Successfully EPC registered with "+selecttedLocationBean.getItemLocationName()+"\n and unregistered with "+itemLocationBeanRegistered.getItemLocationName(),Toast.LENGTH_SHORT).show();
                    selecttedLocationBean=null;
                    epcIDConfirmTV.setText("");
                    epcIDTV.setText("");
                    selecttedLocation.setText("");
                    mode="";
                }else
                {
                    Toast.makeText(getApplicationContext(),"Please select unregiter previous!",Toast.LENGTH_SHORT).show();;
                }
            }
        });
        cancelBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();
    }
    public void dialogLocationSelection()
    {
        mode="Search Location";
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.search_dialog);
        dialog.setTitle("Search Location");
        selecttedLocationBean=null;
        Button assetSelectionDone   =(Button) dialog.findViewById(R.id.assetSelectionDone);
        asset_list_view = (ListView) dialog.findViewById(R.id.asset_list_view);
        inputSearch = (EditText) dialog.findViewById(R.id.inputSearch);
        listLocation=sqlLite.getItemLocationByLocationName(new ItemLocationBean(),null);
        if(listLocation!=null) {
            locationListArray = new String[listLocation.size()];
            for(int i=0;i<listLocation.size();i++)
            {
                locationListArray[i]=listLocation.get(i).getItemLocationName();
            }
            adapter = new ArrayAdapter<String>(context, R.layout.search_list_item, R.id.search_asset, locationListArray);
            asset_list_view.setAdapter(adapter);
        }
        searchLocation();
        asset_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(listLocation!=null&&listLocation.size()>position) {
                    selecttedLocationBean = listLocation.get(position);
                    selecttedLocation.setText(""+selecttedLocationBean.getItemLocationName());
                    mode="epcReg1";
                }
                dialog.dismiss();
            }
        } );


        assetSelectionDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.registrationmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
         if (id == R.id.action_asset_registration)
        {

            Intent myIntent = new Intent(LocationRegistration.this, AssetRegistration.class);
            startActivity(myIntent);
            finish();
            return true;

        }else if (id == R.id.action_location_registration) {
            /*
            Intent myIntent = new Intent(LocationRegistration.this, LocationRegistration.class);
            startActivity(myIntent);
            finish();*/
            return true;
        }else  if (id == R.id.action_UnRegistration) {
            Intent myIntent = new Intent(LocationRegistration.this, UnregistrationEpc.class);
            startActivity(myIntent);
            finish();
             return true;
        }else  if (id == R.id.action_search_option) {
//            Intent myIntent = new Intent(LocationRegistration.this, SingleSearchActivity.class);
            Intent myIntent = new Intent(LocationRegistration.this, SearchAssetActivity.class);
            startActivity(myIntent);
            finish();
             return true;
        }else  if (id == R.id.action_home) {
            Intent myIntent = new Intent(LocationRegistration.this, HomeActivity.class);
            startActivity(myIntent);
            finish();
             return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onDestroy()
    {
        System.out.println("Inventory Activity Finish");
        super.onDestroy();
    }
    Handler listEpcHandler=new Handler() {
        @Override
        public void handleMessage(Message msg) {
            try{

                String[] epcList = msg.getData().getStringArray("epcList");
                if(epcList==null || epcList.length<=0)
                {
                    Toast.makeText(getApplicationContext(), "No tag found", Toast.LENGTH_SHORT).show();
                }else   if(mode.equals("epcReg1"))
                {
                    epcIDTV.setText(epcList[0]);
                    mode="epcReg2";
                }else if(mode.equals("epcReg2"))
                {
                    epcIDConfirmTV.setText(epcList[0]);
                    mode="";
                }

            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }
    };
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
       /* if(keyCode==67)
        {
            DownloadServiceCourse.activity=LocationRegistration.this;
            Intent i = new Intent(Common.appPackage + ".synchronization").putExtra("action_type", "image");
            context.sendBroadcast(i);

        }else*/
        if(keyCode==120)
        {
            String epc[] = HKCRfid.startReading();
            Message m = new Message();
            Bundle b = new Bundle();
            b.putStringArray("epcList", epc);
            m.setData(b);
            listEpcHandler.sendMessage(m);
        }else       if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent myIntent = new Intent(LocationRegistration.this, HomeActivity.class);
            startActivity(myIntent);
            finish();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

}

