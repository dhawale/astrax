package com.hkcrfid.jewerly;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.hkcrfid.common.Common;


public class TempRegActivity extends AppCompatActivity
{
    SharedPreferences sharedpreferences =null;
    public static String regEpc=null;
    ListView list;
    Context context=this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedpreferences = getApplicationContext().getSharedPreferences(Common.appPreference, MODE_PRIVATE);
        openRegDialog();
    }



    public void openRegDialog()
    {
        final String epc=regEpc;

        // Create custom dialog object
        final Dialog dialog = new Dialog(TempRegActivity.this);
        // Include dialog.xml file
        dialog.setContentView(R.layout.temp_layout);
        // Set dialog title
        dialog.setTitle("EPC Mapping");

        // set values for custom dialog components - text, image and button
        final EditText textName = (EditText) dialog.findViewById(R.id.tagName);
        final TextView epcID = (TextView) dialog.findViewById(R.id.epcID);
        epcID.setText(epc);
        Button declineButton = (Button) dialog.findViewById(R.id.declineButton);
        dialog.show();


        // if decline button is clicked, close the custom dialog
        declineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(epc, textName.getText().toString());
                editor.commit();
                dialog.dismiss();

                Intent myIntent = new Intent(TempRegActivity.this, RfidSettingActivity.class);
                startActivity(myIntent);
                finish();

            }
        });
    }

    @Override

    public void onDestroy()
    {
        System.out.println("Temp Activity Activity Finish");
        super.onDestroy();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent myIntent = new Intent(TempRegActivity.this, RfidSettingActivity.class);
            startActivity(myIntent);
            finish();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }






}

