package com.hkcrfid.jewerly;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hkcrfid.bean.LoginBean;
import com.hkcrfid.common.Common;
import com.hkcrfid.common.HKCRfid;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A login screen that offers login via email/password.
 */

public class LoginActivity extends AppCompatActivity
{
    Button email_sign_in_button;
    AutoCompleteTextView email;
    EditText password;
    Context context=this;
    SqlLite sqlLite;
    SharedPreferences sharedpreferences;
    Dialog dialogUrlSetting;

    HttpRequestTask httpRequestTask;
    final static String PATTERN =
            "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                    "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        HKCRfid.openDevice();
        sqlLite=new SqlLite(context);
        sharedpreferences = getApplicationContext().getSharedPreferences(Common.appPreference, MODE_PRIVATE);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Login");
//        toolbar.setLogo(R.drawable.ic_launcher);
        setSupportActionBar(toolbar);

        email_sign_in_button();
        if(!DownloadServiceCourse.isServiceRunning(DownloadServiceCourse.class.getName(),getApplicationContext())) {
            Intent myIntent = new Intent(context, DownloadServiceCourse.class);
            startService(myIntent);
        }



    }

    public void email_sign_in_button()
    {
        sharedpreferences = getApplicationContext().getSharedPreferences(Common.appPreference, MODE_PRIVATE);
        Common.appUrl="http://"+sharedpreferences.getString("ipAdd","192.168.1.48")+":"+sharedpreferences.getString("portNo","8080")+"/"+sharedpreferences.getString("projectName","DemoTest")+"/";
        System.out.println("Common.appUrl"+Common.appUrl);
        String userLoginId=sharedpreferences.getString("userLoginId", "");
        //final Typeface customF = Typeface.createFromAsset(getApplicationContext().getAssets(), "06-tirkas-marathi-font.TTF");

      //  final Typeface customF = Typeface.createFromAsset(getApplicationContext().getAssets(), "Krutidev.TTF");



        email=(AutoCompleteTextView)findViewById(R.id.email);
        //email.setTypeface(customF);
        email.setText(userLoginId);
        password=(EditText)findViewById(R.id.password);
        password.setText(sharedpreferences.getString("password", ""));
        email_sign_in_button=(Button)findViewById(R.id.email_sign_in_button);
       // email_sign_in_button.setTypeface(customF);
        email_sign_in_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("email"+email.getText().toString());
                System.out.println("password"+password.getText().toString());
                LoginBean bean=new LoginBean();
                bean.setUserLoginId(email.getText().toString());
                bean.setPassword(password.getText().toString());
                // TODO please validate  emailid and password  not blank and other validation.
                List<LoginBean> loginBean=  sqlLite.getLogin(bean);
                if(loginBean!=null && loginBean.size()>0)
                {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("USERNAME", loginBean.get(0).getUserName());
                    editor.putString("userLoginId", loginBean.get(0).getUserLoginId());
                    editor.putInt("loginId", loginBean.get(0).getLoginId());
                    editor.putInt("roleId", loginBean.get(0).getRoleId());
                    editor.putString("password", loginBean.get(0).getPassword());
                    editor.commit();
                    Intent myIntent = new Intent(LoginActivity.this, HomeActivity.class);
                    startActivity(myIntent);
                    finish();
                }else
                {


                // TODO show dialog user not valid
                }

//                Intent myIntent = new Intent(LoginActivity.this, HomeActivity.class);
//                startActivity(myIntent);
//                finish();
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        if (id == R.id.action_rfid_settings)
        {
            Intent myIntent = new Intent(LoginActivity.this, RfidSettingActivity.class);
            startActivity(myIntent);
            finish();
        }
        else if (id == R.id.action_syncronize) {
            Intent myIntent = new Intent(LoginActivity.this, SyscronizeActivity.class);
            startActivity(myIntent);
            finish();
        }else if (id == R.id.urlSetting) {
            dialogUrlSettings();
        }



        return super.onOptionsItemSelected(item);
    }

    public void onDestroy()
    {
        System.out.println("Login Activity Finish");
        super.onDestroy();
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

       /*
    if(keyCode==120)
    {
        String epc[] = HKCRfid.startReading();
    }*/
       System.out.println("keyCode"+keyCode);
       /*if(keyCode==67)
       {
           DownloadServiceCourse.activity=LoginActivity.this;
           Intent i = new Intent(Common.appPackage + ".synchronization").putExtra("action_type", "image");
           context.sendBroadcast(i);

       }else*/
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            appExit();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }


    public void appExit()
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        Intent myIntent = new Intent(context, DownloadServiceCourse.class);
                        stopService(myIntent);
                        finish();
                        HKCRfid.closeDevice();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }







    private class HttpRequestTask extends AsyncTask<Void, Void,Boolean> {
        public Boolean success=false;
        @Override
        protected Boolean doInBackground(Void... params) {
            URL url;
            HttpURLConnection urlConnection = null;
            String server_response;

            try {
                url = new URL(Common.appUrl);
                urlConnection = (HttpURLConnection) url.openConnection();

                int responseCode = urlConnection.getResponseCode();

                if(responseCode == HttpURLConnection.HTTP_OK){
                    success=true;
                    return true;
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            System.out.println(success);
            if(success)
                Toast.makeText(context, "URL connetion is OK.", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(context, "URL connetion not reachable. ", Toast.LENGTH_SHORT).show();
        }

    }

    public static boolean validate(final String ip){
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher matcher = pattern.matcher(ip);
        return matcher.matches();
    }
    public void dialogUrlSettings()
    {


        dialogUrlSetting = new Dialog(context);
        dialogUrlSetting.setContentView(R.layout.urlsetting);
        dialogUrlSetting.setTitle("App URL Setting");
        dialogUrlSetting.setCancelable(false);

       final TextView messageTV = (TextView) dialogUrlSetting.findViewById(R.id.messageTV);


        final EditText ipEd = (EditText) dialogUrlSetting.findViewById(R.id.ipEd);
        final EditText portEd = (EditText) dialogUrlSetting.findViewById(R.id.portEd);
        final EditText appNametEd = (EditText) dialogUrlSetting.findViewById(R.id.appNametEd);
        ipEd.setText(sharedpreferences.getString("ipAdd","192.168.1.48"));
        portEd.setText(sharedpreferences.getString("portNo","8080"));
        appNametEd.setText(sharedpreferences.getString("projectName","DemoTest1"));




        final Button testUrlBT = (Button) dialogUrlSetting.findViewById(R.id.testUrlBT);
        final Button updateUrlBT = (Button) dialogUrlSetting.findViewById(R.id.updateUrlBT);
        final Button cancelUrlBT= (Button) dialogUrlSetting.findViewById(R.id.cancelUrlBT);
        Common.appUrl="http://"+ipEd.getText().toString()+":"+portEd.getText().toString()+"/"+appNametEd.getText().toString()+"/";
        messageTV.setText(Common.appUrl);
        testUrlBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!validate(ipEd.getText().toString()))
                {
                    Toast.makeText(getApplicationContext(),"Please Enter proper IP Address",Toast.LENGTH_SHORT).show();

                }else if(portEd.getText().toString().length()<2 || portEd.getText().toString().length()>4 )
                {
                    Toast.makeText(getApplicationContext(),"Please Enter proper Port No",Toast.LENGTH_SHORT).show();
                }else
                {
                    Common.appUrl = "http://" + ipEd.getText().toString() + ":" + portEd.getText().toString() + "/" + appNametEd.getText().toString() + "/";
                    messageTV.setText(Common.appUrl );
                    httpRequestTask = new HttpRequestTask();
                    httpRequestTask.execute();
                }
            }
        });
        // if button is clicked, close the custom dialog
        updateUrlBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(httpRequestTask!=null &&  httpRequestTask.success) {
                    Common.appUrl="http://"+ipEd.getText().toString()+":"+portEd.getText().toString()+"/"+appNametEd.getText().toString()+"/";
                    messageTV.setText(Common.appUrl );
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("ipAdd", ipEd.getText().toString());
                    editor.putString("portNo", portEd.getText().toString());
                    editor.putString("projectName", appNametEd.getText().toString());
                    editor.commit();
                    dialogUrlSetting.dismiss();
                    httpRequestTask = new HttpRequestTask();
                    httpRequestTask.execute();
                }else{
                    Toast.makeText(context, "Please Test URL, Confirm URL Connetion is OK. ", Toast.LENGTH_SHORT).show();
                }
            }
        });
        // if button is clicked, close the custom dialog
        cancelUrlBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogUrlSetting.dismiss();

            }
        });

        dialogUrlSetting.show();
        /*
        dialogUrlSetting.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if(keyCode==67)
                {
                    DownloadServiceCourse.activity=LoginActivity.this;
                    Intent i = new Intent(Common.appPackage + ".synchronization").putExtra("action_type", "image");
                    context.sendBroadcast(i);

                }
                return true;
            }
        });*/

    }

}

