package com.hkcrfid.jewerly;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.hkcrfid.bean.InventoryView;
import com.hkcrfid.bean.ItemLocationBean;
import com.hkcrfid.bean.TrackingSessionBean;
import com.hkcrfid.common.Common;
import com.hkcrfid.common.HKCRfid;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class BulkProcessAction extends AppCompatActivity {

    private boolean Scanflag = false, isStartReading = false, isAutoScanReading = false;
    private Timer timer;
    ListView list;
    SqlLite sqlLite;
    String mode = "";
    Context context = this;
    TrackingSessionBean trackingSessionBean = null;
    List<TrackingSessionBean> listTrackingSession = null;
    String listLocationArray[] = null;
    List<ItemLocationBean> listLocation = null;
    String listActionArray[] = {"IN","OUT","SALE","DAMAGE","RETURN","OTHER"};
    String locationListArray[] = {};
    TextView selectedActionView,  locationName,  scannedTvCount;
    BulkActionAdapter adapter;
    Activity activity = this;
    Date readingDate=null;
    List<InventoryView> scannedAssetList = new ArrayList<InventoryView>();

    HashMap<String, String> epcMap = new HashMap<String, String>();
    EditText inputSearch,selecttedLocation,actionInputSearch,selecttedActionSpinner;
    ItemLocationBean selecttedLocationBean;
    ListView asset_list_view;
    ArrayAdapter<String> adapterLocation;
    ArrayAdapter<String>    adapterAction;
    ListView  action_list_view;
    Dialog dialogLocationScan;
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bulkaction);
        sqlLite = new SqlLite(context);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Bulk process");
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        sharedpreferences = getApplicationContext().getSharedPreferences(Common.appPreference, MODE_PRIVATE);
        isAutoScanReading=sharedpreferences.getBoolean("isAutoScanReading",false);
        locationName = (TextView) findViewById(R.id.locationName);
        selectedActionView = (TextView) findViewById(R.id.selectedActionView);
        list = (ListView) findViewById(R.id.list);

        scannedTvCount = (TextView) findViewById(R.id.scannedTvCount);






        listTrackingSession = sqlLite.getTrackingSession();
        if (listTrackingSession == null || listTrackingSession.size() <= 0) {
            Toast.makeText(getApplicationContext(), "Please download session", Toast.LENGTH_SHORT).show();
        } else {
            trackingSessionBean = listTrackingSession.get(0);
            toolbar.setTitle(trackingSessionBean.getTrackingSessionName());
            toolbar.setLogo(R.drawable.assignment);
            dialogLocationScan();

        }

    }


    public void onItemClick(int mPosition) {

            InventoryView tempValues = (InventoryView) scannedAssetList.get(mPosition);
            dialogMessage(tempValues);

    }

    public String getDate(String date)
    {
        SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
        return  sdf.format(Common.getDateFromUtcFormat(date));
    }
    public void dialogMessage(final InventoryView tempValues )
    {


        final   Dialog   dialog = new Dialog(context);
        StringBuffer sbm=new StringBuffer();

        dialog.setContentView(R.layout.message_dialog);
        dialog.setTitle("Item Details");
        TextView messageTV = (TextView) dialog.findViewById(R.id.messageTV);
        sbm.append("Item Code: "+tempValues.getItemDetailsBean().getItemCode()+"\n");
        sbm.append("Description: \n"+ tempValues.getItemDetailsBean().getDescription()+"\n");
        if(tempValues.getItemDetailsBean().getPurchaseDate()!=null && tempValues.getItemDetailsBean().getPurchaseDate().length()>10) {
            sbm.append("Purchase Date: " +getDate(tempValues.getItemDetailsBean().getPurchaseDate())  + "\n");
        }
        sbm.append("Epc: "+tempValues.getItemDetailsBean().getItemEpcId()+"\n");
        sbm.append("Remark: "+tempValues.getItemDetailsBean().getRemark()+"\n");
        sbm.append("Location: "+tempValues.getLastItemLocationBean().getItemLocationName()+"\n");
        sbm.append("Price Rs: "+tempValues.getItemDetailsBean().getPrice()+"\n");
        //   System.out.println("tempValues.getItemDetailsBean().getItemCode()"+tempValues.getItemDetailsBean().getItemCode());
        if(tempValues.getItemDetailsBean().getCreatedOn()!=null && tempValues.getItemDetailsBean().getCreatedOn().length()>10) {


            sbm.append("Created On: " + getDate(tempValues.getItemDetailsBean().getCreatedOn()) + "\n");
        }
        if(tempValues.getItemDetailsBean().getModifyOn()!=null && tempValues.getItemDetailsBean().getModifyOn().length()>10) {


            sbm.append("Modify On: " +getDate(tempValues.getItemDetailsBean().getModifyOn()) + "\n");
        }
        //  sbm.append("Active: "+tempValues.getItemDetailsBean().getActive()+"\n");
        messageTV.setText(sbm.toString());
        Button  completBTSyb = (Button) dialog.findViewById(R.id.completBT);
        completBTSyb.setVisibility(View.VISIBLE);
        completBTSyb.setEnabled(true);
        completBTSyb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public void onDestroy() {

        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
       /* if(keyCode==67)
        {
            DownloadServiceCourse.activity=InventoryActivity.this;
            Intent i = new Intent(Common.appPackage + ".synchronization").putExtra("action_type", "image");
            context.sendBroadcast(i);

        }else*/
        if (keyCode == 120) {
            isStartReading = true;

        } else if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (listTrackingSession != null && listTrackingSession.size() > 0) {
                String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
                sqlLite.addTrackingInventory(scannedAssetList, selecttedLocationBean, trackingSessionBean, androidId,selectedActionView.getText().toString());

                dialogLocationScan();

            } else {
                isAutoScanReading=false;
                stopReading();
                Intent myIntent = new Intent(BulkProcessAction.this, HomeActivity.class);
                startActivity(myIntent);
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    private void stopReading() {

        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        UfhData.Set_sound(false);
    }

    public void startReading() {

        if (timer == null) {
            UfhData.Set_sound(true);
            UfhData.SoundFlag = false;

            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {

                    if (Scanflag) return;
                    String epc[] = null;

                    if (isStartReading || isAutoScanReading) {
                        Scanflag = true;

                        epc = HKCRfid.startReading();
                        Message m = new Message();
                        Bundle b = new Bundle();
                        b.putStringArray("epcList", epc);
                        m.setData(b);
                        listEpcHandler.sendMessage(m);
                        isStartReading = false;
                        if(epc==null || epc.length<=0)
                        {
                            UfhData.SoundFlag = false;
                        }else{
                            readingDate=new Date();
                        }
                    } else {

                        if(readingDate!=null) {
                            long seconds = (new Date().getTime() - readingDate.getTime()) / 1000;

                            if (seconds > 0) {
                                UfhData.SoundFlag = false;
                                if(seconds>100)
                                {
                                    seconds=2;
                                }
                            }
                        }
                    }
                    Scanflag = false;
                }
            }, 0, Common.SCAN_INTERVAL);

        } else {

            if (timer != null) {
                timer.cancel();
                timer = null;
            }
            UfhData.Set_sound(false);
        }
    }


    public void dialogLocationScan() {
        selecttedLocationBean = null;
        if (scannedAssetList != null)
            scannedAssetList.clear();

        if (epcMap != null)
            epcMap.clear();

        setCount();


        dialogLocationScan = new Dialog(context);
        dialogLocationScan.setContentView(R.layout.bulk_location_scan_process);
        dialogLocationScan.setTitle("Search Location");
        dialogLocationScan.setCancelable(false);
        final CheckBox scanLocationEpc = (CheckBox) dialogLocationScan.findViewById(R.id.scanLocationEpc);
        if(isAutoScanReading)
            scanLocationEpc.setChecked(true);
        else
            scanLocationEpc.setChecked(false);

        mode = "LR";

        TextView inventoryName = (TextView) dialogLocationScan.findViewById(R.id.inventoryName);
        inventoryName.setText(trackingSessionBean.getTrackingSessionName());

        selecttedLocation = (EditText) dialogLocationScan.findViewById(R.id.selecttedLocation);
        selecttedActionSpinner = (EditText) dialogLocationScan.findViewById(R.id.selecttedActionSpinner);


        selecttedActionSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogActionSelection();
            }
        });


        selecttedLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isAutoScanReading) {

                    dialogLocationSelection();
                }else{
                    InputMethodManager imm = (InputMethodManager) selecttedLocation.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(selecttedLocation.getWindowToken(), 0);
                    Toast.makeText(getApplicationContext(),"Please turn off auto scan select location.",Toast.LENGTH_LONG).show();
                }
            }
        });


        Button finishInventory = (Button) dialogLocationScan.findViewById(R.id.finishInventory);


        scanLocationEpc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (((CheckBox) v).isChecked() && mode.equals("LR")) {
                    isAutoScanReading = true;
                    // scanLocationEpc.setText("Auto Scan");
                } else {
                    isAutoScanReading = false;
                    scanLocationEpc.setChecked(false);
                    selecttedLocation.requestFocus();
                    //scanLocationEpc.setText("Auto Scan");
                }
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putBoolean("isAutoScanReading", isAutoScanReading);
                editor.commit();


            }
        });
        // if button is clicked, close the custom dialog
        /*
        Button startInventory = (Button) dialogLocationScan.findViewById(R.id.startInventory);
        startInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selecttedLocationBean!=null) {
                    dialogLocationScan.dismiss();
                    isStartLocationReading = false;
                    mode = "IM";
                }
            }
        });
        */
        // if button is clicked, close the custom dialog
        finishInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLocationScan.dismiss();
                Intent myIntent = new Intent(BulkProcessAction.this, HomeActivity.class);
                startActivity(myIntent);
                finish();
                stopReading();
                isAutoScanReading = false;
            }
        });

        dialogLocationScan.show();
        dialogLocationScan.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
              /* if(keyCode==67)
                {
                    DownloadServiceCourse.activity=InventoryActivity.this;
                    Intent i = new Intent(Common.appPackage + ".synchronization").putExtra("action_type", "image");
                    context.sendBroadcast(i);

                }else*/
                if (keyCode == 120) {
                    isStartReading = true;
                }
                return true;
            }
        });
        if (timer == null) {
            startReading();
        }
    }

    public void dialogLocationSelection() {
        mode = "LR";
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.search_dialog);
        dialog.setTitle("Search Location");

        Button assetSelectionDone = (Button) dialog.findViewById(R.id.assetSelectionDone);
        asset_list_view = (ListView) dialog.findViewById(R.id.asset_list_view);
        inputSearch = (EditText) dialog.findViewById(R.id.inputSearch);
        listLocation = sqlLite.getItemLocationByLocationName(new ItemLocationBean(),"inventory_menu");
        if (listLocation != null) {
            locationListArray = new String[listLocation.size()];
            for (int i = 0; i < listLocation.size(); i++) {
                locationListArray[i] = listLocation.get(i).getItemLocationName();
            }
            adapterLocation = new ArrayAdapter<String>(context, R.layout.search_list_item, R.id.search_asset, locationListArray);
            asset_list_view.setAdapter(adapterLocation);
        }
        searchLocation();
        asset_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (listLocation != null && listLocation.size() > position) {
                    selecttedLocationBean = listLocation.get(position);
                    selecttedLocation.setText(selecttedLocationBean.getItemLocationName());
                    locationName.setText("LOC:"+selecttedLocationBean.getItemLocationName());

                    mode = "IM";
                    setCount();
                }
                dialog.dismiss();
                dialogLocationScan.dismiss();

            }
        });


        assetSelectionDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void dialogActionSelection() {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.search_dialog);
        dialog.setTitle("Search Action");
        Button actionSelectionDone = (Button) dialog.findViewById(R.id.assetSelectionDone);
        action_list_view = (ListView) dialog.findViewById(R.id.asset_list_view);
        actionInputSearch = (EditText) dialog.findViewById(R.id.inputSearch);
        adapterAction = new ArrayAdapter<String>(context, R.layout.search_list_item, R.id.search_asset, listActionArray);
        action_list_view.setAdapter(adapterAction);
        searchAction();

        action_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selecttedActionSpinner.setText(listActionArray[position]);
                selectedActionView.setText(listActionArray[position]);
                dialog.dismiss();


            }
        });


        actionSelectionDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();
    }
    public void setCount()
    {

        if(scannedAssetList!=null )
            scannedTvCount.setText("" + scannedAssetList.size());

    }
    public void searchLocation() {
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                ItemLocationBean itemLocationBean = new ItemLocationBean();
                itemLocationBean.setItemLocationName(cs.toString());
                listLocation = sqlLite.getItemLocationByLocationName(itemLocationBean,"inventory_menu");
                if (listLocation != null) {
                    locationListArray = new String[listLocation.size()];
                    for (int i = 0; i < listLocation.size(); i++) {
                        locationListArray[i] = listLocation.get(i).getItemLocationName();
                    }
                    adapterLocation = new ArrayAdapter<String>(context, R.layout.search_list_item, R.id.search_asset, locationListArray);
                    asset_list_view.setAdapter(adapterLocation);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {


                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
    }


    public void searchAction() {
        actionInputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text

                adapterAction = new ArrayAdapter<String>(context, R.layout.search_list_item, R.id.search_asset, listActionArray);
                action_list_view.setAdapter(adapterAction);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    Handler listEpcHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            try {
                String[] epcList = msg.getData().getStringArray("epcList");
                if (mode.equals("LR")) {
                    ItemLocationBean searchLocationBean = new ItemLocationBean();
                    searchLocationBean.setItemLocationEpcId(epcList[0]);

                    List<ItemLocationBean> list = sqlLite.getItemLocationIsReg(searchLocationBean);
                    if (list != null && list.size() > 0) {
                        dialogLocationScan.dismiss();
                        selecttedLocationBean = list.get(0);
                        locationName.setText("LOC:"+selecttedLocationBean.getItemLocationName());
                        mode = "IM";
                        setCount();
                    }

                } else {
                    // todo inventory_menu scanning
                    StringBuffer sb = null;
                    for (int i = 0; epcList != null && i < epcList.length; i++) {
                        if (!epcMap.containsKey(epcList[i])) {
                            epcMap.put(epcList[i], epcList[i]);
                            if (sb == null) {
                                sb = new StringBuffer();
                                sb.append("'" + epcList[i] + "'");
                            } else {
                                sb.append(",'" + epcList[i] + "'");
                            }
                        }
                    }

                    if (sb != null && sb.length() > 0) {
                        List<InventoryView> scannedAssetListNew = sqlLite.getItemDetailsScannedByEpcs(sb, sharedpreferences.getInt("loginId", 0));


                        if(scannedAssetListNew!=null && scannedAssetListNew.size()>0) {

                            scannedAssetList.addAll(0, scannedAssetListNew);
                            adapter = new BulkActionAdapter(activity, scannedAssetList, "Scanned");
                            list.setAdapter(adapter);
                        }
                        setCount();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };





}
