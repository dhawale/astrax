package com.hkcrfid.jewerly;

/**
 * Created by Shree on 9/25/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.hkcrfid.bean.DeviceLocationBean;
import com.hkcrfid.bean.DevicesBean;
import com.hkcrfid.bean.InventoryView;
import com.hkcrfid.bean.ItemDetailsBean;
import com.hkcrfid.bean.ItemLocationBean;
import com.hkcrfid.bean.LoginBean;
import com.hkcrfid.bean.RoleTypeBean;
import com.hkcrfid.bean.TrackingInventoryBean;
import com.hkcrfid.bean.TrackingSessionBean;
import com.hkcrfid.common.Common;
import com.hkcrfid.common.Loger;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SqlLite extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "JewerlyDBHH";


    static
    {
        System.out.println("static sql lite call  ");
        File folder= new File(Common.rootFoleder);
        if (!folder.exists() || !folder.isDirectory())
        {
          folder.mkdirs();
        }



    }
    public SqlLite(Context context) {
      //  super(context, Common.rootFoleder     + File.separator + DATABASE_NAME, null, DATABASE_VERSION);
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
       // super(context, Common.rootFoleder+ File.separator+DATABASE_NAME, null, DATABASE_VERSION);
        System.out.println("SqlLite instaNCE IS CREATED");
    }




    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        System.out.println("Data base is created");

        String CREATE_DeviceLocation= "CREATE TABLE DeviceLocation (deviceLocationId INTEGER  ,deviceLocation TEXT ,deviceLocationParentId INTEGER ,useFor TEXT ,createdBy INTEGER ,createdOn DATE ,modifyBy INTEGER ,modifyOn DATE ,active TEXT )";
        db.execSQL(CREATE_DeviceLocation);
        String CREATE_Devices= "CREATE TABLE Devices (deviceId INTEGER  ,deviceName TEXT ,deviceParentId INTEGER ,deviceIP TEXT ,deviceLocationId INTEGER ,deviceSerialNo TEXT ,createdBy INTEGER ,createdOn DATE ,modifyBy INTEGER ,modifyOn DATE ,active TEXT )";
        db.execSQL(CREATE_Devices);
        String CREATE_ItemDetails= "CREATE TABLE ItemDetails (itemId INTEGER ,itemCode TEXT ,description TEXT ,itemParentID INTEGER  ,purchaseDate DATE ,warrantyDate DATE ,itemEpcId TEXT ,itemBarCodeID TEXT ,remark TEXT ,lastLocationID INTEGER ,currentLocationId INTEGER ,status TEXT ,manufactuare TEXT ,price TEXT ,createdBy INTEGER ,createdOn DATE ,modifyBy INTEGER ,modifyOn DATE ,active TEXT, newItemReg  TEXT,  sectionNumber TEXT, dateOfAssetDeposite TEXT, nameOFAssetDepositorDate TEXT, complaintBy TEXT, criminalName TEXT, assetDescription TEXT, courtRemarkAssetPrezenty TEXT, courtCommandNumberDate TEXT, assetExit TEXT, nameOfAssetCollect TEXT, dateOfAssetCollect TEXT)";
        db.execSQL(CREATE_ItemDetails);
        String CREATE_ItemLocation= "CREATE TABLE ItemLocation (itemLocationId INTEGER  ,itemLocationName TEXT ,itemParentLocationId INTEGER ,itemLocationEpcId TEXT ,itemLocationBarCodeID TEXT ,createdBy INTEGER ,createdOn DATE ,modifyBy INTEGER ,modifyOn DATE ,active TEXT, newItemLocationReg TEXT )";
        db.execSQL(CREATE_ItemLocation);
        String CREATE_Login= "CREATE TABLE Login (loginId INTEGER  ,userName TEXT ,userLoginId TEXT ,password TEXT ,roleId INTEGER ,createdBy INTEGER ,createdOn DATE ,modifyBy INTEGER ,modifyOn DATE ,active TEXT )";
        db.execSQL(CREATE_Login);
        String CREATE_RoleMenu= "CREATE TABLE RoleMenu (menuID INTEGER ,menuName TEXT ,menuParentID INTEGER ,roleId INTEGER ,active TEXT ,insertRecord TEXT ,updateRecord TEXT ,disableRecord TEXT ,displayRecord TEXT ,createdBy INTEGER ,createdOn DATE ,modifyBy INTEGER ,modifyOn DATE )";
        db.execSQL(CREATE_RoleMenu);
        String CREATE_RoleType= "CREATE TABLE RoleType (roleId INTEGER  ,roleName TEXT ,createdBy INTEGER ,createdOn DATE ,modifyBy INTEGER ,modifyOn DATE ,active TEXT )";
        db.execSQL(CREATE_RoleType);
        String CREATE_TrackingInventory= "CREATE TABLE TrackingInventory (trackingInventoryId INTEGER  ,trackingSessionId INTEGER ,itemLocationId INTEGER ,deviceId INTEGER ,status TEXT ,createdBy INTEGER ,createdOn DATE ,modifyBy INTEGER ,modifyOn DATE ,active TEXT, deviceSerialNo Text, itemBarCodeID Text, itemEpcId Text, itemId INTEGER )";
        db.execSQL(CREATE_TrackingInventory);
        String CREATE_TrackingSession= "CREATE TABLE TrackingSession (trackingSessionId INTEGER  ,trackingSessionName TEXT ,startDate DATE ,endDate DATE ,createdBy INTEGER ,createdOn DATE ,modifyBy INTEGER ,modifyOn DATE ,active TEXT )";
        db.execSQL(CREATE_TrackingSession);

    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        System.out.println("onUpgrade ==>>");
        db.execSQL("DROP TABLE IF EXISTS DeviceLocation");// Create tables again
        db.execSQL("DROP TABLE IF EXISTS Devices");
        db.execSQL("DROP TABLE IF EXISTS ItemDetails");
        db.execSQL("DROP TABLE IF EXISTS ItemLocation");
        db.execSQL("DROP TABLE IF EXISTS Login");
        db.execSQL("DROP TABLE IF EXISTS RoleMenu");
        db.execSQL("DROP TABLE IF EXISTS RoleType");
        db.execSQL("DROP TABLE IF EXISTS TrackingInventory");
        db.execSQL("DROP TABLE IF EXISTS TrackingSession");
        onCreate(db);
    }


    public boolean addDeviceLocation(DeviceLocationBean bean)
    {
        boolean success=false;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            System.out.println("db.getVersion()"+db.getVersion());
            ContentValues values = new ContentValues();
            values.put("deviceLocation", bean.getDeviceLocation());
            values.put("deviceLocationParentId", bean.getDeviceLocationParentId());
            values.put("useFor", bean.getUseFor());
            values.put("createdBy", bean.getCreatedBy());
            values.put("createdOn", bean.getCreatedOn());
            values.put("modifyBy", bean.getModifyBy());
            values.put("modifyOn", bean.getModifyOn());

            values.put("active", bean.getActive());
            db.insert("DeviceLocation", null, values);
            success=true;
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
    public int updateDeviceLocation(DeviceLocationBean bean)
    {
        int success=-1;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("deviceLocation", bean.getDeviceLocation());
            values.put("deviceLocationParentId", bean.getDeviceLocationParentId());
            values.put("useFor", bean.getUseFor());
            values.put("createdBy", bean.getCreatedBy());
            values.put("createdOn", bean.getCreatedOn());
            values.put("modifyBy", bean.getModifyBy());
            values.put("modifyOn", bean.getModifyOn());
            values.put("active", bean.getActive());
            success=db.update("DeviceLocation", values,"deviceLocationId=?",   new String[] { String.valueOf(bean.getDeviceLocationId()) } );
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
    public boolean deleteDeviceLocation(DeviceLocationBean bean)
    {
        boolean success=false;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete("DeviceLocation","deviceLocationId=?",   new String[] { String.valueOf(bean.getDeviceLocationId()) } );
            success=true;
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }

    public List<DeviceLocationBean> getDeviceLocation(DeviceLocationBean bean)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        List<DeviceLocationBean> list=null;
        try
        {

            String selectQuery = " SELECT  * FROM DeviceLocation where deviceLocationId='"+ bean.getDeviceLocationId()+"'";
            Cursor cursor = db.rawQuery(selectQuery, null);
            if(cursor.moveToFirst())
            {
                list=new ArrayList<DeviceLocationBean>();
                do
                {
                    DeviceLocationBean bean1=new DeviceLocationBean();
                    bean1.setDeviceLocation(cursor.getString(cursor.getColumnIndex("deviceLocation")));
                    bean1.setDeviceLocationParentId(cursor.getInt(cursor.getColumnIndex("deviceLocationParentId")));
                    bean1.setUseFor(cursor.getString(cursor.getColumnIndex("useFor")));
                    bean1.setCreatedBy(cursor.getInt(cursor.getColumnIndex("createdBy")));
                    bean1.setCreatedOn(cursor.getString(cursor.getColumnIndex("createdOn")));
                    bean1.setModifyBy(cursor.getInt(cursor.getColumnIndex("modifyBy")));
                    bean1.setModifyOn(cursor.getString(cursor.getColumnIndex("modifyOn")));
                    bean1.setActive(cursor.getString(cursor.getColumnIndex("active")));
                    list.add(bean1);
                }while (cursor.moveToNext());
            }

            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }

    //DevicesBean


    public boolean addDevices(DevicesBean bean)
    {
        boolean success=false;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("deviceName", bean.getDeviceName());
            values.put("deviceParentId", bean.getDeviceParentId());
            values.put("deviceIP", bean.getDeviceIP());
            values.put("deviceLocationId", bean.getDeviceLocationId());
            values.put("deviceSerialNo", bean.getDeviceSerialNo());
            values.put("createdBy", bean.getCreatedBy());
            values.put("createdOn", bean.getCreatedOn());
            values.put("modifyBy", bean.getModifyBy());
            values.put("modifyOn", bean.getModifyOn());
            values.put("active", bean.getActive());
            db.insert("Devices", null, values);
            success=true;
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
    public int updateDevices(DevicesBean bean)
    {
        int success=-1;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("deviceName", bean.getDeviceName());
            values.put("deviceParentId", bean.getDeviceParentId());
            values.put("deviceIP", bean.getDeviceIP());
            values.put("deviceLocationId", bean.getDeviceLocationId());
            values.put("deviceSerialNo", bean.getDeviceSerialNo());
            values.put("createdBy", bean.getCreatedBy());
            values.put("createdOn", bean.getCreatedOn());
            values.put("modifyBy", bean.getModifyBy());
            values.put("modifyOn", bean.getModifyOn());
            values.put("active", bean.getActive());
            success=db.update("Devices", values,"deviceId=?",   new String[] { String.valueOf(bean.getDeviceId()) } );
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
    public boolean deleteDevices(DevicesBean bean)
    {
        boolean success=false;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete("Devices","deviceId=?",   new String[] { String.valueOf(bean.getDeviceId()) } );
            success=true;
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }

    public List<DevicesBean> getDevices(DevicesBean bean)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        List<DevicesBean> list=null;
        try
        {

            String selectQuery = " SELECT  * FROM Devices where deviceId='"+ bean.getDeviceId()+"'";
            Cursor cursor = db.rawQuery(selectQuery, null);
            if(cursor.moveToFirst())
            {
                list=new ArrayList<DevicesBean>();
                do
                {
                    DevicesBean bean1=new DevicesBean();
                    bean1.setDeviceName(cursor.getString(cursor.getColumnIndex("deviceName")));
                    bean1.setDeviceParentId(cursor.getInt(cursor.getColumnIndex("deviceParentId")));
                    bean1.setDeviceIP(cursor.getString(cursor.getColumnIndex("deviceIP")));
                    bean1.setDeviceLocationId(cursor.getInt(cursor.getColumnIndex("deviceLocationId")));
                    bean1.setDeviceSerialNo(cursor.getString(cursor.getColumnIndex("deviceSerialNo")));
                    bean1.setCreatedBy(cursor.getInt(cursor.getColumnIndex("createdBy")));
                    bean1.setCreatedOn(cursor.getString(cursor.getColumnIndex("createdOn")));
                    bean1.setModifyBy(cursor.getInt(cursor.getColumnIndex("modifyBy")));
                    bean1.setModifyOn(cursor.getString(cursor.getColumnIndex("modifyOn")));
                    bean1.setActive(cursor.getString(cursor.getColumnIndex("active")));
                    list.add(bean1);
                }while (cursor.moveToNext());
            }

            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }




    public boolean addItemDetails(ItemDetailsBean bean)
    {
        boolean success=false;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("itemId", bean.getItemId());
            values.put("itemCode", bean.getItemCode());
            values.put("description", bean.getDescription());
            if(bean.getItemParent()!=null) {
                values.put("itemParentID", bean.getItemParent().getItemId());
            }else{
                values.put("itemParentID", 0);
            }
            values.put("purchaseDate", bean.getPurchaseDate());
            values.put("warrantyDate", bean.getWarrantyDate());
            values.put("itemEpcId", bean.getItemEpcId());
            values.put("itemBarCodeID", bean.getItemBarCodeID());
            values.put("remark", bean.getRemark());
            values.put("lastLocationID", bean.getLastLocationID());
            values.put("currentLocationId", bean.getCurrentLocationId());
            values.put("status", bean.getStatus());
            values.put("manufactuare", bean.getManufactuare());
            values.put("price", bean.getPrice());
            values.put("createdBy", bean.getCreatedBy());
            values.put("createdOn", bean.getCreatedOn());
            values.put("modifyBy", bean.getModifyBy());
            values.put("modifyOn", bean.getModifyOn());
            values.put("active", bean.getActive());

            values.put("sectionNumber", bean.getSectionNumber());
            values.put("dateOfAssetDeposite", bean.getDateOfAssetDepositeUTC());
            values.put("nameOFAssetDepositorDate", bean.getNameOFAssetDepositorDate());
            values.put("complaintBy", bean.getComplaintBy());
            values.put("criminalName", bean.getCriminalName());
            values.put("assetDescription", bean.getAssetDescription());
            values.put("courtRemarkAssetPrezenty", bean.getCourtRemarkAssetPrezenty());
            values.put("courtCommandNumberDate", bean.getCourtCommandNumberDate());
            values.put("assetExit", bean.getAssetExit());
            values.put("nameOfAssetCollect", bean.getNameOfAssetCollect());
            values.put("dateOfAssetCollect", bean.getDateOfAssetCollectUTC());



            db.insert("ItemDetails", null, values);
            success=true;
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }

    public boolean addItemDetailsList(List <ItemDetailsBean> beans)
    {
        boolean success=false;
        try
        {
            if(beans!=null && beans.size()>0) {

                deleteTable("ItemDetails");
                SQLiteDatabase db = this.getWritableDatabase();
                db.beginTransaction();
                ContentValues values = null;
                for (ItemDetailsBean bean : beans) {
                    values = new ContentValues();
                    values.put("itemId", bean.getItemId());
                    values.put("itemCode", bean.getItemCode());

                    values.put("description", bean.getDescription());
                    if(bean.getItemParent()!=null) {
                        values.put("itemParentID", bean.getItemParent().getItemId());
                    }else{
                        values.put("itemParentID", 0);
                    }
                    values.put("purchaseDate", bean.getPurchaseDate());
                    values.put("warrantyDate", bean.getWarrantyDate());
                    values.put("itemEpcId", bean.getItemEpcId());
                    values.put("itemBarCodeID", bean.getItemBarCodeID());
                    values.put("remark", bean.getRemark());
                    values.put("lastLocationID", bean.getLastLocationID());
                    values.put("currentLocationId", bean.getCurrentLocationId());
                    values.put("status", bean.getStatus());
                    values.put("manufactuare", bean.getManufactuare());
                    values.put("price", bean.getPrice());
                    values.put("createdBy", bean.getCreatedBy());
                    values.put("createdOn", bean.getCreatedOn());
                    System.out.println("Database bean.getItemCode()"+ bean.getItemCode());
                    System.out.println("Database bean.getCreatedOn()"+ bean.getCreatedOn());
                    values.put("modifyBy", bean.getModifyBy());
                    values.put("modifyOn", bean.getModifyOn());
                    System.out.println("Database bean.getModifyOn()"+ bean.getModifyOn());
                    values.put("active", bean.getActive());
                    values.put("newItemReg", bean.getNewItemReg());

                    System.out.println(" download addItemDetailsList getCriminalName"+bean.getCriminalName());
                    values.put("sectionNumber", bean.getSectionNumber());
                    values.put("dateOfAssetDeposite", bean.getDateOfAssetDepositeUTC());
                    values.put("nameOFAssetDepositorDate", bean.getNameOFAssetDepositorDate());
                    values.put("complaintBy", bean.getComplaintBy());
                    values.put("criminalName", bean.getCriminalName());
                    values.put("assetDescription", bean.getAssetDescription());
                    values.put("courtRemarkAssetPrezenty", bean.getCourtRemarkAssetPrezenty());
                    values.put("courtCommandNumberDate", bean.getCourtCommandNumberDate());
                    values.put("assetExit", bean.getAssetExit());
                    values.put("nameOfAssetCollect", bean.getNameOfAssetCollect());
                    values.put("dateOfAssetCollect", bean.getDateOfAssetCollectUTC());
                    db.insert("ItemDetails", null, values);
                    values = null;
                    bean = null;
                }
                db.setTransactionSuccessful();
                db.endTransaction();
                db.close();
                success = true;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
    public int updateItemDetails(ItemDetailsBean bean)
    {
        int success=-1;
        try
        {

            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("itemId", bean.getItemId());
            values.put("itemCode", bean.getItemCode());
            values.put("description", bean.getDescription());
            if(bean.getItemParent()!=null) {
                values.put("itemParentID", bean.getItemParent().getItemId());
            }else{
                values.put("itemParentID", 0);
            }
            values.put("purchaseDate", bean.getPurchaseDate());
            values.put("warrantyDate", bean.getWarrantyDate());
            values.put("itemEpcId", bean.getItemEpcId());
            values.put("itemBarCodeID", bean.getItemBarCodeID());
            values.put("remark", bean.getRemark());
            values.put("lastLocationID", bean.getLastLocationID());
            values.put("currentLocationId", bean.getCurrentLocationId());
            values.put("status", bean.getStatus());
            values.put("manufactuare", bean.getManufactuare());
            values.put("price", bean.getPrice());
            values.put("createdBy", bean.getCreatedBy());
            values.put("createdOn", bean.getCreatedOn());
            values.put("modifyBy", bean.getModifyBy());
            values.put("modifyOn", bean.getModifyOn());
            values.put("active", bean.getActive());
            values.put("newItemReg", bean.getNewItemReg());
            values.put("sectionNumber", bean.getSectionNumber());
            values.put("dateOfAssetDeposite", bean.getDateOfAssetDepositeUTC());
            values.put("nameOFAssetDepositorDate", bean.getNameOFAssetDepositorDate());
            values.put("complaintBy", bean.getComplaintBy());
            values.put("criminalName", bean.getCriminalName());
            values.put("assetDescription", bean.getAssetDescription());
            values.put("courtRemarkAssetPrezenty", bean.getCourtRemarkAssetPrezenty());
            values.put("courtCommandNumberDate", bean.getCourtCommandNumberDate());
            values.put("assetExit", bean.getAssetExit());
            values.put("nameOfAssetCollect", bean.getNameOfAssetCollect());
            values.put("dateOfAssetCollect", bean.getDateOfAssetCollectUTC());


            success=db.update("ItemDetails", values,"itemId=?",   new String[] { String.valueOf(bean.getItemId()) } );
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
    public boolean deleteItemDetails(ItemDetailsBean bean)
    {
        boolean success=false;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete("ItemDetails","itemId=?",   new String[] { String.valueOf(bean.getItemId()) } );
            success=true;
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }

    public List<ItemDetailsBean> getItemDetails(ItemDetailsBean bean)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        List<ItemDetailsBean> list=null;
        try
        {

            String selectQuery = " SELECT  * FROM ItemDetails where itemId='"+ bean.getItemId()+"'";
            Cursor cursor = db.rawQuery(selectQuery, null);
            if(cursor.moveToFirst())
            {
                list=new ArrayList<ItemDetailsBean>();
                do
                {
                    ItemDetailsBean bean1=new ItemDetailsBean();
                    bean1.setItemId(cursor.getInt(cursor.getColumnIndex("itemId")));
                    bean1.setItemCode(cursor.getString(cursor.getColumnIndex("itemCode")));
                    bean1.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                    int itemParentId=cursor.getInt(cursor.getColumnIndex("itemParentID"));
                    if(itemParentId>0) {
                        ItemDetailsBean parentBean=new ItemDetailsBean();
                        parentBean.setItemId(itemParentId);
                        bean1.setItemParent(parentBean);

                    }else{
                        bean1.setItemParent(null);
                    }

                    bean1.setPurchaseDate(cursor.getString(cursor.getColumnIndex("purchaseDate")));
                    bean1.setWarrantyDate(cursor.getString(cursor.getColumnIndex("warrantyDate")));
                    bean1.setItemEpcId(cursor.getString(cursor.getColumnIndex("itemEpcId")));
                    bean1.setItemBarCodeID(cursor.getString(cursor.getColumnIndex("itemBarCodeID")));
                    bean1.setRemark(cursor.getString(cursor.getColumnIndex("remark")));
                    bean1.setLastLocationID(cursor.getInt(cursor.getColumnIndex("lastLocationID")));
                    bean1.setCurrentLocationId(cursor.getInt(cursor.getColumnIndex("currentLocationId")));
                    bean1.setStatus(cursor.getString(cursor.getColumnIndex("status")));
                    bean1.setManufactuare(cursor.getString(cursor.getColumnIndex("manufactuare")));
                    bean1.setPrice(cursor.getString(cursor.getColumnIndex("price")));
                    bean1.setCreatedBy(cursor.getInt(cursor.getColumnIndex("createdBy")));
                    bean1.setCreatedOn(cursor.getString(cursor.getColumnIndex("createdOn")));
                    bean1.setModifyBy(cursor.getInt(cursor.getColumnIndex("modifyBy")));
                    bean1.setModifyOn(cursor.getString(cursor.getColumnIndex("modifyOn")));
                    bean1.setActive(cursor.getString(cursor.getColumnIndex("active")));
                    bean1.setNewItemReg(cursor.getString(cursor.getColumnIndex("newItemReg")));


                    bean1.setSectionNumber(cursor.getString(cursor.getColumnIndex("sectionNumber")));
                    bean1.setDateOfAssetDepositeUTC(cursor.getString(cursor.getColumnIndex("dateOfAssetDeposite")));
                    bean1.setNameOFAssetDepositorDate(cursor.getString(cursor.getColumnIndex("nameOFAssetDepositorDate")));
                    bean1.setComplaintBy(cursor.getString(cursor.getColumnIndex("complaintBy")));
                    bean1.setCriminalName(cursor.getString(cursor.getColumnIndex("criminalName")));
                    bean1.setAssetDescription(cursor.getString(cursor.getColumnIndex("assetDescription")));
                    bean1.setCourtRemarkAssetPrezenty(cursor.getString(cursor.getColumnIndex("courtRemarkAssetPrezenty")));
                    bean1.setCourtCommandNumberDate(cursor.getString(cursor.getColumnIndex("courtCommandNumberDate")));
                    bean1.setAssetExit(cursor.getString(cursor.getColumnIndex("assetExit")));
                    bean1.setNameOfAssetCollect(cursor.getString(cursor.getColumnIndex("nameOfAssetCollect")));
                    bean1.setDateOfAssetCollectUTC(cursor.getString(cursor.getColumnIndex("dateOfAssetCollect")));
                    list.add(bean1);
                }while (cursor.moveToNext());
            }

            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }

    public List<ItemDetailsBean> getItemDetailsReg()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        List<ItemDetailsBean> list=null;
        try
        {

            String selectQuery = " SELECT  * FROM ItemDetails where newItemReg='Y'";
            Cursor cursor = db.rawQuery(selectQuery, null);
            if(cursor.moveToFirst())
            {
                list=new ArrayList<ItemDetailsBean>();
                do
                {
                    ItemDetailsBean bean1=new ItemDetailsBean();

                    bean1.setItemId(cursor.getInt(cursor.getColumnIndex("itemId")));
                    bean1.setItemCode(cursor.getString(cursor.getColumnIndex("itemCode")));
                    bean1.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                    int itemParentId=cursor.getInt(cursor.getColumnIndex("itemParentID"));
                    if(itemParentId>0) {
                        ItemDetailsBean parentBean=new ItemDetailsBean();
                        parentBean.setItemId(itemParentId);
                        bean1.setItemParent(parentBean);

                    }else{
                        bean1.setItemParent(null);
                    }
                    bean1.setPurchaseDate(cursor.getString(cursor.getColumnIndex("purchaseDate")));
                    bean1.setWarrantyDate(cursor.getString(cursor.getColumnIndex("warrantyDate")));
                    bean1.setItemEpcId(cursor.getString(cursor.getColumnIndex("itemEpcId")));
                    bean1.setItemBarCodeID(cursor.getString(cursor.getColumnIndex("itemBarCodeID")));
                    bean1.setRemark(cursor.getString(cursor.getColumnIndex("remark")));
                    bean1.setLastLocationID(cursor.getInt(cursor.getColumnIndex("lastLocationID")));
                    bean1.setCurrentLocationId(cursor.getInt(cursor.getColumnIndex("currentLocationId")));
                    bean1.setStatus(cursor.getString(cursor.getColumnIndex("status")));
                    bean1.setManufactuare(cursor.getString(cursor.getColumnIndex("manufactuare")));
                    bean1.setPrice(cursor.getString(cursor.getColumnIndex("price")));
                    bean1.setCreatedBy(cursor.getInt(cursor.getColumnIndex("createdBy")));
                    bean1.setCreatedOn(cursor.getString(cursor.getColumnIndex("createdOn")));
                    bean1.setModifyBy(cursor.getInt(cursor.getColumnIndex("modifyBy")));
                    bean1.setModifyOn(cursor.getString(cursor.getColumnIndex("modifyOn")));
                    bean1.setActive(cursor.getString(cursor.getColumnIndex("active")));
                    bean1.setNewItemReg(cursor.getString(cursor.getColumnIndex("newItemReg")));
                    bean1.setSectionNumber(cursor.getString(cursor.getColumnIndex("sectionNumber")));
                    bean1.setDateOfAssetDepositeUTC(cursor.getString(cursor.getColumnIndex("dateOfAssetDeposite")));
                    bean1.setNameOFAssetDepositorDate(cursor.getString(cursor.getColumnIndex("nameOFAssetDepositorDate")));
                    bean1.setComplaintBy(cursor.getString(cursor.getColumnIndex("complaintBy")));
                    bean1.setCriminalName(cursor.getString(cursor.getColumnIndex("criminalName")));

                    System.out.println(" getItemDetailsReg getCriminalName"+bean1.getCriminalName());
                    bean1.setAssetDescription(cursor.getString(cursor.getColumnIndex("assetDescription")));
                    bean1.setCourtRemarkAssetPrezenty(cursor.getString(cursor.getColumnIndex("courtRemarkAssetPrezenty")));
                    bean1.setCourtCommandNumberDate(cursor.getString(cursor.getColumnIndex("courtCommandNumberDate")));
                    bean1.setAssetExit(cursor.getString(cursor.getColumnIndex("assetExit")));
                    bean1.setNameOfAssetCollect(cursor.getString(cursor.getColumnIndex("nameOfAssetCollect")));
                    bean1.setDateOfAssetCollectUTC(cursor.getString(cursor.getColumnIndex("dateOfAssetCollect")));
                    list.add(bean1);
                }while (cursor.moveToNext());
            }

            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }


    public List<InventoryView> getItemDetailsUnscannedByLastLocation(int locationID)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        List<InventoryView> list=new ArrayList<InventoryView>();
        try
        {
            //String selectQuery = " SELECT  * FROM ItemDetails idm left outer join ItemLocation ilm on idm.lastLocationID=ilm.itemLocationId ";
            //String selectQuery = " SELECT  * FROM ItemDetails idm left outer join ItemLocation ilm on idm.lastLocationID=ilm.itemLocationId where lastLocationID='"+locationID+"' and lastLocationID!=currentLocationId";
            String selectQuery = " SELECT  * FROM ItemDetails idm left outer join ItemLocation ilm on ((idm.lastLocationID=ilm.itemLocationId and idm.currentLocationId=0) or (idm.currentLocationId=ilm.itemLocationId)) where (idm.lastLocationID='"+locationID+"' and currentLocationId=0) or idm.currentLocationId='"+locationID+"' ";
            Cursor cursor = db.rawQuery(selectQuery, null);
            if(cursor.moveToFirst())
            {


                do
                {
                    InventoryView inventoryView=new InventoryView();
                    ItemDetailsBean bean1=new ItemDetailsBean();
                    ItemLocationBean locationBean=new ItemLocationBean();
                    bean1.setItemId(cursor.getInt(cursor.getColumnIndex("itemId")));
                    bean1.setItemCode(cursor.getString(cursor.getColumnIndex("itemCode")));
                    bean1.setDescription(cursor.getString(cursor.getColumnIndex("description")));

                    int itemParentId=cursor.getInt(cursor.getColumnIndex("itemParentID"));
                    if(itemParentId>0) {
                        ItemDetailsBean parentBean=new ItemDetailsBean();
                        parentBean.setItemId(itemParentId);
                        bean1.setItemParent(parentBean);

                    }else{
                        bean1.setItemParent(null);
                    }



                    bean1.setPurchaseDate(cursor.getString(cursor.getColumnIndex("purchaseDate")));
                    bean1.setWarrantyDate(cursor.getString(cursor.getColumnIndex("warrantyDate")));
                    bean1.setItemEpcId(cursor.getString(cursor.getColumnIndex("itemEpcId")));
                    bean1.setItemBarCodeID(cursor.getString(cursor.getColumnIndex("itemBarCodeID")));
                    bean1.setRemark(cursor.getString(cursor.getColumnIndex("remark")));
                    bean1.setLastLocationID(cursor.getInt(cursor.getColumnIndex("lastLocationID")));
                    bean1.setCurrentLocationId(cursor.getInt(cursor.getColumnIndex("currentLocationId")));

                    bean1.setStatus(cursor.getString(cursor.getColumnIndex("status")));
                    bean1.setManufactuare(cursor.getString(cursor.getColumnIndex("manufactuare")));
                    bean1.setPrice(cursor.getString(cursor.getColumnIndex("price")));

                    bean1.setCreatedBy(cursor.getInt(cursor.getColumnIndex("createdBy")));
                    bean1.setCreatedOn(cursor.getString(cursor.getColumnIndex("createdOn")));
                    bean1.setModifyBy(cursor.getInt(cursor.getColumnIndex("modifyBy")));
                    bean1.setModifyOn(cursor.getString(cursor.getColumnIndex("modifyOn")));

                    bean1.setActive(cursor.getString(cursor.getColumnIndex("active")));
                    bean1.setNewItemReg(cursor.getString(cursor.getColumnIndex("newItemReg")));
                    bean1.setSectionNumber(cursor.getString(cursor.getColumnIndex("sectionNumber")));
                    bean1.setDateOfAssetDepositeUTC(cursor.getString(cursor.getColumnIndex("dateOfAssetDeposite")));
                    bean1.setNameOFAssetDepositorDate(cursor.getString(cursor.getColumnIndex("nameOFAssetDepositorDate")));
                    bean1.setComplaintBy(cursor.getString(cursor.getColumnIndex("complaintBy")));
                    bean1.setCriminalName(cursor.getString(cursor.getColumnIndex("criminalName")));
                    bean1.setAssetDescription(cursor.getString(cursor.getColumnIndex("assetDescription")));
                    bean1.setCourtRemarkAssetPrezenty(cursor.getString(cursor.getColumnIndex("courtRemarkAssetPrezenty")));
                    bean1.setCourtCommandNumberDate(cursor.getString(cursor.getColumnIndex("courtCommandNumberDate")));
                    bean1.setAssetExit(cursor.getString(cursor.getColumnIndex("assetExit")));
                    bean1.setNameOfAssetCollect(cursor.getString(cursor.getColumnIndex("nameOfAssetCollect")));
                    bean1.setDateOfAssetCollectUTC(cursor.getString(cursor.getColumnIndex("dateOfAssetCollect")));


                    inventoryView.setItemDetailsBean(bean1);

                    locationBean.setItemLocationId(cursor.getInt(cursor.getColumnIndex("itemLocationId")));
                    locationBean.setItemLocationName(cursor.getString(cursor.getColumnIndex("itemLocationName")));

                    int parenLocationId=cursor.getInt(cursor.getColumnIndex("itemParentLocationId"));
                    if(parenLocationId>0) {
                        ItemLocationBean parentBean=new ItemLocationBean();
                        parentBean.setItemLocationId(parenLocationId);
                        locationBean.setItemLocationParent(parentBean);
                    }

                    locationBean.setItemLocationEpcId(cursor.getString(cursor.getColumnIndex("itemLocationEpcId")));
                    locationBean.setItemLocationBarCodeID(cursor.getString(cursor.getColumnIndex("itemLocationBarCodeID")));

                    inventoryView.setItemBeanSelected(false);
                    inventoryView.setLastItemLocationBean(locationBean);
                    list.add(inventoryView);
                }while (cursor.moveToNext());
            }

            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }

    public List<InventoryView> getItemDetailsScannedByEpcs(StringBuffer sb,int createdBy)
    {
        SQLiteDatabase db = this.getReadableDatabase();

        List<InventoryView> list=new ArrayList<InventoryView>();
        try
        {
            //System.out.println(sb.toString());
            Loger.error("TAGGGGGGGGG",sb.toString());
            String selectQuery = " SELECT  * FROM ItemDetails idm left outer join ItemLocation ilm on idm.lastLocationID=ilm.itemLocationId where idm.itemEpcId in("+sb.toString()+")";
           // String selectQuery = " SELECT  idm.itemId,idm.itemCode,idm.description,idm.itemParentID, idm.purchaseDate,idm.warrantyDate,idm.itemEpcId,idm.itemBarCodeID,idm.remark,idm.lastLocationID,idm.currentLocationId,idm.status,idm.manufactuare,idm.price,idm.createdBy,idm.createdOn,idm.modifyBy,idm.modifyOn,idm.active,idm.newItemReg FROM ItemDetails idm left outer join ItemLocation ilm on idm.lastLocationID=ilm.itemLocationId where idm.itemEpcId in("+sb.toString()+")";


            //System.out.println("selectQuery"+selectQuery);
            Cursor cursor = db.rawQuery(selectQuery, null);
            if(cursor.moveToFirst())
            {


                do
                {
                    InventoryView inventoryView=new InventoryView();
                    ItemDetailsBean bean1=new ItemDetailsBean();
                    ItemLocationBean locationBean=new ItemLocationBean();
                    bean1.setItemId(cursor.getInt(cursor.getColumnIndex("itemId")));
                    bean1.setItemCode(cursor.getString(cursor.getColumnIndex("itemCode")));
                    bean1.setDescription(cursor.getString(cursor.getColumnIndex("description")));

                    int itemParentId=cursor.getInt(cursor.getColumnIndex("itemParentID"));
                    if(itemParentId>0) {
                        ItemDetailsBean parentBean=new ItemDetailsBean();
                        parentBean.setItemId(itemParentId);
                        bean1.setItemParent(parentBean);

                    }else{
                        bean1.setItemParent(null);
                    }

                    bean1.setPurchaseDate(cursor.getString(cursor.getColumnIndex("purchaseDate")));
                    bean1.setWarrantyDate(cursor.getString(cursor.getColumnIndex("warrantyDate")));
                    bean1.setItemEpcId(cursor.getString(cursor.getColumnIndex("itemEpcId")));
                    bean1.setItemBarCodeID(cursor.getString(cursor.getColumnIndex("itemBarCodeID")));
                    bean1.setRemark(cursor.getString(cursor.getColumnIndex("remark")));
                    bean1.setLastLocationID(cursor.getInt(cursor.getColumnIndex("lastLocationID")));
                    bean1.setCurrentLocationId(cursor.getInt(cursor.getColumnIndex("currentLocationId")));

                    bean1.setStatus(cursor.getString(cursor.getColumnIndex("status")));
                    bean1.setManufactuare(cursor.getString(cursor.getColumnIndex("manufactuare")));
                    bean1.setPrice(cursor.getString(cursor.getColumnIndex("price")));

                    bean1.setCreatedBy(cursor.getInt(cursor.getColumnIndex("createdBy")));
                    bean1.setCreatedOn(cursor.getString(15));
                    bean1.setModifyBy(cursor.getInt(cursor.getColumnIndex("modifyBy")));
                    bean1.setModifyOn(cursor.getString(17));
                    bean1.setActive(cursor.getString(cursor.getColumnIndex("active")));
                    bean1.setNewItemReg(cursor.getString(cursor.getColumnIndex("newItemReg")));
                    bean1.setSectionNumber(cursor.getString(cursor.getColumnIndex("sectionNumber")));
                    bean1.setDateOfAssetDepositeUTC(cursor.getString(cursor.getColumnIndex("dateOfAssetDeposite")));
                    bean1.setNameOFAssetDepositorDate(cursor.getString(cursor.getColumnIndex("nameOFAssetDepositorDate")));
                    bean1.setComplaintBy(cursor.getString(cursor.getColumnIndex("complaintBy")));
                    bean1.setCriminalName(cursor.getString(cursor.getColumnIndex("criminalName")));
                    bean1.setAssetDescription(cursor.getString(cursor.getColumnIndex("assetDescription")));
                    bean1.setCourtRemarkAssetPrezenty(cursor.getString(cursor.getColumnIndex("courtRemarkAssetPrezenty")));
                    bean1.setCourtCommandNumberDate(cursor.getString(cursor.getColumnIndex("courtCommandNumberDate")));
                    bean1.setAssetExit(cursor.getString(cursor.getColumnIndex("assetExit")));
                    bean1.setNameOfAssetCollect(cursor.getString(cursor.getColumnIndex("nameOfAssetCollect")));
                    bean1.setDateOfAssetCollectUTC(cursor.getString(cursor.getColumnIndex("dateOfAssetCollect")));

                    inventoryView.setItemDetailsBean(bean1);

                    locationBean.setItemLocationId(cursor.getInt(cursor.getColumnIndex("itemLocationId")));
                    locationBean.setItemLocationName(cursor.getString(cursor.getColumnIndex("itemLocationName")));

                    int parenLocationId=cursor.getInt(cursor.getColumnIndex("itemParentLocationId"));
                    if(parenLocationId>0) {
                        ItemLocationBean parentBean=new ItemLocationBean();
                        parentBean.setItemLocationId(parenLocationId);
                        locationBean.setItemLocationParent(parentBean);
                    }

                    locationBean.setItemLocationEpcId(cursor.getString(cursor.getColumnIndex("itemLocationEpcId")));
                    locationBean.setItemLocationBarCodeID(cursor.getString(cursor.getColumnIndex("itemLocationBarCodeID")));
                    inventoryView.setCreatedBy(createdBy);
                    inventoryView.setCreatedOnUtc(Common.getUtcFormat(new Date()));
                    inventoryView.setItemBeanSelected(true);
                    inventoryView.setLastItemLocationBean(locationBean);
                    list.add(inventoryView);
                }while (cursor.moveToNext());
            }

            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }



    public List<InventoryView> getItemDetailsNewByLastLocation(int locationId)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        List<InventoryView> list=new ArrayList<InventoryView>();
        try
        {

            String selectQuery = " SELECT  * FROM ItemDetails idm left outer join ItemLocation ilm on   idm.currentLocationId=ilm.itemLocationId  where idm.currentLocationId='"+locationId+"' and idm.lastLocationID!=idm.currentLocationId ";
            //  String selectQuery = " SELECT  * FROM ItemDetails idm left outer join ItemLocation ilm on idm.lastLocationID=ilm.itemLocationId where lastLocationID='"+locationID+"' and lastLocationID!=currentLocationId";
            Cursor cursor = db.rawQuery(selectQuery, null);
            if(cursor.moveToFirst())
            {


                do
                {
                    InventoryView inventoryView=new InventoryView();
                    ItemDetailsBean bean1=new ItemDetailsBean();
                    ItemLocationBean locationBean=new ItemLocationBean();
                    bean1.setItemId(cursor.getInt(cursor.getColumnIndex("itemId")));
                    bean1.setItemCode(cursor.getString(cursor.getColumnIndex("itemCode")));
                    bean1.setDescription(cursor.getString(cursor.getColumnIndex("description")));

                    int itemParentId=cursor.getInt(cursor.getColumnIndex("itemParentID"));
                    if(itemParentId>0) {
                        ItemDetailsBean parentBean=new ItemDetailsBean();
                        parentBean.setItemId(itemParentId);
                        bean1.setItemParent(parentBean);

                    }else{
                        bean1.setItemParent(null);
                    }



                    bean1.setPurchaseDate(cursor.getString(cursor.getColumnIndex("purchaseDate")));
                    bean1.setWarrantyDate(cursor.getString(cursor.getColumnIndex("warrantyDate")));
                    bean1.setItemEpcId(cursor.getString(cursor.getColumnIndex("itemEpcId")));
                    bean1.setItemBarCodeID(cursor.getString(cursor.getColumnIndex("itemBarCodeID")));
                    bean1.setRemark(cursor.getString(cursor.getColumnIndex("remark")));
                    bean1.setLastLocationID(cursor.getInt(cursor.getColumnIndex("lastLocationID")));
                    bean1.setCurrentLocationId(cursor.getInt(cursor.getColumnIndex("currentLocationId")));

                    bean1.setStatus(cursor.getString(cursor.getColumnIndex("status")));
                    bean1.setManufactuare(cursor.getString(cursor.getColumnIndex("manufactuare")));
                    bean1.setPrice(cursor.getString(cursor.getColumnIndex("price")));

                    bean1.setCreatedBy(cursor.getInt(cursor.getColumnIndex("createdBy")));
                    bean1.setCreatedOn(cursor.getString(cursor.getColumnIndex("createdOn")));
                    bean1.setModifyBy(cursor.getInt(cursor.getColumnIndex("modifyBy")));
                    bean1.setModifyOn(cursor.getString(cursor.getColumnIndex("modifyOn")));
                    bean1.setActive(cursor.getString(cursor.getColumnIndex("active")));
                    bean1.setNewItemReg(cursor.getString(cursor.getColumnIndex("newItemReg")));
                    bean1.setSectionNumber(cursor.getString(cursor.getColumnIndex("sectionNumber")));
                    bean1.setDateOfAssetDepositeUTC(cursor.getString(cursor.getColumnIndex("dateOfAssetDeposite")));
                    bean1.setNameOFAssetDepositorDate(cursor.getString(cursor.getColumnIndex("nameOFAssetDepositorDate")));
                    bean1.setComplaintBy(cursor.getString(cursor.getColumnIndex("complaintBy")));
                    bean1.setCriminalName(cursor.getString(cursor.getColumnIndex("criminalName")));
                    bean1.setAssetDescription(cursor.getString(cursor.getColumnIndex("assetDescription")));
                    bean1.setCourtRemarkAssetPrezenty(cursor.getString(cursor.getColumnIndex("courtRemarkAssetPrezenty")));
                    bean1.setCourtCommandNumberDate(cursor.getString(cursor.getColumnIndex("courtCommandNumberDate")));
                    bean1.setAssetExit(cursor.getString(cursor.getColumnIndex("assetExit")));
                    bean1.setNameOfAssetCollect(cursor.getString(cursor.getColumnIndex("nameOfAssetCollect")));
                    bean1.setDateOfAssetCollectUTC(cursor.getString(cursor.getColumnIndex("dateOfAssetCollect")));

                    inventoryView.setItemDetailsBean(bean1);

                    locationBean.setItemLocationId(cursor.getInt(cursor.getColumnIndex("itemLocationId")));
                    locationBean.setItemLocationName(cursor.getString(cursor.getColumnIndex("itemLocationName")));

                    int parenLocationId=cursor.getInt(cursor.getColumnIndex("itemParentLocationId"));
                    if(parenLocationId>0) {
                        ItemLocationBean parentBean=new ItemLocationBean();
                        parentBean.setItemLocationId(parenLocationId);
                        locationBean.setItemLocationParent(parentBean);
                    }

                    locationBean.setItemLocationEpcId(cursor.getString(cursor.getColumnIndex("itemLocationEpcId")));
                    locationBean.setItemLocationBarCodeID(cursor.getString(cursor.getColumnIndex("itemLocationBarCodeID")));

                    inventoryView.setItemBeanSelected(true);
                    inventoryView.setLastItemLocationBean(locationBean);
                    list.add(inventoryView);
                }while (cursor.moveToNext());
            }

            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }

    public List<ItemDetailsBean> getItemDetailsIsReg(ItemDetailsBean bean)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        List<ItemDetailsBean> list=null;
        try
        {

            String selectQuery = " SELECT  * FROM ItemDetails where  active='true' and itemEpcId ='"+ bean.getItemEpcId()+"'";

            Cursor cursor = db.rawQuery(selectQuery, null);
            if(cursor.moveToFirst())
            {
                list=new ArrayList<ItemDetailsBean>();
                do
                {
                    ItemDetailsBean bean1=new ItemDetailsBean();
                    bean1.setItemId(cursor.getInt(cursor.getColumnIndex("itemId")));
                    bean1.setItemCode(cursor.getString(cursor.getColumnIndex("itemCode")));

                    bean1.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                    int itemParentId=cursor.getInt(cursor.getColumnIndex("itemParentID"));
                    if(itemParentId>0) {
                        ItemDetailsBean parentBean=new ItemDetailsBean();
                        parentBean.setItemId(itemParentId);
                        bean1.setItemParent(parentBean);

                    }else{
                        bean1.setItemParent(null);
                    }
                    bean1.setPurchaseDate(cursor.getString(cursor.getColumnIndex("purchaseDate")));
                    bean1.setWarrantyDate(cursor.getString(cursor.getColumnIndex("warrantyDate")));
                    bean1.setItemEpcId(cursor.getString(cursor.getColumnIndex("itemEpcId")));

                    bean1.setItemBarCodeID(cursor.getString(cursor.getColumnIndex("itemBarCodeID")));
                    bean1.setRemark(cursor.getString(cursor.getColumnIndex("remark")));
                    bean1.setLastLocationID(cursor.getInt(cursor.getColumnIndex("lastLocationID")));
                    bean1.setCurrentLocationId(cursor.getInt(cursor.getColumnIndex("currentLocationId")));
                    bean1.setStatus(cursor.getString(cursor.getColumnIndex("status")));
                    bean1.setManufactuare(cursor.getString(cursor.getColumnIndex("manufactuare")));
                    bean1.setPrice(cursor.getString(cursor.getColumnIndex("price")));
                    bean1.setCreatedBy(cursor.getInt(cursor.getColumnIndex("createdBy")));
                    bean1.setCreatedOn(cursor.getString(cursor.getColumnIndex("createdOn")));
                    bean1.setModifyBy(cursor.getInt(cursor.getColumnIndex("modifyBy")));
                    bean1.setModifyOn(cursor.getString(cursor.getColumnIndex("modifyOn")));
                    bean1.setActive(cursor.getString(cursor.getColumnIndex("active")));
                    bean1.setNewItemReg(cursor.getString(cursor.getColumnIndex("newItemReg")));
                    bean1.setSectionNumber(cursor.getString(cursor.getColumnIndex("sectionNumber")));
                    bean1.setDateOfAssetDepositeUTC(cursor.getString(cursor.getColumnIndex("dateOfAssetDeposite")));
                    bean1.setNameOFAssetDepositorDate(cursor.getString(cursor.getColumnIndex("nameOFAssetDepositorDate")));
                    bean1.setComplaintBy(cursor.getString(cursor.getColumnIndex("complaintBy")));
                    bean1.setCriminalName(cursor.getString(cursor.getColumnIndex("criminalName")));
                    bean1.setAssetDescription(cursor.getString(cursor.getColumnIndex("assetDescription")));
                    bean1.setCourtRemarkAssetPrezenty(cursor.getString(cursor.getColumnIndex("courtRemarkAssetPrezenty")));
                    bean1.setCourtCommandNumberDate(cursor.getString(cursor.getColumnIndex("courtCommandNumberDate")));
                    bean1.setAssetExit(cursor.getString(cursor.getColumnIndex("assetExit")));
                    bean1.setNameOfAssetCollect(cursor.getString(cursor.getColumnIndex("nameOfAssetCollect")));
                    bean1.setDateOfAssetCollectUTC(cursor.getString(cursor.getColumnIndex("dateOfAssetCollect")));
                    list.add(bean1);
                }while (cursor.moveToNext());
            }

            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }

    public List<ItemDetailsBean> getItemDetailsByItemCode(ItemDetailsBean bean)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        List<ItemDetailsBean> list=null;
        try
        {
            String selectQuery="";
            if(bean.getItemCode()!=null && bean.getItemCode().length()>0)
            {
                selectQuery = " SELECT  * FROM ItemDetails where  active='true' and itemCode like '%"+ bean.getItemCode()+"%' LIMIT 10 ";
            }else
            {
                selectQuery = " SELECT  * FROM ItemDetails where  active='true' and (length(itemEpcId)<=20 or itemEpcId is null) LIMIT 10 ";
            }

            Cursor cursor = db.rawQuery(selectQuery, null);

            if(cursor.moveToFirst())
            {
                list=new ArrayList<ItemDetailsBean>();
                do
                {
                    ItemDetailsBean bean1=new ItemDetailsBean();
                    bean1.setItemId(cursor.getInt(cursor.getColumnIndex("itemId")));
                    bean1.setItemCode(cursor.getString(cursor.getColumnIndex("itemCode")));
                    bean1.setDescription(cursor.getString(cursor.getColumnIndex("description")));
                    int itemParentId=cursor.getInt(cursor.getColumnIndex("itemParentID"));
                    if(itemParentId>0) {
                        ItemDetailsBean parentBean=new ItemDetailsBean();
                        parentBean.setItemId(itemParentId);
                        bean1.setItemParent(parentBean);

                    }else{
                        bean1.setItemParent(null);
                    }
                    bean1.setPurchaseDate(cursor.getString(cursor.getColumnIndex("purchaseDate")));
                    bean1.setWarrantyDate(cursor.getString(cursor.getColumnIndex("warrantyDate")));

                    bean1.setItemEpcId(cursor.getString(cursor.getColumnIndex("itemEpcId")));
                    bean1.setItemBarCodeID(cursor.getString(cursor.getColumnIndex("itemBarCodeID")));
                    bean1.setRemark(cursor.getString(cursor.getColumnIndex("remark")));
                    bean1.setLastLocationID(cursor.getInt(cursor.getColumnIndex("lastLocationID")));
                    bean1.setCurrentLocationId(cursor.getInt(cursor.getColumnIndex("currentLocationId")));
                    bean1.setStatus(cursor.getString(cursor.getColumnIndex("status")));
                    bean1.setManufactuare(cursor.getString(cursor.getColumnIndex("manufactuare")));
                    bean1.setPrice(cursor.getString(cursor.getColumnIndex("price")));
                    bean1.setCreatedBy(cursor.getInt(cursor.getColumnIndex("createdBy")));
                    bean1.setCreatedOn(cursor.getString(cursor.getColumnIndex("createdOn")));
                    bean1.setModifyBy(cursor.getInt(cursor.getColumnIndex("modifyBy")));
                    bean1.setModifyOn(cursor.getString(cursor.getColumnIndex("modifyOn")));
                    bean1.setActive(cursor.getString(cursor.getColumnIndex("active")));
                    bean1.setNewItemReg(cursor.getString(cursor.getColumnIndex("newItemReg")));
                    bean1.setSectionNumber(cursor.getString(cursor.getColumnIndex("sectionNumber")));
                    bean1.setDateOfAssetDepositeUTC(cursor.getString(cursor.getColumnIndex("dateOfAssetDeposite")));
                    bean1.setNameOFAssetDepositorDate(cursor.getString(cursor.getColumnIndex("nameOFAssetDepositorDate")));
                    bean1.setComplaintBy(cursor.getString(cursor.getColumnIndex("complaintBy")));
                    bean1.setCriminalName(cursor.getString(cursor.getColumnIndex("criminalName")));
                    bean1.setAssetDescription(cursor.getString(cursor.getColumnIndex("assetDescription")));
                    bean1.setCourtRemarkAssetPrezenty(cursor.getString(cursor.getColumnIndex("courtRemarkAssetPrezenty")));
                    bean1.setCourtCommandNumberDate(cursor.getString(cursor.getColumnIndex("courtCommandNumberDate")));
                    bean1.setAssetExit(cursor.getString(cursor.getColumnIndex("assetExit")));
                    bean1.setNameOfAssetCollect(cursor.getString(cursor.getColumnIndex("nameOfAssetCollect")));
                    bean1.setDateOfAssetCollectUTC(cursor.getString(cursor.getColumnIndex("dateOfAssetCollect")));
                    list.add(bean1);
                }while (cursor.moveToNext());
            }

            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }



    public boolean addItemLocation(ItemLocationBean bean)
    {
        boolean success=false;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("itemLocationId", bean.getItemLocationId());
            values.put("itemLocationName", bean.getItemLocationName());
            if(bean.getItemLocationParent()!=null)
                values.put("itemParentLocationId", bean.getItemLocationParent().getItemLocationId());
            else
                values.put("itemParentLocationId", 0);
            values.put("itemLocationEpcId", bean.getItemLocationEpcId());
            values.put("itemLocationBarCodeID", bean.getItemLocationBarCodeID());
            values.put("createdBy", bean.getCreatedBy());
            values.put("createdOn", bean.getCreatedOn());
            values.put("modifyBy", bean.getModifyBy());
            values.put("modifyOn", bean.getModifyOn());
            values.put("active", bean.getActive());
            db.insert("ItemLocation", null, values);
            success=true;
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
    public boolean addItemLocationList(List<ItemLocationBean> beans)
    {
        boolean success=false;
        try
        {
            if(beans!=null && beans.size()>0) {
                deleteTable("ItemLocation");
                SQLiteDatabase db = this.getWritableDatabase();
                db.beginTransaction();
                ContentValues values = null;
                for (ItemLocationBean bean : beans) {
                    values = new ContentValues();
                    values.put("itemLocationId", bean.getItemLocationId());
                    values.put("itemLocationName", bean.getItemLocationName());
                    if(bean.getItemLocationParent()!=null)
                        values.put("itemParentLocationId", bean.getItemLocationParent().getItemLocationId());
                    else
                        values.put("itemParentLocationId", 0);
                    values.put("itemLocationEpcId", bean.getItemLocationEpcId());
                    values.put("itemLocationBarCodeID", bean.getItemLocationBarCodeID());
                    values.put("createdBy", bean.getCreatedBy());
                    values.put("createdOn", bean.getCreatedOn());
                    values.put("modifyBy", bean.getModifyBy());
                    values.put("modifyOn", bean.getModifyOn());
                    values.put("active", bean.getActive());
                    values.put("newItemLocationReg", bean.getNewItemLocationReg());

                    db.insert("ItemLocation", null, values);
                    values = null;
                    bean = null;
                }
                db.setTransactionSuccessful();
                db.endTransaction();
                db.close();
                success = true;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
    public int updateItemLocation(ItemLocationBean bean)
    {
        int success=-1;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("itemLocationName", bean.getItemLocationName());
            if(bean.getItemLocationParent()!=null)
                values.put("itemParentLocationId", bean.getItemLocationParent().getItemLocationId());
            else
                values.put("itemParentLocationId", 0);
            values.put("itemLocationEpcId", bean.getItemLocationEpcId());
            values.put("itemLocationBarCodeID", bean.getItemLocationBarCodeID());
            values.put("createdBy", bean.getCreatedBy());
            values.put("createdOn", bean.getCreatedOn());
            values.put("modifyBy", bean.getModifyBy());
            values.put("modifyOn", bean.getModifyOn());
            values.put("active", bean.getActive());
            values.put("newItemLocationReg", bean.getNewItemLocationReg());
            success=db.update("ItemLocation", values,"itemLocationId=?",   new String[] { String.valueOf(bean.getItemLocationId()) } );
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
    public boolean deleteItemLocation(ItemLocationBean bean)
    {
        boolean success=false;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete("ItemLocation","itemLocationId=?",   new String[] { String.valueOf(bean.getItemLocationId()) } );
            success=true;
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }

    public List<ItemLocationBean> getItemLocationreg()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        List<ItemLocationBean> list=null;
        try
        {

            String selectQuery = " SELECT  * FROM ItemLocation where newItemLocationReg='Y'";
            Cursor cursor = db.rawQuery(selectQuery, null);
            if(cursor.moveToFirst())
            {
                list=new ArrayList<ItemLocationBean>();
                do
                {
                    ItemLocationBean bean1=new ItemLocationBean();
                    bean1.setItemLocationId(cursor.getInt(cursor.getColumnIndex("itemLocationId")));
                    bean1.setItemLocationName(cursor.getString(cursor.getColumnIndex("itemLocationName")));

                    int parenLocationId=cursor.getInt(cursor.getColumnIndex("itemParentLocationId"));
                    if(parenLocationId>0) {
                        ItemLocationBean parentBean=new ItemLocationBean();
                        parentBean.setItemLocationId(parenLocationId);
                        bean1.setItemLocationParent(parentBean);
                    }
                    bean1.setItemLocationEpcId(cursor.getString(cursor.getColumnIndex("itemLocationEpcId")));
                    bean1.setItemLocationBarCodeID(cursor.getString(cursor.getColumnIndex("itemLocationBarCodeID")));
                    bean1.setCreatedBy(cursor.getInt(cursor.getColumnIndex("createdBy")));
                    bean1.setCreatedOn(cursor.getString(cursor.getColumnIndex("createdOn")));
                    bean1.setModifyBy(cursor.getInt(cursor.getColumnIndex("modifyBy")));
                    bean1.setModifyOn(cursor.getString(cursor.getColumnIndex("modifyOn")));
                    bean1.setActive(cursor.getString(cursor.getColumnIndex("active")));
                    bean1.setNewItemLocationReg(cursor.getString(cursor.getColumnIndex("newItemLocationReg")));
                    list.add(bean1);
                }while (cursor.moveToNext());
            }

            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }


    public List<ItemLocationBean> getItemLocationByLocationName(ItemLocationBean bean,String type)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        List<ItemLocationBean> list=null;
        try
        {

            String selectQuery="";
            if(bean.getItemLocationName()!=null)
            {
                selectQuery = " SELECT  * FROM ItemLocation where  active='true' and itemLocationName like '%"+ bean.getItemLocationName()+"%' limit 10 ";
            }else
            {
                if(type.equals("inventory_menu")) {
                    selectQuery = " SELECT  * FROM ItemLocation where  active='true'  limit 10 ";
                }else
                {
                    selectQuery = " SELECT  * FROM ItemLocation where  active='true' and (length(itemLocationEpcId)<=20 or itemLocationEpcId is null) limit 10 ";
                }
            }

            Cursor cursor = db.rawQuery(selectQuery, null);
            if(cursor.moveToFirst())
            {
                list=new ArrayList<ItemLocationBean>();
                do
                {
                    ItemLocationBean bean1=new ItemLocationBean();
                    bean1.setItemLocationId(cursor.getInt(cursor.getColumnIndex("itemLocationId")));
                    bean1.setItemLocationName(cursor.getString(cursor.getColumnIndex("itemLocationName")));
                    int parenLocationId=cursor.getInt(cursor.getColumnIndex("itemParentLocationId"));
                    if(parenLocationId>0) {
                        ItemLocationBean parentBean=new ItemLocationBean();
                        parentBean.setItemLocationId(parenLocationId);
                        bean1.setItemLocationParent(parentBean);
                    }
                    bean1.setItemLocationEpcId(cursor.getString(cursor.getColumnIndex("itemLocationEpcId")));
                    bean1.setItemLocationBarCodeID(cursor.getString(cursor.getColumnIndex("itemLocationBarCodeID")));
                    bean1.setCreatedBy(cursor.getInt(cursor.getColumnIndex("createdBy")));
                    bean1.setCreatedOn(cursor.getString(cursor.getColumnIndex("createdOn")));
                    bean1.setModifyBy(cursor.getInt(cursor.getColumnIndex("modifyBy")));
                    bean1.setModifyOn(cursor.getString(cursor.getColumnIndex("modifyOn")));
                    bean1.setActive(cursor.getString(cursor.getColumnIndex("active")));
                    bean1.setNewItemLocationReg(cursor.getString(cursor.getColumnIndex("newItemLocationReg")));
                    list.add(bean1);
                }while (cursor.moveToNext());
            }

            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }

    public List<ItemLocationBean> getItemLocationIsReg(ItemLocationBean bean)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        List<ItemLocationBean> list=null;
        try
        {

            String selectQuery = " SELECT  * FROM ItemLocation where  active='true' and itemLocationEpcId = '"+ bean.getItemLocationEpcId()+"' ";
            Cursor cursor = db.rawQuery(selectQuery, null);
            if(cursor.moveToFirst())
            {
                list=new ArrayList<ItemLocationBean>();
                do
                {
                    ItemLocationBean bean1=new ItemLocationBean();
                    bean1.setItemLocationId(cursor.getInt(cursor.getColumnIndex("itemLocationId")));
                    bean1.setItemLocationName(cursor.getString(cursor.getColumnIndex("itemLocationName")));
                    int parenLocationId=cursor.getInt(cursor.getColumnIndex("itemParentLocationId"));
                    if(parenLocationId>0) {
                        ItemLocationBean parentBean=new ItemLocationBean();
                        parentBean.setItemLocationId(parenLocationId);
                        bean1.setItemLocationParent(parentBean);
                    }
                    bean1.setItemLocationEpcId(cursor.getString(cursor.getColumnIndex("itemLocationEpcId")));
                    bean1.setItemLocationBarCodeID(cursor.getString(cursor.getColumnIndex("itemLocationBarCodeID")));
                    bean1.setCreatedBy(cursor.getInt(cursor.getColumnIndex("createdBy")));
                    bean1.setCreatedOn(cursor.getString(cursor.getColumnIndex("createdOn")));
                    bean1.setModifyBy(cursor.getInt(cursor.getColumnIndex("modifyBy")));
                    bean1.setModifyOn(cursor.getString(cursor.getColumnIndex("modifyOn")));
                    bean1.setActive(cursor.getString(cursor.getColumnIndex("active")));
                    bean1.setNewItemLocationReg(cursor.getString(cursor.getColumnIndex("newItemLocationReg")));
                    list.add(bean1);
                }while (cursor.moveToNext());
            }

            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }


    public ItemLocationBean getItemLocationById(ItemLocationBean bean)
    {
        SQLiteDatabase db = this.getReadableDatabase();
         ItemLocationBean bean1=null;
        try
        {

            String selectQuery = " SELECT  * FROM ItemLocation where itemLocationId = '"+ bean.getItemLocationId()+"' ";
            Cursor cursor = db.rawQuery(selectQuery, null);
            if(cursor.moveToFirst())
            {

                do
                {
                    bean1=new ItemLocationBean();
                    bean1.setItemLocationId(cursor.getInt(cursor.getColumnIndex("itemLocationId")));
                    bean1.setItemLocationName(cursor.getString(cursor.getColumnIndex("itemLocationName")));
                    int parenLocationId=cursor.getInt(cursor.getColumnIndex("itemParentLocationId"));
                    if(parenLocationId>0) {
                        ItemLocationBean parentBean=new ItemLocationBean();
                        parentBean.setItemLocationId(parenLocationId);
                        bean1.setItemLocationParent(parentBean);
                    }
                    bean1.setItemLocationEpcId(cursor.getString(cursor.getColumnIndex("itemLocationEpcId")));
                    bean1.setItemLocationBarCodeID(cursor.getString(cursor.getColumnIndex("itemLocationBarCodeID")));
                    bean1.setCreatedBy(cursor.getInt(cursor.getColumnIndex("createdBy")));
                    bean1.setCreatedOn(cursor.getString(cursor.getColumnIndex("createdOn")));
                    bean1.setModifyBy(cursor.getInt(cursor.getColumnIndex("modifyBy")));
                    bean1.setModifyOn(cursor.getString(cursor.getColumnIndex("modifyOn")));
                    bean1.setActive(cursor.getString(cursor.getColumnIndex("active")));
                    bean1.setNewItemLocationReg(cursor.getString(cursor.getColumnIndex("newItemLocationReg")));

                }while (cursor.moveToNext());
            }

            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return bean1;
    }


    public long deleteTable(String tableName) {
        int returnResult=0;

        try{
            SQLiteDatabase mDb = this.getWritableDatabase();
            returnResult=mDb.delete(tableName, null, null);
            if(returnResult>0)
            {
                System.out.println("delete data from "+tableName);
            }
            this.close();
        }catch(Exception e)
        {
            e.printStackTrace();
        }
        return returnResult;

    }


    public boolean addLogin(List<LoginBean> beans)
    {
        boolean success=false;
        try
        {
            if(beans!=null && beans.size()>0) {
                deleteTable("Login");

                SQLiteDatabase db = this.getWritableDatabase();
                db.beginTransaction();
                ContentValues values = null;
                for (LoginBean bean : beans) {
                    values = new ContentValues();
                    values.put("loginId", bean.getLoginId());
                    values.put("userName", bean.getUserName());
                    values.put("userLoginId", bean.getUserLoginId());
                    values.put("password", bean.getPassword());
                    values.put("roleId", bean.getRoleId());
                    values.put("createdBy", bean.getCreatedBy());
                    values.put("createdOn", bean.getCreatedOn());
                    values.put("modifyBy", bean.getModifyBy());
                    values.put("modifyOn", bean.getModifyOn());
                    values.put("active", bean.getActive());
                    db.insert("Login", null, values);
                    values = null;
                    bean = null;
                }
                db.setTransactionSuccessful();
                db.endTransaction();
                db.close();
                success = true;
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
    public int updateLogin(LoginBean bean)
    {
        int success=-1;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("userName", bean.getUserName());
            values.put("userLoginId", bean.getUserLoginId());
            values.put("password", bean.getPassword());
            values.put("roleId", bean.getRoleId());
            values.put("createdBy", bean.getCreatedBy());
            values.put("createdOn", bean.getCreatedOn());
            values.put("modifyBy", bean.getModifyBy());
            values.put("modifyOn", bean.getModifyOn());
            values.put("active", bean.getActive());
            success=db.update("Login", values,"loginId=?",   new String[] { String.valueOf(bean.getLoginId()) } );
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
    public boolean deleteLogin(LoginBean bean)
    {
        boolean success=false;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete("Login","loginId=?",   new String[] { String.valueOf(bean.getLoginId()) } );
            success=true;
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }

    public List<LoginBean> getLogin(LoginBean bean)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        List<LoginBean> list=null;
        try
        {
            String selectQuery = " SELECT  * FROM Login where userLoginId='"+ bean.getUserLoginId()+"' and password='"+bean.getPassword()+"'";
            Cursor cursor = db.rawQuery(selectQuery, null);
            if(cursor.moveToFirst())
            {
                list=new ArrayList<LoginBean>();
                do
                {

                    LoginBean bean1=new LoginBean();
                    bean1.setLoginId(cursor.getInt(cursor.getColumnIndex("loginId")));
                    bean1.setUserName(cursor.getString(cursor.getColumnIndex("userName")));
                    bean1.setUserLoginId(cursor.getString(cursor.getColumnIndex("userLoginId")));
                    bean1.setPassword(cursor.getString(cursor.getColumnIndex("password")));
                    bean1.setRoleId(cursor.getInt(cursor.getColumnIndex("roleId")));
                    bean1.setCreatedBy(cursor.getInt(cursor.getColumnIndex("createdBy")));
                    bean1.setCreatedOn(cursor.getString(cursor.getColumnIndex("createdOn")));
                    bean1.setModifyBy(cursor.getInt(cursor.getColumnIndex("modifyBy")));
                    bean1.setModifyOn(cursor.getString(cursor.getColumnIndex("modifyOn")));
                    bean1.setActive(cursor.getString(cursor.getColumnIndex("active")));
                    list.add(bean1);
                    System.out.println("list.size()"+list.size());
                    bean1=null;
                }while (cursor.moveToNext());
            }

            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }


    public boolean addRoleType(RoleTypeBean bean)
    {
        boolean success=false;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("roleId", bean.getRoleId());
            values.put("roleName", bean.getRoleName());
            values.put("createdBy", bean.getCreatedBy());
            values.put("createdOn", bean.getCreatedOn());
            values.put("modifyBy", bean.getModifyBy());
            values.put("modifyOn", bean.getModifyOn());
            values.put("active", bean.getActive());
            db.insert("RoleType", null, values);
            success=true;
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
    public int updateRoleType(RoleTypeBean bean)
    {
        int success=-1;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("roleId", bean.getRoleId());
            values.put("roleName", bean.getRoleName());
            values.put("createdBy", bean.getCreatedBy());
            values.put("createdOn", bean.getCreatedOn());
            values.put("modifyBy", bean.getModifyBy());
            values.put("modifyOn", bean.getModifyOn());
            values.put("active", bean.getActive());
            success=db.update("RoleType", values,"roleId=?",   new String[] { String.valueOf(bean.getRoleId()) } );
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
    public boolean deleteRoleType(RoleTypeBean bean)
    {
        boolean success=false;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete("RoleType","roleId=?",   new String[] { String.valueOf(bean.getRoleId()) } );
            success=true;
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }

    public List<RoleTypeBean> getRoleType(RoleTypeBean bean)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        List<RoleTypeBean> list=null;
        try
        {

            String selectQuery = " SELECT  * FROM RoleType where roleId='"+ bean.getRoleId()+"'";
            Cursor cursor = db.rawQuery(selectQuery, null);
            if(cursor.moveToFirst())
            {
                list=new ArrayList<RoleTypeBean>();
                do
                {
                    RoleTypeBean bean1=new RoleTypeBean();

                    bean1.setRoleId(cursor.getInt(cursor.getColumnIndex("roleId")));
                    bean1.setRoleName(cursor.getString(cursor.getColumnIndex("roleName")));
                    bean1.setCreatedBy(cursor.getInt(cursor.getColumnIndex("createdBy")));
                    bean1.setCreatedOn(cursor.getString(cursor.getColumnIndex("createdOn")));
                    bean1.setModifyBy(cursor.getInt(cursor.getColumnIndex("modifyBy")));
                    bean1.setModifyOn(cursor.getString(cursor.getColumnIndex("modifyOn")));
                    bean1.setActive(cursor.getString(cursor.getColumnIndex("active")));
                    list.add(bean1);
                }while (cursor.moveToNext());
            }

            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }


    public boolean addTrackingInventory(List<InventoryView> beans,ItemLocationBean locationBean,TrackingSessionBean trackingSessionBean,String deviceSerialNo,String status)
    {
        boolean success=false;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.beginTransaction();
            ContentValues values = null;
            for(InventoryView inventoryView:beans) {
                if(inventoryView.isItemBeanSelected())
                {
                    values = new ContentValues();
                    values.put("trackingSessionId", trackingSessionBean.getTrackingSessionId());
                    values.put("itemLocationId", locationBean.getItemLocationId());
                    values.put("deviceId", 0);
                    values.put("status", status);//INVENTORY
                    values.put("createdBy", inventoryView.getCreatedBy());

                    values.put("modifyOn", inventoryView.getCreatedOnUtc());
                    values.put("createdOn", inventoryView.getCreatedOnUtc());
                    values.put("active", "true");
                    values.put("itemBarCodeID", inventoryView.getItemDetailsBean().getItemBarCodeID());
                    values.put("itemEpcId", inventoryView.getItemDetailsBean().getItemEpcId());
                    values.put("deviceSerialNo", deviceSerialNo);

                    values.put("itemId",inventoryView.getItemDetailsBean().getItemId());
                    db.insert("TrackingInventory", null, values);
                }
            }
            db.setTransactionSuccessful();
            db.endTransaction();
            db.close();
            values=null;
            success=true;
            Thread.sleep(100);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
    public int updateTrackingInventory(TrackingInventoryBean bean)
    {
        int success=-1;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("trackingSessionId", bean.getTrackingSessionId());
            values.put("itemLocationId", bean.getItemLocationId());
            values.put("deviceId", bean.getDeviceId());
            values.put("status", bean.getStatus());
            values.put("createdBy", bean.getCreatedBy());
            values.put("createdOn", bean.getCreatedOn());
            values.put("modifyBy", bean.getModifyBy());
            values.put("modifyOn", bean.getModifyOn());
            values.put("active", bean.getActive());

            success=db.update("TrackingInventory", values,"trackingInventoryId=?",   new String[] { String.valueOf(bean.getTrackingInventoryId()) } );
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
    public boolean deleteTrackingInventory(TrackingInventoryBean bean)
    {
        boolean success=false;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete("TrackingInventory","trackingInventoryId=?",   new String[] { String.valueOf(bean.getTrackingInventoryId()) } );
            success=true;
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }

    public List<TrackingInventoryBean> getTrackingInventory()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        List<TrackingInventoryBean> list=null;
        try
        {

            String selectQuery = " SELECT  * FROM TrackingInventory ";
            Cursor cursor = db.rawQuery(selectQuery, null);
            if(cursor.moveToFirst())
            {
                list=new ArrayList<TrackingInventoryBean>();
                do
                {

                    TrackingInventoryBean bean1=new TrackingInventoryBean();
                    bean1.setTrackingSessionId(cursor.getInt(cursor.getColumnIndex("trackingSessionId")));
                    bean1.setItemLocationId(cursor.getInt(cursor.getColumnIndex("itemLocationId")));
                    bean1.setDeviceId(cursor.getInt(cursor.getColumnIndex("deviceId")));
                    bean1.setStatus(cursor.getString(cursor.getColumnIndex("status")));
                    bean1.setCreatedBy(cursor.getInt(cursor.getColumnIndex("createdBy")));
                    bean1.setCreatedOn(cursor.getString(cursor.getColumnIndex("createdOn")));
//                    bean1.setModifyOn(cursor.getString(cursor.getColumnIndex("modifiedOn")));
                    bean1.setActive(cursor.getString(cursor.getColumnIndex("active")));
                    if(bean1.getActive()!=null && bean1.getActive().equals("1"))
                    {
                        bean1.setActive("true");
                    }
                    bean1.setDeviceSerialNo(cursor.getString(cursor.getColumnIndex("deviceSerialNo")));
                    bean1.setItemBarCodeID(cursor.getString(cursor.getColumnIndex("itemBarCodeID")));
                    bean1.setItemEpcId(cursor.getString(cursor.getColumnIndex("itemEpcId")));
                    bean1.setItemId(cursor.getInt(cursor.getColumnIndex("itemId")));

                    list.add(bean1);
                }while (cursor.moveToNext());
            }

            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }


    public boolean addTrackingSession(List<TrackingSessionBean> beans)
    {
        boolean success=false;
        try
        {
            if(beans!=null && beans.size()>0)
            {
                deleteTable("TrackingSession");
                SQLiteDatabase db = this.getWritableDatabase();
                db.beginTransaction();
                ContentValues values = null;
            for(TrackingSessionBean bean:beans)
            {
                values = new ContentValues();
                values.put("trackingSessionId", bean.getTrackingSessionId());
                values.put("trackingSessionName", bean.getTrackingSessionName());
                System.out.println("getTrackingSessionName"+bean.getTrackingSessionName());
                values.put("startDate", bean.getStartDate());
                values.put("endDate", bean.getEndDate());
                values.put("createdBy", bean.getCreatedBy());
                values.put("createdOn", bean.getCreatedOn());
                values.put("modifyBy", bean.getModifyBy());
                values.put("modifyOn", bean.getModifyOn());
                values.put("active", bean.getActive());
                db.insert("TrackingSession", null, values);
                values = null;
                bean = null;
            }
                db.setTransactionSuccessful();
                db.endTransaction();
                db.close();
                success=true;
        }

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
    public int updateTrackingSession(TrackingSessionBean bean)
    {
        int success=-1;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("trackingSessionName", bean.getTrackingSessionName());
            values.put("startDate", bean.getStartDate());
            values.put("endDate", bean.getEndDate());
            values.put("createdBy", bean.getCreatedBy());
            values.put("createdOn", bean.getCreatedOn());
            values.put("modifyBy", bean.getModifyBy());
            values.put("modifyOn", bean.getModifyOn());
            values.put("active", bean.getActive());
            success=db.update("TrackingSession", values,"trackingSessionId=?",   new String[] { String.valueOf(bean.getTrackingSessionId()) } );
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }
    public boolean deleteTrackingSession(TrackingSessionBean bean)
    {
        boolean success=false;
        try
        {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete("TrackingSession","trackingSessionId=?",   new String[] { String.valueOf(bean.getTrackingSessionId()) } );
            success=true;
            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return success;
    }

    public List<TrackingSessionBean> getTrackingSession()
    {
        SQLiteDatabase db = this.getReadableDatabase();
        List<TrackingSessionBean> list=null;
        try
        {

            String selectQuery = " SELECT  * FROM TrackingSession ";
            Cursor cursor = db.rawQuery(selectQuery, null);
            if(cursor.moveToFirst())
            {
                list=new ArrayList<TrackingSessionBean>();
                do
                {
                    TrackingSessionBean bean1=new TrackingSessionBean();
                    bean1.setTrackingSessionId(cursor.getInt(cursor.getColumnIndex("trackingSessionId")));
                    bean1.setTrackingSessionName(cursor.getString(cursor.getColumnIndex("trackingSessionName")));
                    bean1.setStartDate(cursor.getString(cursor.getColumnIndex("startDate")));
                    bean1.setEndDate(cursor.getString(cursor.getColumnIndex("endDate")));
                    bean1.setCreatedBy(cursor.getInt(cursor.getColumnIndex("createdBy")));
                    bean1.setCreatedOn(cursor.getString(cursor.getColumnIndex("createdOn")));
                    bean1.setModifyBy(cursor.getInt(cursor.getColumnIndex("modifyBy")));
                    bean1.setModifyOn(cursor.getString(cursor.getColumnIndex("modifyOn")));
                    bean1.setActive(cursor.getString(cursor.getColumnIndex("active")));
                    list.add(bean1);
                }while (cursor.moveToNext());
            }

            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }

    public Integer getCountByType(String type, int trackingSessionId)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        Integer count=0;
        try
        {

            String selectQuery =null;
            if(type.equals("REG_ASSET"))
            {
                selectQuery="select count(itemID) from ItemDetails where length(itemEpcID)>20 and active='true'";
            }else if(type.equals("TOTAL_ASSET"))
            {
                selectQuery="select count(itemID) from ItemDetails where itemParentID>0 and  active='true'";
            }else if(type.equals("REG_LOCATION"))
            {
                selectQuery="select count(itemLocationId) from ItemLocation where length(itemLocationEpcId)>20 and active='true'";
            }else if(type.equals("TOTAL_LOCATION"))
            {
                selectQuery="select count(itemLocationId) from ItemLocation where  itemParentLocationId >0 and active='true'";
            }else if(type.equals("TOTAL_INVENTORY"))
            {
                selectQuery="select count(distinct itemEpcId) from TrackingInventory where trackingSessionId='"+trackingSessionId+"'";
            }else if(type.equals("TOTAL_SERVER_INVENTORY"))
            {
                selectQuery="select count(distinct itemEpcId) from ItemDetails where currentLocationId>0 and  itemParentID >0 and itemEpcId not in( select  itemEpcId from TrackingInventory where trackingSessionId='"+trackingSessionId+"')";
            }

            Cursor cursor = db.rawQuery(selectQuery, null);
            if(cursor.moveToFirst())
            {

                do
                {
                    count=cursor.getInt(0);


                }while (cursor.moveToNext());
            }

            db.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return count;
    }
}