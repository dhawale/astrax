package com.hkcrfid.jewerly;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hkcrfid.bean.ItemDetailsBean;
import com.hkcrfid.bean.ItemLocationBean;
import com.hkcrfid.common.Common;
import com.hkcrfid.common.HKCRfid;
import com.hkcrfid.common.Loger;

import java.util.Date;
import java.util.List;


public class AssetRegistration extends AppCompatActivity {
    String mode = "";
    Context context = this;
    EditText inputSearch, selecttedAsset;
    ListView asset_list_view;
    ArrayAdapter<String> adapter;
    String[] assetList = {};
    List<ItemDetailsBean> listAsset = null;
    TextView epcIDTV, epcIDConfirmTV, detailsTV;
    ItemDetailsBean selecttedAssetBean = null;
    SqlLite sqlLite;
    Button regSaveBt, regClearBt;
    SharedPreferences sharedpreferences;
//    private Button btnGetTag;
    Handler listEpcHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            try {
                String[] epcList = msg.getData().getStringArray("epcList");
                if (epcList == null || epcList.length <= 0) {
                    Toast.makeText(getApplicationContext(), "No tag found", Toast.LENGTH_SHORT).show();
                } else if (mode.equals("epcReg1")) {
                    epcIDTV.setText(epcList[0]);
                    mode = "epcReg2";
                } else if (mode.equals("epcReg2")) {
                    epcIDConfirmTV.setText(epcList[0]);
                    mode = "";
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.asset_registration);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Asset Registration");
//        toolbar.setLogo(R.drawable.item_registration);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        sharedpreferences = getApplicationContext().getSharedPreferences(Common.appPreference, MODE_PRIVATE);
        sqlLite = new SqlLite(context);
        detailsTV = (TextView) findViewById(R.id.detailsTV);
        epcIDTV = (TextView) findViewById(R.id.epcIDTV);
//        btnGetTag= (Button) findViewById(R.id.btnGetTag);


        epcIDConfirmTV = (TextView) findViewById(R.id.epcIDConfirmTV);
        selecttedAsset = (EditText) findViewById(R.id.selecttedAsset);
        selecttedAsset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAssetSelection();
            }
        });

        regSaveBt = (Button) findViewById(R.id.regSaveBt);
        regSaveBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo save regi details
                //1 check tagid not registered with other asset
                //2 epc and coinfirm epc is same.

                if (selecttedAssetBean == null) {
                    Toast.makeText(getApplicationContext(), "Please select Item.", Toast.LENGTH_SHORT).show();
                } else if (epcIDConfirmTV.getText().toString().length() <= 20) {
                    Toast.makeText(getApplicationContext(), "Please Scan EPC.", Toast.LENGTH_SHORT).show();
                } else if (!epcIDConfirmTV.getText().toString().equals(epcIDTV.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "EPC not match", Toast.LENGTH_SHORT).show();
                } else {
                    ItemLocationBean itemLocationBean = new ItemLocationBean();
                    itemLocationBean.setItemLocationEpcId(epcIDConfirmTV.getText().toString());
                    List<ItemLocationBean> locationList = sqlLite.getItemLocationIsReg(itemLocationBean);
                    if (locationList != null && locationList.size() > 0) {
                        Toast.makeText(getApplicationContext(), "Not registered, EPC registered with Location " + locationList.get(0).getItemLocationName() + ".", Toast.LENGTH_LONG).show();
                    } else {
                        selecttedAssetBean.setItemEpcId(epcIDConfirmTV.getText().toString());
                        List<ItemDetailsBean> list = sqlLite.getItemDetailsIsReg(selecttedAssetBean);
                        if (list == null || list.size() <= 0) {
                            selecttedAssetBean.setNewItemReg("Y");
                            selecttedAssetBean.setCreatedOn(Common.getUtcFormat(new Date()));
                            selecttedAssetBean.setCreatedBy(sharedpreferences.getInt("loginId", 0));
                            sqlLite.updateItemDetails(selecttedAssetBean);
                            Toast.makeText(getApplicationContext(), "Successfully EPC registered with " + selecttedAssetBean.getItemCode(), Toast.LENGTH_SHORT).show();
                            selecttedAssetBean = null;
                            epcIDConfirmTV.setText("");
                            epcIDTV.setText("");
                            selecttedAsset.setText("");
                            detailsTV.setText("");
                        } else if (list.size() > 0) {
                            dialogUnregiterPreviousAssignOther(list.get(0));
                        }
                    }
                }


            }

        });

        regClearBt = (Button) findViewById(R.id.regClearBt);
        regClearBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //todo cleate tag details
                mode = "epcReg1";
                epcIDConfirmTV.setText("");
                epcIDTV.setText("");

            }
        });

//        btnGetTag.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mode = "epcReg2";
//                String epc[] = HKCRfid.startReading();
//                Message m = new Message();
//                Bundle b = new Bundle();
//                b.putStringArray("epcList", epc);
//                m.setData(b);
//                listEpcHandler.sendMessage(m);
//            }
//        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent myIntent = new Intent(AssetRegistration.this, HomeActivity.class);
        startActivity(myIntent);
        finish();
        return true;
    }

    public void dialogUnregiterPreviousAssignOther(final ItemDetailsBean itemDetailsBeanRegistered) {
        mode = "Reassign EPC";
        final Dialog dialog = new Dialog(context);
        TextView messageTV;
        final CheckBox unregiterPrevious;
        Button updateBT, cancelBT;
        dialog.setContentView(R.layout.unregister_previous_assign_other);
        dialog.setTitle("Reassign EPC!");
        messageTV = (TextView) dialog.findViewById(R.id.messageTV);
        unregiterPrevious = (CheckBox) dialog.findViewById(R.id.unregiterPrevious);
        updateBT = (Button) dialog.findViewById(R.id.updateBT);
        cancelBT = (Button) dialog.findViewById(R.id.cancelBT);
        messageTV.setText("Scanned EPC registered with " + itemDetailsBeanRegistered.getItemCode() + ".\n Do you want to reassign with " + selecttedAssetBean.getItemCode());


        updateBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (unregiterPrevious.isChecked()) {

                    itemDetailsBeanRegistered.setItemEpcId(null);
                    itemDetailsBeanRegistered.setNewItemReg("Y");
                    itemDetailsBeanRegistered.setModifyOn(Common.getUtcFormat(new Date()));
                    itemDetailsBeanRegistered.setModifyBy(sharedpreferences.getInt("loginId", 0));

                    sqlLite.updateItemDetails(itemDetailsBeanRegistered);
                    selecttedAssetBean.setCreatedOn(Common.getUtcFormat(new Date()));
                    selecttedAssetBean.setCreatedBy(sharedpreferences.getInt("loginId", 0));
                    sqlLite.updateItemDetails(selecttedAssetBean);
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Successfully EPC registered with " + selecttedAssetBean.getItemCode() + "\n and unregistered with " + itemDetailsBeanRegistered.getItemCode(), Toast.LENGTH_SHORT).show();
                    selecttedAssetBean = null;
                    epcIDConfirmTV.setText("");
                    epcIDTV.setText("");
                    selecttedAsset.setText("");
                    detailsTV.setText("");
                    mode = "";
                } else {
                    Toast.makeText(getApplicationContext(), "Please select unregiter previous!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        cancelBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void searchAsset() {
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                ItemDetailsBean itemDetailsBean = new ItemDetailsBean();
                itemDetailsBean.setItemCode(cs.toString());
                listAsset = sqlLite.getItemDetailsByItemCode(itemDetailsBean);
                if (listAsset != null) {
                    assetList = new String[listAsset.size()];
                    for (int i = 0; i < listAsset.size(); i++) {
                        assetList[i] = listAsset.get(i).getItemCode();
                    }

                    adapter = new ArrayAdapter<String>(context, R.layout.search_list_item, R.id.search_asset, assetList);
                    asset_list_view.setAdapter(adapter);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
       /*if(keyCode==67)

            DownloadServiceCourse.activity=AssetRegistration.this;
            Intent i = new Intent(Common.appPackage + ".synchronization").putExtra("action_type", "image");
            context.sendBroadcast  {(i);

        }else*/
        Loger.error("TAG", "KeyCode - " + keyCode);
        if (keyCode == 120) {
            String[] epc = HKCRfid.startReading();
            Message m = new Message();
            Bundle b = new Bundle();
            b.putStringArray("epcList", epc);
            m.setData(b);
            listEpcHandler.sendMessage(m);
        } else if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent myIntent = new Intent(AssetRegistration.this, HomeActivity.class);
            startActivity(myIntent);
            finish();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    public void dialogAssetSelection() {
        mode = "Search Asset";
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.search_dialog);
        dialog.setTitle("Search Asset");
        selecttedAssetBean = null;
        Button assetSelectionDone = (Button) dialog.findViewById(R.id.assetSelectionDone);
        asset_list_view = (ListView) dialog.findViewById(R.id.asset_list_view);
        //final Typeface customF = Typeface.createFromAsset(getApplicationContext().getAssets(), "mangalb.ttf");
        inputSearch = (EditText) dialog.findViewById(R.id.inputSearch);
        //inputSearch.setTypeface(customF);
        listAsset = sqlLite.getItemDetailsByItemCode(new ItemDetailsBean());
        if (listAsset != null) {
            assetList = new String[listAsset.size()];
            for (int i = 0; i < listAsset.size(); i++) {
                assetList[i] = listAsset.get(i).getItemCode();
            }
            adapter = new ArrayAdapter<String>(context, R.layout.search_list_item, R.id.search_asset, assetList);
            asset_list_view.setAdapter(adapter);
        }
        searchAsset();
        // Adding items to listview

        asset_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (listAsset != null && listAsset.size() > position) {
                    mode = "epcReg1";
                    selecttedAssetBean = listAsset.get(position);
                    selecttedAsset.setText("" + selecttedAssetBean.getItemCode());
                    detailsTV.setText(selecttedAssetBean.getDescription());
                }
                dialog.dismiss();
            }
        });


        // if button is clicked, close the custom dialog
        assetSelectionDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.registrationmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_asset_registration) {
            /*
            Intent myIntent = new Intent(AssetRegistration.this, AssetRegistration.class);
            startActivity(myIntent);
            finish();

            */
            return true;
        } else if (id == R.id.action_location_registration) {
            Intent myIntent = new Intent(AssetRegistration.this, LocationRegistration.class);
            startActivity(myIntent);
            finish();
            return true;
        } else if (id == R.id.action_UnRegistration) {
            Intent myIntent = new Intent(AssetRegistration.this, UnregistrationEpc.class);
            startActivity(myIntent);
            finish();
            return true;
        } else if (id == R.id.action_search_option) {
//            Intent myIntent = new Intent(AssetRegistration.this, SingleSearchActivity.class);
            Intent myIntent = new Intent(AssetRegistration.this, SearchAssetActivity.class);
            startActivity(myIntent);
            finish();
            return true;
        } else if (id == R.id.action_home) {
            Intent myIntent = new Intent(AssetRegistration.this, HomeActivity.class);
            startActivity(myIntent);
            finish();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

}

