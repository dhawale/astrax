package com.hkcrfid.jewerly;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hkcrfid.bean.InventoryView;
import com.hkcrfid.bean.ItemLocationBean;
import com.hkcrfid.bean.TrackingSessionBean;
import com.hkcrfid.common.Common;
import com.hkcrfid.common.HKCRfid;
import com.hkcrfid.common.Loger;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class InventoryActivity extends AppCompatActivity {
    Date readingDate=null;

    private boolean Scanflag = false, isStartReading = false, isAutoScanReading = false;
    private Timer timer;
    ListView list;
    SqlLite sqlLite;
    String mode = "";
    String modeList = "EXISTING";//SCANNED,NEW SCANNED
    Context context = this;
    TrackingSessionBean trackingSessionBean = null;
    List<TrackingSessionBean> listTrackingSession = null;
    String listLocationArray[] = null;
    List<ItemLocationBean> listLocation = null;
    String locationListArray[] = {};
    TextView tacView,  locationName, unscannedTvCount, scannedTvCount, newScannedTvCount;
    Button unscannedBt, scannedBt, newScanned;
    InventoryAdapter adapter;
    Activity activity = this;
    List<InventoryView> unScannedAssetList = new ArrayList<InventoryView>();
    List<InventoryView> scannedAssetList = new ArrayList<InventoryView>();
    List<InventoryView> newScannedAssetList = new ArrayList<InventoryView>();
    int   TOTAL_COUNT=0,TOTAL_SERVER_INVENTORY=0;
    HashMap<String, String> epcMap = new HashMap<String, String>();
    EditText inputSearch,selecttedLocation;
    ItemLocationBean selecttedLocationBean;
    ListView asset_list_view;
    ArrayAdapter<String> adapterLocation;
    Dialog dialogLocationScan;
    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inventory);
        sqlLite = new SqlLite(context);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Inventory");
        setSupportActionBar(toolbar);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        sharedpreferences = getApplicationContext().getSharedPreferences(Common.appPreference, MODE_PRIVATE);
        isAutoScanReading=sharedpreferences.getBoolean("isAutoScanReading",false);
        locationName = (TextView) findViewById(R.id.locationName);
        tacView = (TextView) findViewById(R.id.tacView);
        list = (ListView) findViewById(R.id.list);

        unscannedTvCount = (TextView) findViewById(R.id.unscannedTvCount);
        scannedTvCount = (TextView) findViewById(R.id.scannedTvCount);
        newScannedTvCount = (TextView) findViewById(R.id.newScannedTvCount);

        unscannedBt = (Button) findViewById(R.id.unscannedBt);
        unscannedBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 modeList = "EXISTING";//SCANNED,NEWSCANNED
                adapter = new InventoryAdapter(activity, unScannedAssetList, "UnScanned");
                list.setAdapter(adapter);
            }
        });
        scannedBt = (Button) findViewById(R.id.scannedBt);
        scannedBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 modeList = "SCANNED";//SCANNED,NEWSCANNED
                adapter = new InventoryAdapter(activity, scannedAssetList, "Scanned");
                list.setAdapter(adapter);
            }
        });
        newScanned = (Button) findViewById(R.id.newScanned);
        newScanned.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 modeList = "NEWSCANNED";//SCANNED,NEWSCANNED
                 adapter=new InventoryAdapter(activity,newScannedAssetList,"NewScanned");
                list.setAdapter(adapter);
            }
        });


        listTrackingSession = sqlLite.getTrackingSession();
        if (listTrackingSession == null || listTrackingSession.size() <= 0) {
            Toast.makeText(getApplicationContext(), "please download Session", Toast.LENGTH_SHORT).show();
        } else {
            trackingSessionBean = listTrackingSession.get(0);
            toolbar.setTitle(trackingSessionBean.getTrackingSessionName());
//            toolbar.setLogo(R.drawable.assignment);
            dialogLocationScan();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.inventory_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.save_inventory)
        {
            saveAndOpen();

        }

        return super.onOptionsItemSelected(item);
    }

    public void onItemClick(int mPosition) {

        if (modeList.equals("EXISTING") && unScannedAssetList != null && unScannedAssetList.size() > mPosition) {
            InventoryView tempValues = (InventoryView) unScannedAssetList.get(mPosition);
            dialogMessage(tempValues);
        }else if (modeList.equals("SCANNED") &&scannedAssetList != null && scannedAssetList.size() > mPosition) {
            InventoryView tempValues = (InventoryView) scannedAssetList.get(mPosition);
            dialogMessage(tempValues);
        }else if (modeList.equals("NEWSCANNED") && newScannedAssetList != null && newScannedAssetList.size() > mPosition) {
            InventoryView tempValues = (InventoryView) newScannedAssetList.get(mPosition);
            dialogMessage(tempValues);
        }
    }

    public String getDate(String date)
    {
        SimpleDateFormat sdf=new SimpleDateFormat("dd/MM/yyyy");
       return  sdf.format(Common.getDateFromUtcFormat(date));
    }
    public void dialogMessage(final InventoryView tempValues )
    {


     final   Dialog   dialog = new Dialog(context);
        StringBuffer sbm=new StringBuffer();

        dialog.setContentView(R.layout.message_dialog);
        dialog.setTitle("Item Details");
        TextView messageTV = (TextView) dialog.findViewById(R.id.messageTV);
        sbm.append("Item Code: "+tempValues.getItemDetailsBean().getItemCode()+"\n");
        sbm.append("Description: \n"+ tempValues.getItemDetailsBean().getDescription()+"\n");
        if(tempValues.getItemDetailsBean().getPurchaseDate()!=null && tempValues.getItemDetailsBean().getPurchaseDate().length()>10) {
            sbm.append("Purchase Date: " +getDate(tempValues.getItemDetailsBean().getPurchaseDate())  + "\n");
        }
        sbm.append("Epc: "+tempValues.getItemDetailsBean().getItemEpcId()+"\n");
        sbm.append("Remark: "+tempValues.getItemDetailsBean().getRemark()+"\n");
        sbm.append("Location: "+tempValues.getLastItemLocationBean().getItemLocationName()+"\n");
        sbm.append("Price Rs: "+tempValues.getItemDetailsBean().getPrice()+"\n");
/*        dialog.setContentView(R.layout.message_dialog);
        dialog.setTitle("Item Details"+ tempValues.getItemDetailsBean().getItemCode());
        final Typeface customF = Typeface.createFromAsset(getApplicationContext().getAssets(), "Krutidev.TTF");
        TextView messageTV = (TextView) dialog.findViewById(R.id.messageTV);
        messageTV.setTypeface(customF);

        sbm.append("gaunha naaoMd k,maaMk va klama  "+tempValues.getItemDetailsBean().getSectionNumber()+"\n");
        //sbm.append("Date Asset Depoiste Date: "+tempValues.getItemDetailsBean().getDateOfAssetDepositeUTC()+"\n");
        sbm.append(" maala jaPt krNaaracao naava va tarIK   "+tempValues.getItemDetailsBean().getNameOFAssetDepositorDate()+"\n");
        sbm.append("ifyaa-dIcao naaMva va p%ta  "+tempValues.getItemDetailsBean().getComplaintBy()+"\n");
        sbm.append("AaraopIcao naaMva va p%ta "+tempValues.getItemDetailsBean().getCriminalName()+"\n");

        sbm.append("mauddomaalaacao vaNa-na "+ tempValues.getItemDetailsBean().getAssetDescription()+"\n");
        sbm.append("kaoTa-t pazivaNyaabaabat Saora  "+tempValues.getItemDetailsBean().getCourtRemarkAssetPrezenty()+"\n");
        sbm.append("kaoT- hukuma naMbar va tarIK  "+tempValues.getItemDetailsBean().getCourtCommandNumberDate()+"\n");
        sbm.append("maalaacaI ivalhovaaT"+tempValues.getItemDetailsBean().getAssetExit()+"\n");
        sbm.append("maala tabyaat GaoNaaryaacao naava "+tempValues.getItemDetailsBean().getNameOfAssetCollect()+"\n");
        */
     /*
        sbm.append("Date of asset collect : "+tempValues.getItemDetailsBean().getDateOfAssetCollectUTC()+"\n");
        if(tempValues.getItemDetailsBean().getPurchaseDate()!=null && tempValues.getItemDetailsBean().getPurchaseDate().length()>10) {
            sbm.append("Purchase Date: " +getDate(tempValues.getItemDetailsBean().getPurchaseDate())  + "\n");
        }
        sbm.append("Epc: "+tempValues.getItemDetailsBean().getItemEpcId()+"\n");
        sbm.append("Remark: "+tempValues.getItemDetailsBean().getRemark()+"\n");
        sbm.append("Location: "+tempValues.getLastItemLocationBean().getItemLocationName()+"\n");
        sbm.append("Price Rs: "+tempValues.getItemDetailsBean().getPrice()+"\n");
     //   System.out.println("tempValues.getItemDetailsBean().getItemCode()"+tempValues.getItemDetailsBean().getItemCode());
        if(tempValues.getItemDetailsBean().getCreatedOn()!=null && tempValues.getItemDetailsBean().getCreatedOn().length()>10) {


            sbm.append("Created On: " + getDate(tempValues.getItemDetailsBean().getCreatedOn()) + "\n");
        }
        if(tempValues.getItemDetailsBean().getModifyOn()!=null && tempValues.getItemDetailsBean().getModifyOn().length()>10) {


            sbm.append("Modify On: " +getDate(tempValues.getItemDetailsBean().getModifyOn()) + "\n");
        }
        */
      //  sbm.append("Active: "+tempValues.getItemDetailsBean().getActive()+"\n");
        messageTV.setText(sbm.toString());
        Button  completBTSyb = (Button) dialog.findViewById(R.id.completBT);
        completBTSyb.setVisibility(View.VISIBLE);
        completBTSyb.setEnabled(true);
        completBTSyb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();

    }

    public void onDestroy() {

        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
       /* if(keyCode==67)
        {
            DownloadServiceCourse.activity=InventoryActivity.this;
            Intent i = new Intent(Common.appPackage + ".synchronization").putExtra("action_type", "image");
            context.sendBroadcast(i);

        }else*/
        if (keyCode == 120) {
            isStartReading = true;

        } else if (keyCode == KeyEvent.KEYCODE_BACK) {
            saveAndOpen();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void saveAndOpen() {
        if (listTrackingSession != null && listTrackingSession.size() > 0) {
            String androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            sqlLite.addTrackingInventory(scannedAssetList, selecttedLocationBean, trackingSessionBean, androidId,"INVENTORY");
            dialogLocationScan();
        } else {
            isAutoScanReading=false;
            stopReading();
            Intent myIntent = new Intent(InventoryActivity.this, HomeActivity.class);
            startActivity(myIntent);
            finish();
        }
    }


    private void stopReading() {

        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        UfhData.Set_sound(false);
    }

    public void startReading() {

        if (timer == null) {
            UfhData.Set_sound(true);
            UfhData.SoundFlag = false;

            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {

                    if (Scanflag) return;
                    String epc[] = null;

                    if (isStartReading || isAutoScanReading) {
                        Scanflag = true;

                        epc = HKCRfid.startReading();
                        Message m = new Message();
                        Bundle b = new Bundle();
                        b.putStringArray("epcList", epc);
                        m.setData(b);
                        listEpcHandler.sendMessage(m);
                        isStartReading = false;
                        if(epc==null || epc.length<=0)
                        {
                            UfhData.SoundFlag = false;
                        }else{
                            readingDate=new Date();
                        }
                    } else {
//test
                        if(readingDate!=null) {
                            long seconds = (new Date().getTime() - readingDate.getTime()) / 1000;

                            if (seconds > 0) {
                                UfhData.SoundFlag = false;
                                if(seconds>100)
                                {
                                    seconds=2;
                                }
                            }
                        }
                    }
                    Scanflag = false;

                }
            }, 0, Common.SCAN_INTERVAL);

        } else {

            if (timer != null) {
                timer.cancel();
                timer = null;
            }
            UfhData.Set_sound(false);
        }
    }


    public void dialogLocationScan() {
        selecttedLocationBean = null;
        if (scannedAssetList != null)
            scannedAssetList.clear();
        if (unScannedAssetList != null)
            unScannedAssetList.clear();
        if (newScannedAssetList != null)
            newScannedAssetList.clear();
        if (epcMap != null)
            epcMap.clear();
        TOTAL_COUNT=0;
        TOTAL_SERVER_INVENTORY=0;
        setCount();
        adapter = new InventoryAdapter(activity, unScannedAssetList, "UnScanned");
        list.setAdapter(adapter);

        dialogLocationScan = new Dialog(context);
        dialogLocationScan.setContentView(R.layout.location_scan_popup);
        dialogLocationScan.setTitle("Search Location");
        dialogLocationScan.setCancelable(false);
        final CheckBox scanLocationEpc = (CheckBox) dialogLocationScan.findViewById(R.id.scanLocationEpc);
        if(isAutoScanReading)
            scanLocationEpc.setChecked(true);
        else
            scanLocationEpc.setChecked(false);

        mode = "LR";

        TextView inventoryName = (TextView) dialogLocationScan.findViewById(R.id.inventoryName);
        inventoryName.setText(trackingSessionBean.getTrackingSessionName());

        selecttedLocation = (EditText) dialogLocationScan.findViewById(R.id.selecttedLocation);

        selecttedLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isAutoScanReading) {
                    dialogLocationSelection();
                }else{
                    InputMethodManager imm = (InputMethodManager) selecttedLocation.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(selecttedLocation.getWindowToken(), 0);
                    Toast.makeText(getApplicationContext(),"Please turn off auto scan select location.",Toast.LENGTH_LONG).show();
                }
            }
        });


        Button finishInventory = (Button) dialogLocationScan.findViewById(R.id.finishInventory);


        scanLocationEpc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (((CheckBox) v).isChecked() && mode.equals("LR")) {
                    isAutoScanReading = true;
                   // scanLocationEpc.setText("Auto Scan");
                } else {
                    isAutoScanReading = false;
                    scanLocationEpc.setChecked(false);
                    selecttedLocation.requestFocus();
                    //scanLocationEpc.setText("Auto Scan");
                }
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putBoolean("isAutoScanReading", isAutoScanReading);
                editor.commit();


            }
        });
        // if button is clicked, close the cusftom dialog
        /*
        Button startInventory = (Button) dialogLocationScan.findViewById(R.id.startInventory);
        startInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selecttedLocationBean!=null) {
                    dialogLocationScan.dismiss();
                    isStartLocationReading = false;
                    mode = "IM";
                }
            }
        });
        */
        // if button is clicked, close the custom dialog
        finishInventory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLocationScan.dismiss();
                Intent myIntent = new Intent(InventoryActivity.this, HomeActivity.class);
                startActivity(myIntent);
                finish();
                stopReading();
                isAutoScanReading = false;
            }
        });

        dialogLocationScan.show();
        dialogLocationScan.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
              /* if(keyCode==67)
                {
                    DownloadServiceCourse.activity=InventoryActivity.this;
                    Intent i = new Intent(Common.appPackage + ".synchronization").putExtra("action_type", "image");
                    context.sendBroadcast(i);

                }else*/
                if (keyCode == 120) {
                    isStartReading = true;
                }
                return true;
            }
        });
        if (timer == null) {
                startReading();
        }
    }

    public void dialogLocationSelection() {
        mode = "LR";
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.search_dialog);
        dialog.setCancelable(false);
        dialog.setTitle("Search Location");

        Button assetSelectionDone = (Button) dialog.findViewById(R.id.assetSelectionDone);
        asset_list_view = (ListView) dialog.findViewById(R.id.asset_list_view);
        inputSearch = (EditText) dialog.findViewById(R.id.inputSearch);
        listLocation = sqlLite.getItemLocationByLocationName(new ItemLocationBean(),"inventory_menu");
        if (listLocation != null) {
            locationListArray = new String[listLocation.size()];
            for (int i = 0; i < listLocation.size(); i++) {
                locationListArray[i] = listLocation.get(i).getItemLocationName();
            }
            adapterLocation = new ArrayAdapter<String>(context, R.layout.search_list_item, R.id.search_asset, locationListArray);
            asset_list_view.setAdapter(adapterLocation);
        }
        searchLocation();
        asset_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (listLocation != null && listLocation.size() > position) {
                    selecttedLocationBean = listLocation.get(position);
                    selecttedLocation.setText(selecttedLocationBean.getItemLocationName());
                    locationName.setText("LOC:"+selecttedLocationBean.getItemLocationName());
                    unScannedAssetList = sqlLite.getItemDetailsUnscannedByLastLocation(selecttedLocationBean.getItemLocationId());//find current location unscanned list
                    newScannedAssetList = sqlLite.getItemDetailsNewByLastLocation(selecttedLocationBean.getItemLocationId());
                    TOTAL_COUNT=sqlLite.getCountByType("TOTAL_INVENTORY",trackingSessionBean.getTrackingSessionId());
                    TOTAL_SERVER_INVENTORY=sqlLite.getCountByType("TOTAL_SERVER_INVENTORY",trackingSessionBean.getTrackingSessionId());


                    mode = "IM";
                    setCount();
                }
                dialog.dismiss();
                dialogLocationScan.dismiss();

            }
        });


        assetSelectionDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();
    }
    public void setCount()
    {

        if(unScannedAssetList!=null )
            unscannedTvCount.setText(""+unScannedAssetList.size());
        if(newScannedAssetList!=null )
            newScannedTvCount.setText(""+newScannedAssetList.size());
        if(scannedAssetList!=null )
            scannedTvCount.setText("" + scannedAssetList.size());
        tacView.setText("Total: "+(TOTAL_COUNT+TOTAL_SERVER_INVENTORY));
    }
    public void searchLocation() {
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                ItemLocationBean itemLocationBean = new ItemLocationBean();
                itemLocationBean.setItemLocationName(cs.toString());
                listLocation = sqlLite.getItemLocationByLocationName(itemLocationBean,"inventory_menu");
                if (listLocation != null) {
                    locationListArray = new String[listLocation.size()];
                    for (int i = 0; i < listLocation.size(); i++) {
                        locationListArray[i] = listLocation.get(i).getItemLocationName();
                    }
                    adapterLocation = new ArrayAdapter<String>(context, R.layout.search_list_item, R.id.search_asset, locationListArray);
                    asset_list_view.setAdapter(adapterLocation);
                }

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    Handler listEpcHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            try {
                String[] epcList = msg.getData().getStringArray("epcList");

                System.out.println("mode"+mode);
                if (mode.equals("LR")) {
                    ItemLocationBean searchLocationBean = new ItemLocationBean();
                    searchLocationBean.setItemLocationEpcId(epcList[0]);

                    List<ItemLocationBean> list = sqlLite.getItemLocationIsReg(searchLocationBean);
                    if (list != null && list.size() > 0) {
                        dialogLocationScan.dismiss();
                        selecttedLocationBean = list.get(0);
                        locationName.setText("LOC:"+selecttedLocationBean.getItemLocationName());
                        unScannedAssetList = sqlLite.getItemDetailsUnscannedByLastLocation(selecttedLocationBean.getItemLocationId());//find current location unscanned list
                        newScannedAssetList = sqlLite.getItemDetailsNewByLastLocation(selecttedLocationBean.getItemLocationId());
                        TOTAL_COUNT=sqlLite.getCountByType("TOTAL_INVENTORY",trackingSessionBean.getTrackingSessionId());
                        TOTAL_SERVER_INVENTORY=sqlLite.getCountByType("TOTAL_SERVER_INVENTORY",trackingSessionBean.getTrackingSessionId());


                        mode = "IM";
                        setCount();
                    }

                } else {
                    // todo inventory_menu scanning
                    StringBuffer sb = null;
                    for (int i = 0; epcList != null && i < epcList.length; i++) {
                        if (!epcMap.containsKey(epcList[i])) {
                            epcMap.put(epcList[i], epcList[i]);
                            if (sb == null) {
                                sb = new StringBuffer();
                                sb.append("'" + epcList[i] + "'");
                            } else {
                                sb.append(",'" + epcList[i] + "'");
                            }
                        }
                    }

                    if (sb != null && sb.length() > 0) {
                        List<InventoryView> scannedAssetListNew = sqlLite.getItemDetailsScannedByEpcs(sb, sharedpreferences.getInt("loginId", 0));


                        if(scannedAssetListNew!=null && scannedAssetListNew.size()>0) {
                            Loger.error("SEIXS",scannedAssetListNew.size()+"");
                            InventoryView.removeItem(unScannedAssetList,scannedAssetListNew);
                            InventoryView.addNewItem(newScannedAssetList,scannedAssetListNew);
                            scannedAssetList.addAll(0, scannedAssetListNew);
                            modeList="SCANNED";
                            adapter = new InventoryAdapter(activity, scannedAssetList, "Scanned");
                            list.setAdapter(adapter);

                        }
                        setCount();
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };





}
