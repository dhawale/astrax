package com.hkcrfid.jewerly;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.hkcrfid.bean.ItemDetailsBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Amit dhawale on 7/06/19.
 */

public class SearchAssetAdapter extends ArrayAdapter<ItemDetailsBean> implements Filterable {

    List<ItemDetailsBean> assettItemList = new ArrayList<>();

    private Activity activity;
    private SqlLite sqlLite;

    public SearchAssetAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
        this.activity = (Activity) context;
        assettItemList = new ArrayList<ItemDetailsBean>();
        sqlLite = new SqlLite(context);

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.row_item_adapter, parent, false);
        }
        try {
            ItemDetailsBean itemAsset = assettItemList.get(position);
            if (itemAsset != null) {
                TextView customerNameLabel = (TextView) v.findViewById(R.id.tvItem);
                if (customerNameLabel != null) {
                    customerNameLabel.setText(itemAsset.getItemCode());

                }
            }
        } catch (Exception e) {
            Log.e("ProductNameAdapter", "getView", e);
        }
        return v;
    }


    @Override
    public int getCount() {
        if (assettItemList != null)
            return assettItemList.size();
        else return 0;
    }

    @Override
    public ItemDetailsBean getItem(int index) {
        if (assettItemList != null) {
            return assettItemList.get(index);
        } else {
            return null;
        }
    }


    @NonNull
    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            public CharSequence convertResultToString(Object resultValue) {
                try {
                    return ((ItemDetailsBean) resultValue).getItemCode();
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null && constraint.toString().trim().length() > 0) {

                    //notifyDataSetChanged();
                    // A class that queries a web API, parses the data and returns an ArrayList<Style>
                    try {
                        List<ItemDetailsBean> itemList = OfflineGetProduct(constraint.toString());
                        assettItemList.clear();
                        assettItemList.addAll(itemList);
                         notifyDataSetChanged();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    filterResults.values = assettItemList;
                    filterResults.count = assettItemList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence contraint, FilterResults results) {
                /*if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                    //  notifyDataSetChanged();
                }*/
                try {
                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            notifyDataSetChanged();
                        }
                    });

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        };
    }

    private List<ItemDetailsBean> OfflineGetProduct(String searchText) {
        //TODO PriceBook Change
        ItemDetailsBean itemDetailsBean = new ItemDetailsBean();
        itemDetailsBean.setItemCode(searchText);
        return sqlLite.getItemDetailsByItemCode(itemDetailsBean);


    }
}
