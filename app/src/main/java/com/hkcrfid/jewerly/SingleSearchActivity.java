package com.hkcrfid.jewerly;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.hkcrfid.bean.InventoryView;
import com.hkcrfid.bean.ItemDetailsBean;
import com.hkcrfid.bean.ItemLocationBean;
import com.hkcrfid.common.Common;
import com.hkcrfid.common.HKCRfid;

import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Shree on 10/23/2017.
 */

public class SingleSearchActivity extends AppCompatActivity {
    RadioGroup radioSearch;
    Context context=this;
    EditText searchBy,inputSearch;
    String mode="Asset";
    SqlLite sqlLite;
    ItemLocationBean locationBean=null;
    ItemDetailsBean itemDetailsBean=null;
    ListView asset_list_view;
    String searchList[] = {};
    List<ItemDetailsBean> listAsset=null;
    List<ItemLocationBean>listLocation=null;
    ArrayAdapter<String> adapter;
    TextView displayRecord;

    private boolean Scanflag = false, isStartReading = false,isStartReadingSearch=false;
    private Timer timer;
    SharedPreferences sharedpreferences;
    ImageView search;
    Button clear;
    ProgressBar progressBar ;
    Button completeSearchButton;
    TextView resultSearchTV;
    Dialog dialogSearch;
    ImageView imageSearch;
    Date readingDate=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Search Option");
        toolbar.setLogo(R.drawable.search);
        setSupportActionBar(toolbar);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        sharedpreferences = getApplicationContext().getSharedPreferences(Common.appPreference, MODE_PRIVATE);
        sqlLite=new SqlLite(context);
        searchBy=(EditText)findViewById(R.id.searchBy);
        search=(ImageView)findViewById(R.id.search);
        clear=(Button)findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               clearData();
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (timer == null) {
                    startReading();
                }
                if(itemDetailsBean==null && locationBean==null)
                {
                    if(!mode.equals("EPC")) {
                        Toast.makeText(context, "Please Select " + mode, Toast.LENGTH_SHORT).show();
                    }
                }else {
                    if((itemDetailsBean!=null && itemDetailsBean.getItemEpcId()!=null && itemDetailsBean.getItemEpcId().length()>20)
                            ||
                            (locationBean!=null && locationBean.getItemLocationEpcId()!=null && locationBean.getItemLocationEpcId().length()>20)) {
                        isStartReadingSearch = true;
                        dialogSearchSelection();
                    }else
                        {
                            Toast.makeText(context, "Selected " + mode+" not registered.", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        searchBy=(EditText) findViewById(R.id.searchBy);
        displayRecord=(TextView) findViewById(R.id.displayRecord);
        searchBy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mode.equals("Asset")) {
                    dialogAssetSelection();

                }else if(mode.equals("Location")){
                    dialogLocationSelection();
                }


            }
        });

        radioSearch = (RadioGroup) findViewById(R.id.radioSearch);
        radioSearch.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                String radioType=null;
                searchBy.setText("");
                displayRecord.setText("");
                if (null != rb && checkedId > -1) {
                   // Toast.makeText(context, rb.getText(), Toast.LENGTH_SHORT).show();
                    radioType=rb.getText().toString();
                    if(radioType!=null && radioType.equalsIgnoreCase("Asset"))
                    {
                        mode="Asset";
                        searchBy.setEnabled(true);
                        searchBy.setFocusableInTouchMode(true);
                        searchBy.setHint("Select Asset");
                        dialogAssetSelection();
                        search.setVisibility(View.VISIBLE);
                    }else if(radioType!=null && radioType.equalsIgnoreCase("Location"))
                    {
                        mode="Location";
                        searchBy.setEnabled(true);
                        searchBy.setFocusableInTouchMode(true);
                        searchBy.setHint("Select Location");
                        dialogLocationSelection();
                        search.setVisibility(View.VISIBLE);
                    }else if(radioType!=null && radioType.equalsIgnoreCase("EPC"))
                    {
                        mode="EPC";
                        searchBy.setEnabled(false);
                        searchBy.setFocusableInTouchMode(false);
                        searchBy.setHint("Search By Epc");
                        search.setVisibility(View.INVISIBLE);
                    }
                }
            }
        });
        if (timer == null) {
            startReading();
        }
    }
public void clearData()
{
    isStartReadingSearch=false;
    displayRecord.setText("");
    displayRecord.setVisibility(View.INVISIBLE);
    itemDetailsBean=null;
    locationBean=null;
    searchBy.setText("");
}
    Handler listEpcHandler=new Handler() {
        @Override
        public void handleMessage(Message msg) {
            try{
                List<ItemLocationBean> listLocationSearch=null;
                List<ItemDetailsBean>listAssetSearch=null;
                String[] epcList = msg.getData().getStringArray("epcList");
                System.out.println("mode"+mode);
                if(mode.equals("Asset") && isStartReadingSearch)
                {

                    StringBuffer sb = null;
                    for (int i = 0; epcList != null && i < epcList.length; i++)
                    {
                            if (sb == null) {
                                sb = new StringBuffer();
                                sb.append("'" + epcList[i] + "'");
                            } else {
                                sb.append(",'" + epcList[i] + "'");
                            }
                    }

                    if (sb != null && sb.length() > 0) {
                        List<InventoryView> scannedAssetListNew = sqlLite.getItemDetailsScannedByEpcs(sb, sharedpreferences.getInt("loginId", 0));
                        for(int i=0;scannedAssetListNew!=null && i<scannedAssetListNew.size();i++)
                        {
                            if(scannedAssetListNew.get(i).getItemDetailsBean()!=null && scannedAssetListNew.get(i).getItemDetailsBean().getItemId()==itemDetailsBean.getItemId())
                            {
                                isStartReadingSearch=false;
                                progressBar.setVisibility(View.INVISIBLE);
                                completeSearchButton.setText("Search Complete");
                                resultSearchTV.setVisibility(View.VISIBLE);
                                imageSearch.setVisibility(View.VISIBLE);
                                resultSearchTV.setText(itemDetailsBean.getItemCode()+" item code found.");
                                Toast.makeText(context, itemDetailsBean.getItemCode()+" item code found.", Toast.LENGTH_SHORT).show();
                                break;

                            }
                        }
                    }



                }else if(mode.equals("Location")&& isStartReadingSearch)
                {

                    for(int i=0;i<epcList.length;i++)
                    {
                        ItemLocationBean beanLocation=new ItemLocationBean();
                        beanLocation.setItemLocationEpcId(epcList[0]);
                        listLocationSearch=sqlLite.getItemLocationIsReg(beanLocation);
                        if(listLocationSearch!=null && listLocationSearch.size()>0)
                        {
                            if(listLocationSearch.get(0)!=null && listLocationSearch.get(0).getItemLocationId()==locationBean.getItemLocationId())
                            {

                                System.out.println("location Found");
                                isStartReadingSearch=false;
                                progressBar.setVisibility(View.INVISIBLE);
                                completeSearchButton.setText("Search Complete");
                                resultSearchTV.setVisibility(View.VISIBLE);
                                imageSearch.setVisibility(View.VISIBLE);
                                resultSearchTV.setText(locationBean.getItemLocationName()+" location found.");
                                Toast.makeText(context, locationBean.getItemLocationName()+" location found.", Toast.LENGTH_SHORT).show();
                                break;
                            }
                        }
                    }



                }else if(mode.equals("EPC"))
                {
                    searchBy.setText(""+epcList[0]);
                    ItemDetailsBean bean=new ItemDetailsBean();
                    bean.setItemEpcId(epcList[0]);
                    listAssetSearch= sqlLite.getItemDetailsIsReg(bean);
                    if(listAssetSearch!=null && listAssetSearch.size()>0)
                    {
                        itemDetailsBean=listAssetSearch.get(0);
                        displayAsset();
                        isStartReadingSearch=false;
                    }else{
                        ItemLocationBean beanLocation=new ItemLocationBean();
                        beanLocation.setItemLocationEpcId(epcList[0]);
                        listLocationSearch=sqlLite.getItemLocationIsReg(beanLocation);
                        if(listLocationSearch!=null && listLocationSearch.size()>0)
                        {
                           locationBean= listLocationSearch.get(0);
                            displayLocation();
                            isStartReadingSearch=false;
                        }
                    }

                    if((listLocationSearch==null || listLocationSearch.size()<=0) && (listAssetSearch==null || listAssetSearch.size()<=0))
                    {
                        Toast.makeText(context,"Tag not associate with item or location.",Toast.LENGTH_SHORT).show();
                        displayRecord.setVisibility(View.VISIBLE);
                        displayRecord.setText("Tag not associate with item or location.");
                    }
                }

            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }
    };



    public void dialogLocationSelection()
    {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.search_dialog);
        dialog.setTitle("Search Location");
        itemDetailsBean=null;
        locationBean=null;
        Button assetSelectionDone   =(Button) dialog.findViewById(R.id.assetSelectionDone);
        asset_list_view = (ListView) dialog.findViewById(R.id.asset_list_view);
        inputSearch = (EditText) dialog.findViewById(R.id.inputSearch);
        listLocation=sqlLite.getItemLocationByLocationName(new ItemLocationBean(),"inventory_menu");
        if(listLocation!=null) {
            searchList = new String[listLocation.size()];
            for(int i=0;i<listLocation.size();i++)
            {
                searchList[i]=listLocation.get(i).getItemLocationName();
            }
            adapter = new ArrayAdapter<String>(context, R.layout.search_list_item, R.id.search_asset, searchList);
            asset_list_view.setAdapter(adapter);
        }
        smartSearch();
        asset_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(listLocation!=null&&listLocation.size()>position) {

                    locationBean = listLocation.get(position);
                    searchBy.setText(""+locationBean.getItemLocationName());
                    displayLocation();
                }
                dialog.dismiss();
            }
        } );


        assetSelectionDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();
    }
public  void displayLocation() {
    StringBuffer sb = new StringBuffer();
    if (locationBean != null)
    {

        if(locationBean.getItemLocationName()!=null) {
            searchBy.setText(""+locationBean.getItemLocationName());
            sb.append("Location Name: " + locationBean.getItemLocationName() + "\n");
        }
        if(locationBean.getItemLocationEpcId()!=null)
        sb.append("EPC: " + locationBean.getItemLocationEpcId() + "\n");
        displayRecord.setText("" + sb.toString());
        displayRecord.setVisibility(View.VISIBLE);

    }
}
public void displayAsset()
{
    if(itemDetailsBean!=null) {
        ItemLocationBean locBean=new ItemLocationBean();
        if(itemDetailsBean.getCurrentLocationId()>0)
        {
            locBean.setItemLocationId(itemDetailsBean.getCurrentLocationId());
            locBean=sqlLite.getItemLocationById(locBean);
        }else if(itemDetailsBean.getLastLocationID()>0)
        {
            locBean.setItemLocationId(itemDetailsBean.getLastLocationID());
            locBean=sqlLite.getItemLocationById(locBean);
        }

        displayRecord.setVisibility(View.VISIBLE);
        StringBuffer sb = new StringBuffer();
        searchBy.setText("" + itemDetailsBean.getItemCode());
        sb.append("Item Code " + itemDetailsBean.getItemCode() + "\n");
        if(locBean!=null && locBean.getItemLocationName()!=null)
            sb.append("Last Location " + locBean.getItemLocationName() + "\n");
        if (itemDetailsBean.getDescription() != null)
            sb.append("Description " + itemDetailsBean.getDescription() + "\n");
        if (itemDetailsBean.getPrice() != null)
            sb.append("Price " + itemDetailsBean.getPrice() + "\n");
        if (itemDetailsBean.getItemEpcId() != null)
            sb.append("EPC " + itemDetailsBean.getItemEpcId() + "\n");
        displayRecord.setText("" + sb.toString());
    }
}


    public void dialogAssetSelection()
    {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.search_dialog);
        dialog.setTitle("Search Asset");
        itemDetailsBean=null;
        locationBean=null;
        Button assetSelectionDone   =(Button) dialog.findViewById(R.id.assetSelectionDone);
        asset_list_view = (ListView) dialog.findViewById(R.id.asset_list_view);
        inputSearch = (EditText) dialog.findViewById(R.id.inputSearch);
        if(mode.equals("Asset")) {
            listAsset = sqlLite.getItemDetailsByItemCode(new ItemDetailsBean());
            if (listAsset != null) {
                searchList = new String[listAsset.size()];
                for (int i = 0; i < listAsset.size(); i++) {
                    searchList[i] = listAsset.get(i).getItemCode();
                }
                adapter = new ArrayAdapter<String>(context, R.layout.search_list_item, R.id.search_asset, searchList);
                asset_list_view.setAdapter(adapter);
            }

        }
        smartSearch();
        // Adding items to listview

        asset_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(listAsset!=null&&listAsset.size()>position) {
                    StringBuffer sb=new StringBuffer();
                    itemDetailsBean = listAsset.get(position);
                    displayAsset();
                }
                dialog.dismiss();
            }
        } );




        // if button is clicked, close the custom dialog
        assetSelectionDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public void smartSearch()
    {
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text

                if(mode.equalsIgnoreCase("Asset")) {
                    ItemDetailsBean itemDetailsBean=new ItemDetailsBean();
                    itemDetailsBean.setItemCode(cs.toString());
                    listAsset = sqlLite.getItemDetailsByItemCode(itemDetailsBean);
                    if (listAsset != null) {
                        searchList = new String[listAsset.size()];
                        for (int i = 0; i < listAsset.size(); i++) {
                            searchList[i] = listAsset.get(i).getItemCode();
                        }

                        adapter = new ArrayAdapter<String>(context, R.layout.search_list_item, R.id.search_asset, searchList);
                        asset_list_view.setAdapter(adapter);
                    }
                }else if(mode.equalsIgnoreCase("Location"))
                {
                    ItemLocationBean itemLocationBean=new ItemLocationBean();
                    itemLocationBean.setItemLocationName(cs.toString());
                    listLocation=sqlLite.getItemLocationByLocationName(itemLocationBean,"inventory_menu");
                    if(listLocation!=null) {
                        searchList = new String[listLocation.size()];
                        for(int i=0;i<listLocation.size();i++)
                        {
                            searchList[i]=listLocation.get(i).getItemLocationName();
                        }
                        adapter = new ArrayAdapter<String>(context, R.layout.search_list_item, R.id.search_asset, searchList);
                        asset_list_view.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
    }
    public void onDestroy()
    {

        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
/*
        if(keyCode==67)
        {
            DownloadServiceCourse.activity=SingleSearchActivity.this;
            Intent i = new Intent(Common.appPackage + ".synchronization").putExtra("action_type", "image");
            context.sendBroadcast(i);
        }else*/
        if (keyCode == 120) {
            isStartReading = true;

        } else
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent myIntent = new Intent(SingleSearchActivity.this, HomeActivity.class);
            startActivity(myIntent);
            finish();
            stopReading();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    public void startReading() {


        if (timer == null) {
            UfhData.Set_sound(true);
            UfhData.SoundFlag = false;

            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {


                    if (Scanflag) return;
                    String epc[] = null;
                    if (isStartReading ) {
                        Scanflag = true;

                        epc = HKCRfid.startReading();
                        Message m = new Message();
                        Bundle b = new Bundle();
                        b.putStringArray("epcList", epc);
                        m.setData(b);
                        listEpcHandler.sendMessage(m);
                        isStartReading = false;
                        if(epc==null || epc.length<=0)
                        {
                            UfhData.SoundFlag = false;
                        }else{
                            readingDate=new Date();
                        }
                    }else{
                        if(readingDate!=null) {
                            long seconds = (new Date().getTime() - readingDate.getTime()) / 1000;

                            if (seconds > 0) {
                                UfhData.SoundFlag = false;
                                if(seconds>100)
                                {
                                    seconds=2;
                                }
                            }
                        }
                    }
                    Scanflag = false;

                }
            }, 0, Common.SCAN_INTERVAL);

        } else {

            if (timer != null) {
                timer.cancel();
                timer = null;
            }
            UfhData.Set_sound(false);
        }
    }

    private void stopReading() {

        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        UfhData.Set_sound(false);
    }



    public void dialogSearchSelection()
    {

        dialogSearch = new Dialog(context);
        dialogSearch.requestWindowFeature(Window.FEATURE_RIGHT_ICON);
        dialogSearch.setContentView(R.layout.search_option_progress);
        dialogSearch.setFeatureDrawableResource(Window.FEATURE_RIGHT_ICON, R.drawable.search_icon_tab);
        dialogSearch.setTitle("Search "+mode);
        dialogSearch.setCancelable(false);

        progressBar = (ProgressBar) dialogSearch.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        completeSearchButton   =(Button) dialogSearch.findViewById(R.id.completBT);
        completeSearchButton.setText("Stop");
        resultSearchTV=(TextView) dialogSearch.findViewById(R.id.resultSearchTV);
        resultSearchTV.setVisibility(View.INVISIBLE);
        imageSearch=(ImageView) dialogSearch.findViewById(R.id.imageSearch);
        imageSearch.setVisibility(View.INVISIBLE);
        TextView displayRecordSearch   =(TextView) dialogSearch.findViewById(R.id.displayRecord);
        displayRecordSearch.setText(displayRecord.getText().toString());
        completeSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.INVISIBLE);
                dialogSearch.dismiss();
                clearData();
            }
        });

        dialogSearch.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
             /*  if(keyCode==67)
                {
                    DownloadServiceCourse.activity=SingleSearchActivity.this;
                    Intent i = new Intent(Common.appPackage + ".synchronization").putExtra("action_type", "image");
                    context.sendBroadcast(i);

                }else*/
                if (keyCode == 120) {
                    isStartReading = true;

                } else
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    dialogSearch.dismiss();
                }
                return true;
            }
        });
        dialogSearch.show();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.registrationmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_asset_registration)
        {

            Intent myIntent = new Intent(SingleSearchActivity.this, AssetRegistration.class);
            startActivity(myIntent);
            finish();
            return true;

        }else if (id == R.id.action_location_registration) {
            Intent myIntent = new Intent(SingleSearchActivity.this, LocationRegistration.class);
            startActivity(myIntent);
            finish();
            return true;
        }else  if (id == R.id.action_UnRegistration) {

            Intent myIntent = new Intent(SingleSearchActivity.this, UnregistrationEpc.class);
            startActivity(myIntent);
            finish();

            return true;
        }else  if (id == R.id.action_search_option) {
            /*
            Intent myIntent = new Intent(SingleSearchActivity.this, SingleSearchActivity.class);
            startActivity(myIntent);
            finish();
            */
            return true;
        }else  if (id == R.id.action_home) {
            Intent myIntent = new Intent(SingleSearchActivity.this, HomeActivity.class);
            startActivity(myIntent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
