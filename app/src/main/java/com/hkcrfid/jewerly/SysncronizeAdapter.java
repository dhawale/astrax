package com.hkcrfid.jewerly;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.hkcrfid.common.Common;

import java.util.ArrayList;

/********* Adapter class extends with BaseAdapter and implements with OnClickListener ************/
public class SysncronizeAdapter extends BaseAdapter implements View.OnClickListener {

    /*********** Declare Used Variables *********/
    private Activity activity;
    private ArrayList data;
    private static LayoutInflater inflater=null;
    public Resources res;
    SyncronizeListModel tempValues=null;
    int i=0;

    /*************  HomeAdapter Constructor *****************/
    public SysncronizeAdapter(Activity a, ArrayList d, Resources resLocal) {

        /********** Take passed values **********/
        activity = a;
        data=d;
        res = resLocal;

        /***********  Layout inflator to call external xml layout () ***********/
        inflater = ( LayoutInflater )activity.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    /******** What is the size of Passed Arraylist Size ************/
    public int getCount() {

        if(data.size()<=0)
            return 1;
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{


        public TextView textView;
        public ImageView imageView;
        public CheckBox checkBox;

    }

    /****** Depends upon data size called for each row , Create each ListView row *****/
    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder;

        if(convertView==null){

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.syncronize_list_view, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();
            holder.textView = (TextView) vi.findViewById(R.id.textView);

            holder.imageView=(ImageView)vi.findViewById(R.id.imageView);
            holder.checkBox=(CheckBox)vi.findViewById(R.id.checkBox);
            /************  Set holder with LayoutInflater ************/

            vi.setTag( holder );
        }
        else
            holder=(ViewHolder)vi.getTag();

        if(data.size()<=0)
        {
            holder.textView.setText("No Data");
        }
        else
        {
            /***** Get each Model object from Arraylist ********/
            tempValues=null;
            tempValues = ( SyncronizeListModel ) data.get( position );

            /************  Set Model values in Holder elements ***********/

            //holder.checkBox.setOnClickListener(new OnItemClickListener( position ));

            holder.imageView.setImageResource(
                    res.getIdentifier(
                            Common.appPackage+":drawable/"+tempValues.getImage()
                            ,null,null));


            holder.textView.setText( tempValues.getMenuName() );
            holder.checkBox.setChecked(tempValues.isCheckBoxSelected());
            holder.checkBox.setOnClickListener(new OnItemClickListener( position ));
        }
        return vi;
    }

    @Override
    public void onClick(View v) {

    }

    /********* Called when Item click in ListView ************/
    private class OnItemClickListener  implements View.OnClickListener{
        private int mPosition;

        OnItemClickListener(int position){
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {


            SyscronizeActivity sct = (SyscronizeActivity)activity;



            sct.onItemClick(mPosition);
            tempValues = ( SyncronizeListModel ) data.get( mPosition );
            if(tempValues.isCheckBoxSelected())
            {
                tempValues.setCheckBoxSelected(false);
            }else{
                tempValues.setCheckBoxSelected(true);
            }

        }
    }
}


