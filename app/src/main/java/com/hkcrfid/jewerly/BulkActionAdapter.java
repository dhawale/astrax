package com.hkcrfid.jewerly;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import com.hkcrfid.bean.InventoryView;
import java.util.List;

/********* Adapter class extends with BaseAdapter and implements with OnClickListener ************/
public class BulkActionAdapter extends BaseAdapter implements View.OnClickListener {

    /*********** Declare Used Variables *********/
    private Activity activity;
    private List<InventoryView> itemList;
    private static LayoutInflater inflater=null;
    private String listType=null;
    InventoryView inventoryView=null;
    int i=0;

    /*************  HomeAdapter Constructor *****************/
    public BulkActionAdapter(Activity a, List<InventoryView> d ,String listType) {

        /********** Take passed values **********/
        activity = a;
        itemList=d;
        this.listType=listType;

        /***********  Layout inflator to call external xml layout () ***********/
        inflater = ( LayoutInflater )activity. getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    /******** What is the size of Passed Arraylist Size ************/
    public int getCount() {

        if(itemList.size()<=0)
            return 1;
        return itemList.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    /********* Create a holder Class to contain inflated xml file elements *********/
    public static class ViewHolder{

        public TextView textViewAsset;
        public TextView textViewLastLocation;
        public CheckBox inventoryCB;

    }

    /****** Depends upon data size called for each row , Create each ListView row *****/
    public View getView(int position, View convertView, ViewGroup parent) {

        View vi = convertView;
        ViewHolder holder;

        if(convertView==null){

            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.bulkactionlist, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/

            holder = new ViewHolder();
            holder.textViewAsset = (TextView) vi.findViewById(R.id.textViewAsset);
            holder.textViewLastLocation= (TextView) vi.findViewById(R.id.textViewLastLocation);
            holder.inventoryCB= (CheckBox) vi.findViewById(R.id.inventoryCB);


            /************  Set holder with LayoutInflater ************/
            vi.setTag( holder );
        }
        else
            holder=(ViewHolder)vi.getTag();

        if(listType!=null && listType.equals("Scanned"))
        {
            int myColor = activity.getResources().getColor(R.color.scanned);
            vi.setBackgroundColor(myColor);
        }else if(listType!=null && listType.equals("UnScanned"))
        {
            int myColor = activity.getResources().getColor(R.color.unscanned);
            vi.setBackgroundColor(myColor);
        }else if(listType!=null && listType.equals("NewScanned"))
        {
            int myColor = activity.getResources().getColor(R.color.newscanned);
            vi.setBackgroundColor(myColor);
        }

        if(itemList.size()<=0)
        {
            holder.textViewAsset.setText("No record found");
            holder.textViewLastLocation.setText("-");

        }
        else
        {
            inventoryView=null;
            inventoryView= itemList.get( position );

            holder.textViewAsset.setText( inventoryView.getItemDetailsBean().getItemCode() );

            if(inventoryView.getLastItemLocationBean()!=null && inventoryView.getLastItemLocationBean().getItemLocationName()!=null ) {
                holder.textViewLastLocation.setText("" + inventoryView.getLastItemLocationBean().getItemLocationName());
            }else{
                holder.textViewLastLocation.setText("");
            }
            holder.inventoryCB.setChecked(inventoryView.isItemBeanSelected());

            holder.inventoryCB.setOnClickListener(new OnItemClickListener( position ));

            vi.setOnClickListener(new OnItemClickListener( position ));
        }
        return vi;
    }

    @Override
    public void onClick(View v) {

    }

    /********* Called when Item click in ListView ************/
    private class OnItemClickListener  implements View.OnClickListener{
        private int mPosition;

        OnItemClickListener(int position){
            mPosition = position;
        }

        @Override
        public void onClick(View arg0) {


            BulkProcessAction sct = (BulkProcessAction)activity;

            /****  Call  onItemClick Method inside HomeActivity Class ( See Below )****/

            sct.onItemClick(mPosition);
        }
    }
}

