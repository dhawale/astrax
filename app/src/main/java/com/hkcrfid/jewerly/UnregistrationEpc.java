package com.hkcrfid.jewerly;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.hkcrfid.bean.ItemDetailsBean;
import com.hkcrfid.bean.ItemLocationBean;
import com.hkcrfid.common.Common;
import com.hkcrfid.common.HKCRfid;

import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Shree on 10/23/2017.
 */

public class UnregistrationEpc extends AppCompatActivity {
    RadioGroup radioSearch;
    RadioButton radioAsset,radioLocation;
    Context context=this;
    EditText searchBy,inputSearch;
    String mode="Asset";
    SqlLite sqlLite;
    ItemLocationBean locationBean=null;
    ItemDetailsBean itemDetailsBean=null;
    ListView asset_list_view;
    String searchList[] = {};
    List<ItemDetailsBean> listAsset=null;
    List<ItemLocationBean>listLocation=null;
    ArrayAdapter<String> adapter;
    TextView displayRecord;
    private static final int SCAN_INTERVAL = 300;
    private boolean Scanflag = false, isStartReading = false,isStartReadingSearch=true;
    private Timer timer;
    SharedPreferences sharedpreferences;
    Button clear;
    Dialog dialogSearch;
    ImageView unregister;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.unregistration);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("UnRegister Option");
        toolbar.setLogo(R.drawable.unregister);
        setSupportActionBar(toolbar);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        sharedpreferences = getApplicationContext().getSharedPreferences(Common.appPreference, MODE_PRIVATE);
        sqlLite=new SqlLite(context);
        searchBy=(EditText)findViewById(R.id.searchBy);
        unregister=(ImageView) findViewById(R.id.unregister);
        unregister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isStartReadingSearch=false;
                if(mode.equals("Asset") && itemDetailsBean!=null)
                {
                    if(itemDetailsBean.getItemEpcId()==null || itemDetailsBean.getItemEpcId().length()<=20)
                    {
                        Toast.makeText(getApplicationContext(),"Item not associated EPC",Toast.LENGTH_SHORT).show();
                    }else
                        {
                            itemDetailsBean.setNewItemReg("Y");
                            itemDetailsBean.setItemEpcId(null);
                            itemDetailsBean.setModifyBy( sharedpreferences.getInt("loginId", 0));
                            itemDetailsBean.setStatus("Unregister");
                            itemDetailsBean.setModifyOn(Common.getUtcFormat(new Date()));
                            int result=  sqlLite.updateItemDetails(itemDetailsBean);
                            itemDetailsBean=null;
                            clearData();
                            displayRecord.setText("Unregister Item successfully.");

                    }
                }else if(mode.equals("Asset") && locationBean!=null)
                {
                    if(locationBean.getItemLocationEpcId()==null || locationBean.getItemLocationEpcId().length()<=20)
                    {
                        Toast.makeText(getApplicationContext(),"Location not associated EPC",Toast.LENGTH_SHORT).show();
                    }else
                    {
                            locationBean.setItemLocationEpcId(null);
                            locationBean.setModifyBy( sharedpreferences.getInt("loginId", 0));
                            locationBean.setModifyOn(Common.getUtcFormat(new Date()));
                            locationBean.setNewItemLocationReg("Y");
                            int result=  sqlLite.updateItemLocation(locationBean);
                            locationBean=null;
                            clearData();
                            displayRecord.setText("Unregister Location successfully.");


                    }

                }


            }
        });
        clear=(Button)findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearData();
            }
        });



        searchBy=(EditText) findViewById(R.id.searchBy);
        displayRecord=(TextView) findViewById(R.id.displayRecord);
        searchBy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mode.equals("Asset")) {
                    dialogAssetSelection();

                }else if(mode.equals("Location")){
                    dialogLocationSelection();
                }


            }
        });

        radioSearch = (RadioGroup) findViewById(R.id.radioSearch);
        radioAsset = (RadioButton) findViewById(R.id.radioAsset);
        radioLocation = (RadioButton) findViewById(R.id.radioLocation);


        radioSearch.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                String radioType=null;
                searchBy.setText("");
                displayRecord.setText("");
                if (null != rb && checkedId > -1) {

                    radioType=rb.getText().toString();
                    if(radioType!=null && radioType.equalsIgnoreCase("Asset"))
                    {
                        mode="Asset";
                        searchBy.setEnabled(true);
                        searchBy.setFocusableInTouchMode(true);
                        searchBy.setHint("Select Asset");
                      //  dialogAssetSelection();

                    }else if(radioType!=null && radioType.equalsIgnoreCase("Location"))
                    {
                        mode="Location";
                        searchBy.setEnabled(true);
                        searchBy.setFocusableInTouchMode(true);
                        searchBy.setHint("Select Location");
                       // dialogLocationSelection();

                    }
                }
            }
        });
        if (timer == null) {
            startReading();
        }
    }
    public void clearData()
    {
        isStartReadingSearch=true;
        displayRecord.setText("");
        displayRecord.setVisibility(View.INVISIBLE);
        itemDetailsBean=null;
        locationBean=null;
        searchBy.setText("");
        radioAsset.setChecked(false);
        radioLocation.setChecked(false);
    }
    Handler listEpcHandler=new Handler() {
        @Override
        public void handleMessage(Message msg) {
            try{
                List<ItemLocationBean> listLocationSearch=null;
                List<ItemDetailsBean>listAssetSearch=null;
                String[] epcList = msg.getData().getStringArray("epcList");
                if(isStartReadingSearch) {

                    searchBy.setText("" + epcList[0]);
                    ItemDetailsBean bean = new ItemDetailsBean();
                    bean.setItemEpcId(epcList[0]);
                    listAssetSearch = sqlLite.getItemDetailsIsReg(bean);
                    if (listAssetSearch != null && listAssetSearch.size() > 0) {
                        itemDetailsBean = listAssetSearch.get(0);
                        mode = "Asset";
                        isStartReadingSearch = false;
                        radioAsset.setChecked(true);
                        displayAsset();

                    } else {
                        ItemLocationBean beanLocation = new ItemLocationBean();
                        beanLocation.setItemLocationEpcId(epcList[0]);
                        listLocationSearch = sqlLite.getItemLocationIsReg(beanLocation);
                        if (listLocationSearch != null && listLocationSearch.size() > 0) {
                            mode = "Location";
                            locationBean = listLocationSearch.get(0);
                            isStartReadingSearch = false;
                            radioLocation.setChecked(true);
                            displayLocation();

                        }
                    }

                    if ((listLocationSearch == null || listLocationSearch.size() <= 0) && (listAssetSearch == null || listAssetSearch.size() <= 0)) {
                        Toast.makeText(context, "Tag not associate with item or location.", Toast.LENGTH_SHORT).show();
                        displayRecord.setVisibility(View.VISIBLE);
                        displayRecord.setText("Tag not associate with item or location.");
                    }
                }

            }catch(Exception e)
            {
                e.printStackTrace();
            }
        }
    };



    public void dialogLocationSelection()
    {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.search_dialog);
        dialog.setTitle("Search Location");
        itemDetailsBean=null;
        locationBean=null;
        Button assetSelectionDone   =(Button) dialog.findViewById(R.id.assetSelectionDone);
        asset_list_view = (ListView) dialog.findViewById(R.id.asset_list_view);
        inputSearch = (EditText) dialog.findViewById(R.id.inputSearch);
        listLocation=sqlLite.getItemLocationByLocationName(new ItemLocationBean(),"inventory_menu");
        if(listLocation!=null) {
            searchList = new String[listLocation.size()];
            for(int i=0;i<listLocation.size();i++)
            {
                searchList[i]=listLocation.get(i).getItemLocationName();
            }
            adapter = new ArrayAdapter<String>(context, R.layout.search_list_item, R.id.search_asset, searchList);
            asset_list_view.setAdapter(adapter);
        }
        smartSearch();
        asset_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(listLocation!=null&&listLocation.size()>position) {

                    locationBean = listLocation.get(position);
                    searchBy.setText(""+locationBean.getItemLocationName());
                    displayLocation();
                    isStartReadingSearch=false;
                    radioLocation.setChecked(true);

                }
                dialog.dismiss();
            }
        } );


        assetSelectionDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();
    }
    public  void displayLocation() {
        StringBuffer sb = new StringBuffer();
        if (locationBean != null)
        {

            if(locationBean.getItemLocationName()!=null) {
                searchBy.setText(""+locationBean.getItemLocationName());
                sb.append("Location Name: " + locationBean.getItemLocationName() + "\n");
            }
            if(locationBean.getItemLocationEpcId()!=null)
                sb.append("EPC: " + locationBean.getItemLocationEpcId() + "\n");
            displayRecord.setText("" + sb.toString());
            displayRecord.setVisibility(View.VISIBLE);

        }
    }
    public void displayAsset()
    {
        if(itemDetailsBean!=null) {
            ItemLocationBean locBean=new ItemLocationBean();
            if(itemDetailsBean.getCurrentLocationId()>0)
            {
                locBean.setItemLocationId(itemDetailsBean.getCurrentLocationId());
                locBean=sqlLite.getItemLocationById(locBean);
            }else if(itemDetailsBean.getLastLocationID()>0)
            {
                locBean.setItemLocationId(itemDetailsBean.getLastLocationID());
                locBean=sqlLite.getItemLocationById(locBean);
            }


            StringBuffer sb = new StringBuffer();
            searchBy.setText("" + itemDetailsBean.getItemCode());
            sb.append("Item Code " + itemDetailsBean.getItemCode() + "\n");
            if(locBean!=null && locBean.getItemLocationName()!=null)
                sb.append("Last Location " + locBean.getItemLocationName() + "\n");
            if (itemDetailsBean.getDescription() != null)
                sb.append("Description " + itemDetailsBean.getDescription() + "\n");
            if (itemDetailsBean.getPrice() != null)
                sb.append("Price " + itemDetailsBean.getPrice() + "\n");
            if (itemDetailsBean.getItemEpcId() != null)
                sb.append("EPC " + itemDetailsBean.getItemEpcId() + "\n");
            displayRecord.setText("" + sb.toString());
            displayRecord.setVisibility(View.VISIBLE);

        }
    }


    public void dialogAssetSelection()
    {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.search_dialog);
        dialog.setTitle("Search Asset");
        itemDetailsBean=null;
        locationBean=null;
        Button assetSelectionDone   =(Button) dialog.findViewById(R.id.assetSelectionDone);
        asset_list_view = (ListView) dialog.findViewById(R.id.asset_list_view);
        inputSearch = (EditText) dialog.findViewById(R.id.inputSearch);
        if(mode.equals("Asset")) {
            listAsset = sqlLite.getItemDetailsByItemCode(new ItemDetailsBean());
            if (listAsset != null) {
                searchList = new String[listAsset.size()];
                for (int i = 0; i < listAsset.size(); i++) {
                    searchList[i] = listAsset.get(i).getItemCode();
                }
                adapter = new ArrayAdapter<String>(context, R.layout.search_list_item, R.id.search_asset, searchList);
                asset_list_view.setAdapter(adapter);
            }

        }
        smartSearch();
        // Adding items to listview

        asset_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(listAsset!=null&&listAsset.size()>position) {
                    StringBuffer sb=new StringBuffer();
                    itemDetailsBean = listAsset.get(position);
                    displayAsset();
                    isStartReadingSearch=false;
                    radioAsset.setChecked(true);
                }
                dialog.dismiss();
            }
        } );




        // if button is clicked, close the custom dialog
        assetSelectionDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        dialog.show();
    }


    public void smartSearch()
    {
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text

                if(mode.equalsIgnoreCase("Asset")) {
                    ItemDetailsBean itemDetailsBean=new ItemDetailsBean();
                    itemDetailsBean.setItemCode(cs.toString());
                    listAsset = sqlLite.getItemDetailsByItemCode(itemDetailsBean);
                    if (listAsset != null) {
                        searchList = new String[listAsset.size()];
                        for (int i = 0; i < listAsset.size(); i++) {
                            searchList[i] = listAsset.get(i).getItemCode();
                        }

                        adapter = new ArrayAdapter<String>(context, R.layout.search_list_item, R.id.search_asset, searchList);
                        asset_list_view.setAdapter(adapter);
                    }
                }else if(mode.equalsIgnoreCase("Location"))
                {
                    ItemLocationBean itemLocationBean=new ItemLocationBean();
                    itemLocationBean.setItemLocationName(cs.toString());
                    listLocation=sqlLite.getItemLocationByLocationName(itemLocationBean,"inventory_menu");
                    if(listLocation!=null) {
                        searchList = new String[listLocation.size()];
                        for(int i=0;i<listLocation.size();i++)
                        {
                            searchList[i]=listLocation.get(i).getItemLocationName();
                        }
                        adapter = new ArrayAdapter<String>(context, R.layout.search_list_item, R.id.search_asset, searchList);
                        asset_list_view.setAdapter(adapter);
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
    }
    public void onDestroy()
    {

        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
       /* if(keyCode==67)
        {
            DownloadServiceCourse.activity=UnregistrationEpc.this;
            Intent i = new Intent(Common.appPackage + ".synchronization").putExtra("action_type", "image");
            context.sendBroadcast(i);

        }else*/
        if (keyCode == 120) {
            isStartReading = true;

        } else
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent myIntent = new Intent(UnregistrationEpc.this, HomeActivity.class);
            startActivity(myIntent);
            finish();
            stopReading();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    public void startReading() {


        if (timer == null) {
            UfhData.Set_sound(true);
            UfhData.SoundFlag = false;

            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {


                    if (Scanflag) return;
                    String epc[] = null;
                    if (isStartReading  ) {
                        Scanflag = true;
                        epc = HKCRfid.startReading();
                        Message m = new Message();
                        Bundle b = new Bundle();
                        b.putStringArray("epcList", epc);
                        m.setData(b);
                        listEpcHandler.sendMessage(m);
                        isStartReading = false;
                    } else {
                        UfhData.SoundFlag = false;
                    }
                    Scanflag = false;
                }
            }, 0, SCAN_INTERVAL);

        } else {

            if (timer != null) {
                timer.cancel();
                timer = null;
            }
            UfhData.Set_sound(false);
        }
    }

    private void stopReading() {

        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        UfhData.Set_sound(false);
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.registrationmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

       if (id == R.id.action_asset_registration)
        {

            Intent myIntent = new Intent(UnregistrationEpc.this, AssetRegistration.class);
            startActivity(myIntent);
            finish();
            return true;

        }else if (id == R.id.action_location_registration) {
            Intent myIntent = new Intent(UnregistrationEpc.this, LocationRegistration.class);
            startActivity(myIntent);
            finish();
            return true;
        }else  if (id == R.id.action_UnRegistration) {
            /*
            Intent myIntent = new Intent(UnregistrationEpc.this, UnregistrationEpc.class);
            startActivity(myIntent);
            finish();
            */
           return true;
        }else  if (id == R.id.action_search_option) {
//            Intent myIntent = new Intent(UnregistrationEpc.this, SingleSearchActivity.class);
            Intent myIntent = new Intent(UnregistrationEpc.this, SearchAssetActivity.class);
            startActivity(myIntent);
            finish();
           return true;
        }else  if (id == R.id.action_home) {
            Intent myIntent = new Intent(UnregistrationEpc.this, HomeActivity.class);
            startActivity(myIntent);
            finish();
           return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
