package com.hkcrfid.jewerly;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.hkcrfid.common.Common;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
public	 class CallAPI extends AsyncTask<String, String, String> {
	
	private String returnData=null;
	private Boolean taskComplete=false;
	private Boolean flagSuccess=false;
	private ProgressDialog progress;
	private String messageProgress=null;
	boolean enableProgres=false;
	public CallAPI(Context context, String msg, boolean enableProgres)
	{
		this.progress = new ProgressDialog(context);
		this.messageProgress=msg;
		this.enableProgres=enableProgres;
		
	}
	
	public void showProgress()
	{
		if(enableProgres)
		{
		  progress.setMessage(messageProgress);      
	      progress.setCancelable(false);
	      progress.show();
		}
	   
	}
	public void closeProgress()
	{
		if(progress!=null && enableProgres && progress.isShowing())
		{
			progress.dismiss();
		}
		
	}
    public String getReturnData() {
		return returnData;
	}

	public void setReturnData(String returnData) {
		this.returnData = returnData;
	}

	public Boolean getTaskComplete() {
		return taskComplete;
	}

	public void setTaskComplete(Boolean taskComplete) {
		this.taskComplete = taskComplete;
	}

	public Boolean getFlagSuccess() {
		return flagSuccess;
	}

	public void setFlagSuccess(Boolean flagSuccess) {
		this.flagSuccess = flagSuccess;
	}

	@Override
    protected String doInBackground(String... params) {
      String urlString=params[0]; // URL to call
      String resultToDisplay = "";
      InputStream in = null;
   
      try {
    	 
        URL url = new URL(urlString);        
    	HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
    	urlConnection.setConnectTimeout(15000);

    	in = new BufferedInputStream(urlConnection.getInputStream());    	
    	resultToDisplay=convertStreamToString(in);
    	setReturnData(resultToDisplay);
    	setFlagSuccess(true);
    	setTaskComplete(true);
      }catch (IOException e)
      {
    	  e.printStackTrace();
    	setTaskComplete(true);
    	setReturnData(""+Common.pleaseCheckNwConnection);
    	closeProgress();
    	return e.getMessage();
      }
      catch (Exception e)
      {
    	  e.printStackTrace();
    	setTaskComplete(true);
    	setReturnData(e.getMessage());
    	closeProgress();
    	return e.getMessage();
      }   
      
          
       return resultToDisplay;

    }

    protected void onPostExecute(String result)
    {
    	closeProgress();
    /*	System.out.println("result"+result);
    	returnData="2"+result;*/
    }
    
    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e)
        {
        	e.printStackTrace();
        	setFlagSuccess(false);
        	setTaskComplete(true);
        	closeProgress();
        	sb.append(Common.pleaseCheckNwConnection);
            e.printStackTrace();
        } 
        finally
        {
            try {
            	closeProgress();
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }   


} // end CallAPI
