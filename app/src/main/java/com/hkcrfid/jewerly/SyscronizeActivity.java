package com.hkcrfid.jewerly;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hkcrfid.common.Common;

import java.util.ArrayList;
public class SyscronizeActivity extends AppCompatActivity {

    ListView list;
    SysncronizeAdapter adapter;
    public SyscronizeActivity CustomListView = null;
    public ArrayList<SyncronizeListModel> CustomListViewValuesArr = new ArrayList<SyncronizeListModel>();
    Context context=this;
    private BroadcastReceiver mReceiver;
    Button syncronizeBt;
    CheckBox autoSyncronization;
    String AUTO_SYNCRONIZE="N";
    StringBuffer sbResut=null;
    Dialog dialogMessageSetting;
    TextView messageTV;
    ProgressBar progressBar ;
    Button completBTSyb;
    SharedPreferences sharedpreferences;
    IntentFilter intentFilter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.syncronize_activity);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        sharedpreferences = getApplicationContext().getSharedPreferences(Common.appPreference, MODE_PRIVATE);
        CustomListView = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Syncronize");
        setSupportActionBar(toolbar);
        /******** Take some data in Arraylist ( CustomListViewValuesArr ) ***********/
        setListData();

        Resources res =getResources();
        list= ( ListView )findViewById( R.id.list );  // List defined in XML ( See Below )
        autoSyncronization=(CheckBox) findViewById( R.id.autoSyncronization );
         AUTO_SYNCRONIZE=sharedpreferences.getString("AUTO_SYNCRONIZE","N");
        if(AUTO_SYNCRONIZE.equals("Y")) {
            autoSyncronization.setChecked(true);
        }else{
            autoSyncronization.setChecked(false);
        }
        autoSyncronization.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( ((CheckBox)v).isChecked() ) {
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("AUTO_SYNCRONIZE", "Y");
                    editor.commit();
                    AUTO_SYNCRONIZE="Y";
                }else{
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    editor.putString("AUTO_SYNCRONIZE", "N");
                    editor.commit();
                    AUTO_SYNCRONIZE="N";
                }

            }
        });
        syncronizeBt=(Button)findViewById( R.id.syncronizeBt );


        syncronizeBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sbResut=new StringBuffer();
                StringBuffer sb=new StringBuffer();
                for(int i=0;i<CustomListViewValuesArr.size();i++)
                {
                    if(CustomListViewValuesArr.get(i).isCheckBoxSelected()) {
                        if(i==0)
                        {
                            sb.append(CustomListViewValuesArr.get(i).getMenuName());
                        }else{
                            sb.append(","+CustomListViewValuesArr.get(i).getMenuName());
                        }

                    }

                }
                if(sb.length()>0) {
                    Intent i = new Intent(Common.appPackage + ".synchronization").putExtra("action_type", sb.toString());
                    context.sendBroadcast(i);
                    dialogMessageSettings();
                }else
                {
                    Toast.makeText(getApplicationContext(), "Please select atleast single option for sysncronizayion.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        /**************** Create Custom Adapter *********/
        adapter=new SysncronizeAdapter(CustomListView, CustomListViewValuesArr,res );
        list.setAdapter(adapter);

    }

    /****** Function to set data in ArrayList *************/
    public void setListData()
    {
/*
        SyncronizeListModel sched = new SyncronizeListModel();
        sched.setMenuName("Auto Syncronize ");
        sched.setImage("syncronize");
        CustomListViewValuesArr.add( sched );
*/
        SyncronizeListModel sched = new SyncronizeListModel();
        sched.setMenuName("Register Asset");
        sched.setImage("item_registration");
        CustomListViewValuesArr.add( sched );


        sched = new SyncronizeListModel();
        sched.setMenuName("Register Location");
        sched.setImage("location_reg");
        CustomListViewValuesArr.add( sched );




        sched = new SyncronizeListModel();
        sched.setMenuName("Inventory");
        sched.setImage("assignment");
        CustomListViewValuesArr.add( sched );

        sched = new SyncronizeListModel();
        sched.setMenuName("Search Asset List");
        sched.setImage("search");
        CustomListViewValuesArr.add( sched );



    }


    /*****************  This function used by adapter ****************/
    public void onItemClick(int mPosition)
    {
        SyncronizeListModel tempValues = ( SyncronizeListModel ) CustomListViewValuesArr.get(mPosition);

    }
    public void onDestroy()
    {
        System.out.println("Syncronize Activity Finish");


        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
       /*if(keyCode==67)
        {
            DownloadServiceCourse.activity=SyscronizeActivity.this;
            Intent i = new Intent(Common.appPackage + ".synchronization").putExtra("action_type", "image");
            context.sendBroadcast(i);

        }else*/
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if(mReceiver!=null)
            this.unregisterReceiver(mReceiver);
            Intent myIntent = new Intent(SyscronizeActivity.this, LoginActivity.class);
            startActivity(myIntent);
            finish();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
    intentFilter = new IntentFilter(
            Common.appPackage+".download");

    mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String []results = intent.getStringArrayExtra("result");

            for(int i=0;results!=null && i<results.length;i++)
            {
                System.out.println("=>>"+results[i]);
                sbResut.append(results[i]+"\n");

                if(progressBar!=null && results[i].contains("Stop Service!") )
                {
                    progressBar.setVisibility(View.INVISIBLE);
                    completBTSyb.setVisibility(View.VISIBLE);
                    completBTSyb.setEnabled(true);
                }
            }
            messageTV.setText(sbResut.toString());

        }
    };

		this.registerReceiver(mReceiver, intentFilter);
}

    public void dialogMessageSettings()
    {


        dialogMessageSetting = new Dialog(context);
        dialogMessageSetting.setContentView(R.layout.message_dialog);
        dialogMessageSetting.setTitle("Syncronization update");
        messageTV = (TextView) dialogMessageSetting.findViewById(R.id.messageTV);
        progressBar = (ProgressBar) dialogMessageSetting.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        completBTSyb = (Button) dialogMessageSetting.findViewById(R.id.completBT);
        completBTSyb.setVisibility(View.INVISIBLE);
        completBTSyb.setEnabled(false);
        completBTSyb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogMessageSetting.dismiss();
                progressBar.setVisibility(View.INVISIBLE);
                sbResut=new StringBuffer();
            }
        });
        dialogMessageSetting.show();

    }
}



