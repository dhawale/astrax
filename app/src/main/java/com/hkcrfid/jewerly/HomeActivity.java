package com.hkcrfid.jewerly;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.hkcrfid.bean.TrackingSessionBean;

import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity {

    ListView list;
    HomeAdapter adapter;
    public HomeActivity CustomListView = null;
    public ArrayList<HomeMenuListModel> CustomListViewValuesArr = new ArrayList<HomeMenuListModel>();
    SqlLite sqlLite;
    Context context=this;
    List<TrackingSessionBean> listTrackingSession;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_menu);

        sqlLite = new SqlLite(context);
        listTrackingSession = sqlLite.getTrackingSession();
        if(listTrackingSession==null || listTrackingSession.size()<=0)
        {
            Toast.makeText(getApplicationContext(),"Please download tracking session before login application",Toast.LENGTH_LONG).show();
            Intent myIntent = new Intent(HomeActivity.this, LoginActivity.class);
            startActivity(myIntent);
            finish();
        }else {

            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("Home");
            toolbar.setLogo(R.drawable.home);
            setSupportActionBar(toolbar);
            CustomListView = this;
            setListData();
            Resources res = getResources();
            list = (ListView) findViewById(R.id.list);  // List defined in XML ( See Below )

            /**************** Create Custom Adapter *********/
            adapter = new HomeAdapter(CustomListView, CustomListViewValuesArr, res);
            list.setAdapter(adapter);

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_rfid_settings) {
            return true;
        }else if (id == R.id.action_inventory)
        {
            Intent myIntent = new Intent(HomeActivity.this, InventoryActivity.class);
            startActivity(myIntent);
            finish();
            return true;
        }else if (id == R.id.action_asset_registration) {
            Intent myIntent = new Intent(HomeActivity.this, AssetRegistration.class);
            startActivity(myIntent);
            finish();
            return true;
        }else if (id == R.id.action_location_registration) {
            Intent myIntent = new Intent(HomeActivity.this, LocationRegistration.class);
            startActivity(myIntent);
            finish();
            return true;
        }else  if (id == R.id.actionSearch_Option) {
//            Intent myIntent = new Intent(HomeActivity.this, SingleSearchActivity.class);
            Intent myIntent = new Intent(HomeActivity.this, SearchAssetActivity.class);
            startActivity(myIntent);
            finish();
        }else  if (id == R.id.action_bulk_process) {
            Intent myIntent = new Intent(HomeActivity.this, BulkProcessAction.class);
            startActivity(myIntent);
            finish();
        }





        return super.onOptionsItemSelected(item);
    }
    /****** Function to set data in ArrayList *************/
    public void setListData()
    {
        int trackingSessionId=0;
        if(listTrackingSession!=null && listTrackingSession.size()>0)
        {
            trackingSessionId= listTrackingSession.get(0).getTrackingSessionId();
        }
        int REG_ASSET_COUNT=0;
        int REG_COUNT=sqlLite.getCountByType("REG_ASSET",trackingSessionId);
        int TOTAL_COUNT=sqlLite.getCountByType("TOTAL_ASSET",trackingSessionId);
        REG_ASSET_COUNT=REG_COUNT;
        HomeMenuListModel  sched = new HomeMenuListModel();
        sched.setMenuName("Register Asset");
        sched.setTextsummery(TOTAL_COUNT+"/"+REG_COUNT);
        sched.setImage("item_registration");
        CustomListViewValuesArr.add( sched );

         REG_COUNT=sqlLite.getCountByType("REG_LOCATION",trackingSessionId);
         TOTAL_COUNT=sqlLite.getCountByType("TOTAL_LOCATION",trackingSessionId);
        sched = new HomeMenuListModel();
        sched.setMenuName("Register Location");
        sched.setImage("location_reg");
        sched.setTextsummery(TOTAL_COUNT+"/"+REG_COUNT);
        CustomListViewValuesArr.add( sched );
System.out.println("trackingSessionId"+trackingSessionId);
        TOTAL_COUNT=sqlLite.getCountByType("TOTAL_INVENTORY",trackingSessionId);
       int TOTAL_SERVER_INVENTORY=sqlLite.getCountByType("TOTAL_SERVER_INVENTORY",trackingSessionId);

        System.out.println("TOTAL_COUNT"+TOTAL_COUNT);
        System.out.println("TOTAL_SERVER_INVENTORY"+TOTAL_SERVER_INVENTORY);
        sched = new HomeMenuListModel();
        sched.setMenuName("Inventory");
        sched.setImage("assignment");
        sched.setTextsummery(REG_ASSET_COUNT+"/"+(TOTAL_COUNT+TOTAL_SERVER_INVENTORY));
        CustomListViewValuesArr.add( sched );
/*
        sched = new HomeMenuListModel();
        sched.setMenuName("Search Asset List");
        sched.setImage("search");
        sched.setTextsummery(0+"/"+0);
        CustomListViewValuesArr.add( sched );
        */

        sched = new HomeMenuListModel();
        sched.setMenuName("Search Option");
        sched.setImage("search");
        sched.setTextsummery("");
        CustomListViewValuesArr.add( sched );

        sched = new HomeMenuListModel();
        sched.setMenuName("UnRegister");
        sched.setImage("unregister");
        sched.setTextsummery("");
        CustomListViewValuesArr.add( sched );

        sched = new HomeMenuListModel();
        sched.setMenuName("Exit");
        sched.setImage("exit");
        CustomListViewValuesArr.add( sched );
    }


    /*****************  This function used by adapter ****************/
    public void onItemClick(int mPosition)
    {
        HomeMenuListModel tempValues = ( HomeMenuListModel ) CustomListViewValuesArr.get(mPosition);

        if(tempValues.getMenuName().equals("Register Asset"))
        {
            Intent myIntent = new Intent(HomeActivity.this, AssetRegistration.class);
            startActivity(myIntent);
            finish();
        }else if(tempValues.getMenuName().equals("Register Location"))
        {
            Intent myIntent = new Intent(HomeActivity.this, LocationRegistration.class);
            startActivity(myIntent);
            finish();

        }else if(tempValues.getMenuName().equals("Inventory"))
        {
            Intent myIntent = new Intent(HomeActivity.this, InventoryActivity.class);
            startActivity(myIntent);
            finish();
        }else if(tempValues.getMenuName().equals("Search Asset List"))
        {
            //// TODO: 10/17/2017
        }else if(tempValues.getMenuName().equals("Search Option"))
        {

//            Intent myIntent = new Intent(HomeActivity.this, SingleSearchActivity.class);
            Intent myIntent = new Intent(HomeActivity.this, SearchAssetActivity.class);
            startActivity(myIntent);
            finish();
            //// TODO: 10/17/2017
        }else if(tempValues.getMenuName().equals("Exit"))
        {
            appExit();
        }else if(tempValues.getMenuName().equals("UnRegister"))
        {
            Intent myIntent = new Intent(HomeActivity.this, UnregistrationEpc.class);
            startActivity(myIntent);
            finish();
            //// TODO: 10/17/2017
        }else if(tempValues.getMenuName().equals("action_bulk_process"))
        {
            Intent myIntent = new Intent(HomeActivity.this, BulkProcessAction.class);
            startActivity(myIntent);
            finish();
            //// TODO: 10/17/2017
        }




}
    public void onDestroy()
    {
        System.out.println("Home Activity Finish");
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        System.out.println("keyCode"+keyCode);
       /* if(keyCode==67)
        {
            DownloadServiceCourse.activity=HomeActivity.this;
            Intent i = new Intent(Common.appPackage + ".synchronization").putExtra("action_type", "image");
            context.sendBroadcast(i);

        }else*/
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            appExit();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }


    public void appExit()
    {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent myIntent = new Intent(HomeActivity.this, LoginActivity.class);
                        startActivity(myIntent);
                        finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
