package com.hkcrfid.jewerly;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hkcrfid.bean.ItemDetailsBean;
import com.hkcrfid.common.Common;
import com.hkcrfid.common.HKCRfid;
import com.hkcrfid.common.Loger;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


public class SearchAssetActivity extends AppCompatActivity {
    private static final int FOUND = 1000;
    private static final int RESET = 2000;
    private static final int SET = 3000;
    static Dialog dialog;
    SearchAssetAdapter searchAssetAdapter;
    ItemDetailsBean item;
    TextView tvMessage;
    Date readingDate = null;
    MediaPlayer mpFound;
    MediaPlayer mpSearching;
    int counter = 0;
    boolean isDialogCalled = false;
    Handler listEpcHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            try {
                if (msg.what == FOUND) {
                    isFound=true;
                    mpSearching.pause();
                    mpFound.start();

                    Loger.error("ASSET FOUND","ASSET FOUND");

                    openDialog();
//                    Toast.makeText(getApplicationContext(), "FOUND", Toast.LENGTH_SHORT).show();
                }
                if (msg.what == RESET) {


                    if (dialog != null && dialog.isShowing()) {
                        tvMessage.setBackgroundResource(R.color.white);
                    }
                }
                else if(msg.what==SET){
//                    mpSearching.stop();
                    if (dialog != null && dialog.isShowing()) {
                        tvMessage.setBackgroundResource(R.color.green);
                    }
                }
//                String epcList = msg.getData().getString("epcList");
//                if (epcList != null) {
//                    Loger.error("EPC ", epcList);
//                    if (epcList.contains(item.getItemEpcId())) {
//                        if (dialog != null && dialog.isShowing()) {
//                            tvMessage.setTextColor(getResources().getColor(R.color.light_pink));
//                        }
//                    }
//                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    private AutoCompleteTextView auto_search_item;
    private TextView tvCode;
    private TextView tvDesc;
    private TextView tvEpcId;
    //    private boolean isReaading = false;
    //    private boolean autoScan=false;
    private LinearLayout linLaySearch;
    private boolean scanReading = false;
    private boolean isStartReading = false, isAutoScanReading = false;
    private Timer timer;
    boolean findingAsset=false;
    boolean isFound=false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_asset);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        auto_search_item = (AutoCompleteTextView) findViewById(R.id.auto_search_item);
        tvCode = (TextView) findViewById(R.id.tvCode);
        tvDesc = (TextView) findViewById(R.id.tvDesc);
        tvEpcId = (TextView) findViewById(R.id.tvEpcId);
        linLaySearch = (LinearLayout) findViewById(R.id.linLaySearch);
        linLaySearch.setVisibility(View.GONE);
        searchAssetAdapter = new SearchAssetAdapter(this, R.layout.activity_search_asset);
        onItemClick();
        mpFound = MediaPlayer.create(this, R.raw.found);
        mpSearching = MediaPlayer.create(this, R.raw.searchiing);
        auto_search_item.setAdapter(searchAssetAdapter);
        findingAsset = true;
        new UHFThread().start();
        if (timer == null) {
            startReading();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        Intent myIntent = new Intent(SearchAssetActivity.this, HomeActivity.class);
        startActivity(myIntent);
        finish();
        return true;
    }

    private void onItemClick() {
//        linLaySearch.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(isReaading){
//                    isReaading=false;
////                    autoScan=false;
//                    try {
//                        Thread.sleep(150);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//
//                }
//            }
//        });
        auto_search_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                auto_search_item.setText("");
            }
        });
        auto_search_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                item = searchAssetAdapter.getItem(position);
                if (item != null) {
                    tvCode.setText(item.getItemCode());
                    String description = item.getDescription();
                    if (description != null && description.length() > 0)
                        tvDesc.setText(description);
                    else {
                        tvDesc.setText("NA");
                    }
                    String itemEpcId = item.getItemEpcId();
                    if(itemEpcId!=null && itemEpcId.length()>20){
                        tvEpcId.setText(itemEpcId);
                    }else{
                        tvEpcId.setText("Asset is not Registered");
                    }

                    hideKeyBoard();
                }

            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
       /*if(keyCode==67)
        {
            DownloadServiceCourse.activity=AssetRegistration.this;
            Intent i = new Intent(Common.appPackage + ".synchronization").putExtra("action_type", "image");
            context.sendBroadcast(i);

        }else*/
        Loger.error("TAG", "KeyCode - " + keyCode);
        if (keyCode == 120) {
            isStartReading = item != null;
        } else if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent myIntent = new Intent(SearchAssetActivity.this, HomeActivity.class);
            startActivity(myIntent);
            finish();
            return true;
        }


        return super.onKeyDown(keyCode, event);
    }

    private void stopReading() {

        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        UfhData.Set_sound(false);
    }

    public void startReading() {

        if (timer == null) {
//            UfhData.Set_sound(true);
            UfhData.SoundFlag = false;

            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {

                    if (scanReading) return;
                    String[] epc = null;

                    if (isStartReading) {
                        scanReading = true;

                        mpSearching.start();
                        StringBuffer sb = new StringBuffer();
                        epc = HKCRfid.startReading();


                        if (item != null && epc != null) {
                            for (int i = 0; i < epc.length; i++) {
                                Loger.error("FINDER","FINDER");
                                if (epc[i].equalsIgnoreCase(item.getItemEpcId())) {
                                    Message m = new Message();
                                    m.what = FOUND;
                                    listEpcHandler.sendMessage(m);
                                    break;
                                }

                            }
                        }else{
                            UfhData.SoundFlag = false;
                        }
                        isStartReading = false;
                        if(epc!=null && epc.length>0) {
                            readingDate = new Date();
                        }
                    } else {
                        if (readingDate != null) {
                            long seconds = (new Date().getTime() - readingDate.getTime()) / 1000;

                            if (seconds > 0) {
                                UfhData.SoundFlag = false;
                                if (seconds > 100) {
                                    seconds = 2;
                                }
                            }
                        }
                    }
                    scanReading = false;

                }
            }, 0, Common.SCAN_INTERVAL);

        } else {

            if (timer != null) {
                timer.cancel();
                timer = null;
            }
            UfhData.Set_sound(false);
        }
    }

    void openDialog() {
        if (!isDialogCalled) {
            isDialogCalled = true;
            if (dialog == null || dialog.isShowing()) {
                dialog = new Dialog(this);

                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_found_asset);
                dialog.setCancelable(false);

//                dialog.setTitle("ASSET FOUND");
                tvMessage = (TextView) dialog.findViewById(R.id.tvMessage);
                tvMessage.setText("CODE : " + item.getItemCode());
                dialog.findViewById(R.id.btnOk).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        isDialogCalled = false;
                    }
                });
                dialog.setOnKeyListener(new Dialog.OnKeyListener() {

                    @Override
                    public boolean onKey(DialogInterface arg0, int keyCode,
                                         KeyEvent event) {
                        // TODO Auto-generated method stub
              /* if(keyCode==67)
                {
                    DownloadServiceCourse.activity=InventoryActivity.this;
                    Intent i = new Intent(Common.appPackage + ".synchronization").putExtra("action_type", "image");
                    context.sendBroadcast(i);

                }else*/
                        if (keyCode == 120) {
                            isStartReading = true;
                        }
                        return true;
                    }
                });
                dialog.show();
            } else {
                dialog.show();
            }
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopReading();
        findingAsset=false;
        scanReading = false;
        dialog = null;
//        isReaading = false;
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    class UHFThread extends Thread {
        int counter = 0;

        @Override
        public void run() {
            super.run();

            while (findingAsset) {

                try {
//                    Loger.error("THREAD", counter+ " found - "+isFound);
                    if (counter == 3 && isFound) {
                        Message m = new Message();
                        m.what = SET;

                        listEpcHandler.sendMessage(m);

                    }else{
                        isFound=false;
                    }
                    if(counter>3){
                        Message m = new Message();
                        m.what = RESET;

                        listEpcHandler.sendMessage(m);
                        counter=0;
                    }
                    Thread.sleep(200);
                    counter++;

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    void hideKeyBoard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
