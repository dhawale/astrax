package com.hkcrfid.jewerly;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.ResultReceiver;

import com.hkcrfid.bean.ItemDetailsBean;
import com.hkcrfid.bean.ItemLocationBean;
import com.hkcrfid.bean.LoginBean;
import com.hkcrfid.bean.TrackingInventoryBean;
import com.hkcrfid.bean.TrackingSessionBean;
import com.hkcrfid.bean.WSDataCommunication;
import com.hkcrfid.common.Common;

import org.json.simple.parser.JSONParser;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Kailas on 10/4/2017.
 */
public class DownloadService extends IntentService {
    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;
    private static final String TAG = "DownloadService";
    static int counter = 0;
    SqlLite db = null;
    SharedPreferences sharedpreferences;
    Context context = this;
    ResultReceiver receiver;
    String USERNAME = null;
    String action_type = null;
    Handler listHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {

            try {

                String message = msg.getData().getString("message");
                String operation = msg.getData().getString("operation");
                System.out.println("operation" + operation);
                System.out.println("message" + message);
                org.json.simple.JSONObject jSONObject = (org.json.simple.JSONObject) new JSONParser().parse(message);
                org.json.simple.JSONArray jSONArray = null;
                boolean successFlag = false;
                if (jSONObject.containsKey("list")) {
                    jSONArray = (org.json.simple.JSONArray) jSONObject.get("list");
                }
                //  System.out.println("jSONArray"+jSONArray.toJSONString());
                if (jSONObject.containsKey("successFlag")) {
                    successFlag = (Boolean) jSONObject.get("successFlag");
                }

                if (!successFlag || jSONArray == null || jSONArray.size() <= 0) {
                    Bundle bundle = new Bundle();
                    bundle.putStringArray("result", DownloadService.getArrayResult("Error: " + " " + operation));
                    receiver.send(STATUS_RUNNING, bundle);
                    sleep();
                } else if (successFlag && jSONArray != null && jSONArray.size() > 0) {

                    if (operation.equals("listUser")) {
                        db.addLogin(LoginBean.prepareBean(jSONArray));
                    } else if (operation.equals("listItems")) {
                        db.addItemDetailsList(ItemDetailsBean.prepareBean(jSONArray));
                    } else if (operation.equals("listItemLocation")) {
                        db.addItemLocationList(ItemLocationBean.prepareBean(jSONArray));
                    } else if (operation.equals("listTrackingSessionBean")) {
                        db.addTrackingSession(TrackingSessionBean.prepareBean(jSONArray));
                    }

                }


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public DownloadService() {
        super(DownloadService.class.getName());
    }

    public static String[] getArrayResult(String list) {
        String[] stockArr = {list};
        return stockArr;
    }

    public static void sleep() {
        try {
            Thread.sleep(100);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {


        sharedpreferences = getApplicationContext().getSharedPreferences(Common.appPreference, MODE_PRIVATE);
        USERNAME = sharedpreferences.getString("USERNAME", "");
        db = new SqlLite(context);

        receiver = intent.getParcelableExtra("receiver");
        if (intent.hasExtra("action_type"))
            action_type = intent.getStringExtra("action_type");

        Bundle bundle = new Bundle();
        bundle.putStringArray("result", getArrayResult("Service Started!"));
        receiver.send(STATUS_RUNNING, bundle);
        sleep();
        if (isOnline()) {

            if (action_type != null && action_type.contains("Register Asset")) {
                counter = 0;
                boolean suceess = false;
                while (counter < 3 && !suceess) {
                    suceess = itemDetailsPostData();
                }
            }
            if (action_type != null && action_type.contains("Register Location")) {
                counter = 0;
                boolean suceess = false;
                while (counter < 3 && !suceess) {
                    suceess = postItemLocationBean();
                    suceess = postItemLocationBean();
                }
            }
            if (action_type != null && action_type.contains("Inventory")) {
                counter = 0;
                boolean success = false;
                while (!success && counter < 3) {
                    success = postDataInventory();
                }
                downloadUserList();
                downloadListTrackingSessionBean();
                downloadListItemsLocation();
                downloadListItems();
            }
            sleep();
            bundle = new Bundle();
            bundle.putStringArray("result", getArrayResult("Stop Service!"));
            receiver.send(STATUS_RUNNING, bundle);
            sleep();

        } else {
            bundle = new Bundle();
            bundle.putStringArray("result", getArrayResult("" + Common.pleaseCheckNwConnection));
            receiver.send(STATUS_RUNNING, bundle);
        }


    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void downloadUserList() {

        Bundle bundle = new Bundle();
        bundle.putStringArray("result", getArrayResult("Login data downloding."));
        receiver.send(STATUS_RUNNING, bundle);
        sleep();
        try {

            String url = Common.appUrl + "getListUser.do";
            CallAPI callAPI = new CallAPI(context, "User List Download", false);
            callAPI.execute(url);
            callAPI.showProgress();

            ExecutionTask executionTask = new ExecutionTask(callAPI, "listUser");
            executionTask.start();
            while (!executionTask.callAPI.getTaskComplete()) {
                Thread.sleep(60);
            }

            if (executionTask.callAPI.getTaskComplete() && executionTask.callAPI.getFlagSuccess()) {
                bundle = new Bundle();
                bundle.putStringArray("result", getArrayResult("Login data successfully downloded."));
                receiver.send(STATUS_RUNNING, bundle);
                sleep();
            } else {
                bundle = new Bundle();
                bundle.putStringArray("result", getArrayResult("Login data fail to downloding."));
                receiver.send(STATUS_RUNNING, bundle);
                sleep();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void downloadListItems() {

        try {
            Bundle bundle = new Bundle();
            bundle.putStringArray("result", getArrayResult("Asset downloding."));
            receiver.send(STATUS_RUNNING, bundle);
            sleep();
            String url = Common.appUrl + "getListItem.do";
            CallAPI callAPI = new CallAPI(context, "Item List Download", false);
            callAPI.execute(url);
            callAPI.showProgress();

            ExecutionTask executionTask = new ExecutionTask(callAPI, "listItems");
            executionTask.start();
            while (!executionTask.callAPI.getTaskComplete()) {
                Thread.sleep(60);
            }

            if (executionTask.callAPI.getTaskComplete() && executionTask.callAPI.getFlagSuccess()) {
                bundle = new Bundle();
                bundle.putStringArray("result", getArrayResult("Asset successfully downloded."));
                receiver.send(STATUS_RUNNING, bundle);
                sleep();
            } else {
                bundle = new Bundle();
                bundle.putStringArray("result", getArrayResult("Asset fail to downloding."));
                receiver.send(STATUS_RUNNING, bundle);
                sleep();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void downloadListItemsLocation() {

        Bundle bundle = new Bundle();
        bundle.putStringArray("result", getArrayResult("Asset Location downloding."));
        receiver.send(STATUS_RUNNING, bundle);
        sleep();
        try {

            String url = Common.appUrl + "getListItemLocation.do";
            CallAPI callAPI = new CallAPI(context, "Item Location List Download", false);
            callAPI.execute(url);
            callAPI.showProgress();

            ExecutionTask executionTask = new ExecutionTask(callAPI, "listItemLocation");
            executionTask.start();
            while (!executionTask.callAPI.getTaskComplete()) {
                Thread.sleep(60);
            }
            if (executionTask.callAPI.getTaskComplete() && executionTask.callAPI.getFlagSuccess()) {
                bundle = new Bundle();
                bundle.putStringArray("result", getArrayResult("Asset Location successfully downloded."));
                receiver.send(STATUS_RUNNING, bundle);
                sleep();
            } else {
                bundle = new Bundle();
                bundle.putStringArray("result", getArrayResult("Asset Location fail to downloding."));
                receiver.send(STATUS_RUNNING, bundle);
                sleep();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void downloadListTrackingSessionBean() {

        Bundle bundle = new Bundle();
        bundle.putStringArray("result", getArrayResult("Tracking Session downloing."));
        receiver.send(STATUS_RUNNING, bundle);
        sleep();
        try {

            String url = Common.appUrl + "getListTrackingSessionBean.do";
            CallAPI callAPI = new CallAPI(context, "Tracking Session Bean Download", false);
            callAPI.execute(url);
            callAPI.showProgress();
            ExecutionTask executionTask = new ExecutionTask(callAPI, "listTrackingSessionBean");
            executionTask.start();
            while (!executionTask.callAPI.getTaskComplete()) {
                Thread.sleep(60);
            }

            if (executionTask.callAPI.getTaskComplete() && executionTask.callAPI.getFlagSuccess()) {
                bundle = new Bundle();
                bundle.putStringArray("result", getArrayResult("Tracking Session successfully downloded."));
                receiver.send(STATUS_RUNNING, bundle);
                sleep();
            } else {
                bundle = new Bundle();
                bundle.putStringArray("result", getArrayResult("Tracking Session  fail to downloding."));
                receiver.send(STATUS_RUNNING, bundle);
                sleep();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public boolean itemDetailsPostData() {
        boolean success = false;
        Bundle bundle = null;
        if (counter == 0) {
            bundle = new Bundle();
            bundle.putStringArray("result", getArrayResult("Register Asset uploding."));
            receiver.send(STATUS_RUNNING, bundle);
        }
        ItemDetailsBeanHttpRequestTask task = new ItemDetailsBeanHttpRequestTask();
        task.execute();
        sleep();
        while (!task.taskComplete) {
            try {
                Thread.sleep(60);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        counter++;
        if (task.taskComplete && task.flagSuccess) {
            success = true;
            bundle = new Bundle();
            bundle.putStringArray("result", getArrayResult(task.wsDataCommunication.getMessage()));
            receiver.send(STATUS_RUNNING, bundle);
            sleep();
        } else {
            if (counter > 2) {
                bundle = new Bundle();
                bundle.putStringArray("result", getArrayResult("Register Asset fail to uploding."));
                receiver.send(STATUS_RUNNING, bundle);
                sleep();
            }
        }
        return success;
    }

    public boolean postItemLocationBean() {
        boolean sccess = false;
        Bundle bundle = null;
        if (counter == 0) {
            bundle = new Bundle();
            bundle.putStringArray("result", getArrayResult("Register Location uploding."));
            receiver.send(STATUS_RUNNING, bundle);
            sleep();
        }
        ItemLocationBeanHttpRequestTask requestTask = new ItemLocationBeanHttpRequestTask();
        requestTask.execute();

        while (!requestTask.taskComplete) {
            try {
                Thread.sleep(60);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        counter++;
        if (requestTask.taskComplete && requestTask.flagSuccess) {
            sccess = true;
            bundle = new Bundle();
            bundle.putStringArray("result", getArrayResult(requestTask.wsDataCommunication.getMessage()));
            receiver.send(STATUS_RUNNING, bundle);
            sleep();
        } else {
            if (counter > 2)
                bundle = new Bundle();
            bundle.putStringArray("result", getArrayResult("Register Location fail to uploding."));
            receiver.send(STATUS_RUNNING, bundle);
            sleep();
        }
        return sccess;
    }

    public boolean postDataInventory() {
        boolean success = false;
        Bundle bundle = null;
        if (counter == 0) {
            bundle = new Bundle();
            bundle.putStringArray("result", getArrayResult("Inventory uploding."));
            receiver.send(STATUS_RUNNING, bundle);
            sleep();
        }
        TrackingInventoryHttpRequestTask trackingInventoryHttpRequestTask = new TrackingInventoryHttpRequestTask();
        trackingInventoryHttpRequestTask.execute();
        while (!trackingInventoryHttpRequestTask.taskComplete) {
            try {
                Thread.sleep(60);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        counter++;
        if (trackingInventoryHttpRequestTask.taskComplete && trackingInventoryHttpRequestTask.flagSuccess) {
            success = true;
            bundle = new Bundle();
            bundle.putStringArray("result", getArrayResult(trackingInventoryHttpRequestTask.wsDataCommunication.getMessage()));
            receiver.send(STATUS_RUNNING, bundle);
            sleep();
        } else {
            if (counter > 2) {
                bundle = new Bundle();
                bundle.putStringArray("result", getArrayResult("Inventory uploding fail to uploding.."));
                receiver.send(STATUS_RUNNING, bundle);
                sleep();
            }
        }
        return success;
    }

    public class ExecutionTask extends Thread {
        CallAPI callAPI = null;
        String mode = null;

        public ExecutionTask(CallAPI callAPI, String mode) {
            this.callAPI = callAPI;
            this.mode = mode;
        }

        public void run() {

            try {

                while (!callAPI.getTaskComplete()) {
                    Thread.sleep(60);
                }

                if (callAPI.getFlagSuccess()) {
                    if (callAPI.getReturnData() != null && callAPI.getReturnData().length() > 0) {
                        Message m = new Message();
                        Bundle b = new Bundle();
                        b.putString("operation", mode);
                        b.putString("message", callAPI.getReturnData());
                        m.setData(b);
                        listHandler.sendMessage(m);
                        Thread.sleep(60);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }

    }

    private class ItemDetailsBeanHttpRequestTask extends AsyncTask<Void, Void, WSDataCommunication<ItemDetailsBean>> {
        WSDataCommunication<ItemDetailsBean> wsDataCommunication = null;
        private Boolean taskComplete = false;
        private Boolean flagSuccess = false;

        @Override
        protected WSDataCommunication<ItemDetailsBean> doInBackground(Void... params) {
            try {

                String url = Common.appUrl + "regListItem.do";
                RestTemplate restTemplate = new RestTemplate(true);
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                wsDataCommunication = new WSDataCommunication<ItemDetailsBean>();
                wsDataCommunication.setCallingPage("ItemDetailsRegistration");
                wsDataCommunication.setOperation("ItemDetailsRegistration");
                wsDataCommunication.setList(db.getItemDetailsReg());
                if (wsDataCommunication.getList() != null && wsDataCommunication.getList().size() > 0) {
                    for (int i = 0; i < wsDataCommunication.getList().size(); i++) {

                        String date = null;
                        date = wsDataCommunication.getList().get(i).getPurchaseDate();
                        wsDataCommunication.getList().get(i).setPurchaseDate(null);
                        wsDataCommunication.getList().get(i).purchaseDateUtc = date;


                        date = wsDataCommunication.getList().get(i).getWarrantyDate();
                        wsDataCommunication.getList().get(i).setWarrantyDate(null);
                        wsDataCommunication.getList().get(i).warrantyDateUtc = date;


                        date = wsDataCommunication.getList().get(i).getCreatedOn();
                        wsDataCommunication.getList().get(i).setCreatedOn(null);
                        wsDataCommunication.getList().get(i).createdOnUtc = date;

                        date = wsDataCommunication.getList().get(i).getModifyOn();
                        wsDataCommunication.getList().get(i).setModifyOn(null);
                        wsDataCommunication.getList().get(i).modifyOnUtc = date;

                    }
                    WSDataCommunication wsDataCommunication1 = null;
                    try {
                        wsDataCommunication1 = restTemplate.postForObject(url, wsDataCommunication, WSDataCommunication.class);
                    } catch (ResourceAccessException e) {
                        wsDataCommunication1 = restTemplate.postForObject(url, wsDataCommunication, WSDataCommunication.class);
                        e.printStackTrace();
                    }
                    if (wsDataCommunication1 != null) {
                        wsDataCommunication.setSuccessFlag(wsDataCommunication1.getSuccessFlag());
                        wsDataCommunication.setMessage(wsDataCommunication1.getMessage());
                    }
                } else {
                    wsDataCommunication.setMessage("No Item Registration Data found upload");
                    wsDataCommunication.setSuccessFlag(true);
                }
                return wsDataCommunication;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(WSDataCommunication<ItemDetailsBean> wsDataCommunication) {
            taskComplete = true;
            this.wsDataCommunication = wsDataCommunication;
            if (wsDataCommunication != null) {
                System.out.println("onPostExecute==>>" + wsDataCommunication.getSuccessFlag());
                System.out.println("getMessage==>>" + wsDataCommunication.getMessage());
                if (wsDataCommunication.getSuccessFlag()) {
                    flagSuccess = true;
                    for (int i = 0; wsDataCommunication.getList() != null && i < wsDataCommunication.getList().size(); i++) {
                        wsDataCommunication.getList().get(i).setNewItemReg("D");
                        db.updateItemDetails(wsDataCommunication.getList().get(i));
                    }
                }
            }
/*
            Message m = new Message();
            Bundle b = new Bundle();
            b.putString("operation",wsDataCommunication.getOperation());

            if(wsDataCommunication.getSuccessFlag())
                b.putString("message", "Y");
            else
                b.putString("message", "N");
            m.setData(b);
            listHandler.sendMessage(m);
            */
        }

    }

    private class ItemLocationBeanHttpRequestTask extends AsyncTask<Void, Void, WSDataCommunication<ItemLocationBean>> {
        WSDataCommunication<ItemLocationBean> wsDataCommunication;
        private Boolean taskComplete = false;
        private Boolean flagSuccess = false;

        @Override
        protected WSDataCommunication<ItemLocationBean> doInBackground(Void... params) {
            try {
                System.out.println("doInBackground");

                String url = Common.appUrl + "setListItemLocation.do";
                RestTemplate restTemplate = new RestTemplate(true);
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                wsDataCommunication = new WSDataCommunication<ItemLocationBean>();
                wsDataCommunication.setCallingPage("ItemLocationBeanRegistration");
                wsDataCommunication.setOperation("ItemLocationBeanRegistration");
                wsDataCommunication.setList(db.getItemLocationreg());
                if (wsDataCommunication.getList() != null && wsDataCommunication.getList().size() > 0) {

                    for (int i = 0; i < wsDataCommunication.getList().size(); i++) {

                        String date = null;
                        date = wsDataCommunication.getList().get(i).getCreatedOn();
                        wsDataCommunication.getList().get(i).setCreatedOn(null);
                        wsDataCommunication.getList().get(i).createdOnUtc = date;

                        date = wsDataCommunication.getList().get(i).getModifyOn();
                        wsDataCommunication.getList().get(i).setModifyOn(null);
                        wsDataCommunication.getList().get(i).modifyOnUtc = date;
                    }
                    WSDataCommunication<ItemLocationBean> wsDataCommunication1 = null;
                    try {
                        wsDataCommunication1 = restTemplate.postForObject(url, wsDataCommunication, WSDataCommunication.class);
                    } catch (ResourceAccessException e) {
                        e.printStackTrace();
                        wsDataCommunication1 = restTemplate.postForObject(url, wsDataCommunication, WSDataCommunication.class);
                    }
                    if (wsDataCommunication1 != null) {
                        wsDataCommunication.setSuccessFlag(wsDataCommunication1.getSuccessFlag());
                        wsDataCommunication.setMessage(wsDataCommunication1.getMessage());
                    }
                } else {
                    wsDataCommunication.setSuccessFlag(true);
                    wsDataCommunication.setMessage("No Item Location Registration data found for upload");

                }
                return wsDataCommunication;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(WSDataCommunication<ItemLocationBean> wsDataCommunication) {
            taskComplete = true;
            this.wsDataCommunication = wsDataCommunication;
            if (wsDataCommunication != null) {
                if (wsDataCommunication.getSuccessFlag()) {
                    flagSuccess = true;
                    for (int i = 0; wsDataCommunication.getList() != null && i < wsDataCommunication.getList().size(); i++) {
                        wsDataCommunication.getList().get(i).setNewItemLocationReg("D");
                        db.updateItemLocation(wsDataCommunication.getList().get(i));
                    }
                }
            }
        }

    }

    private class TrackingInventoryHttpRequestTask extends AsyncTask<Void, Void, WSDataCommunication<TrackingInventoryBean>> {
        WSDataCommunication<TrackingInventoryBean> wsDataCommunication;
        private Boolean taskComplete = false;
        private Boolean flagSuccess = false;

        @Override
        protected WSDataCommunication<TrackingInventoryBean> doInBackground(Void... params) {
            try {
                System.out.println("doInBackground");

                String url = Common.appUrl + "setListTrackingInventory.do";
                RestTemplate restTemplate = new RestTemplate(true);
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                wsDataCommunication = new WSDataCommunication<TrackingInventoryBean>();
                wsDataCommunication.setCallingPage("TrackingInventoryBeanCP");
                wsDataCommunication.setOperation("TrackingInventoryBeanOPERATION");

                wsDataCommunication.setList(db.getTrackingInventory());

                if (wsDataCommunication.getList() != null && wsDataCommunication.getList().size() > 0) {
                    for (int i = 0; i < wsDataCommunication.getList().size(); i++) {
                        String date = null;

                        date = wsDataCommunication.getList().get(i).getCreatedOn();

                        wsDataCommunication.getList().get(i).setCreatedOn(null);
                        wsDataCommunication.getList().get(i).createdOnUtc = date;

                    }
                    try {
                        wsDataCommunication = restTemplate.postForObject(url, wsDataCommunication, WSDataCommunication.class);
                    } catch (ResourceAccessException e) {
                        e.printStackTrace();
                        wsDataCommunication = restTemplate.postForObject(url, wsDataCommunication, WSDataCommunication.class);
                    }
                } else {
                    wsDataCommunication.setSuccessFlag(true);
                    wsDataCommunication.setMessage("No data inventory_menu for upload");
                }
                return wsDataCommunication;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(WSDataCommunication<TrackingInventoryBean> wsDataCommunication) {
            taskComplete = true;
            this.wsDataCommunication = wsDataCommunication;
            if (wsDataCommunication != null) {
                System.out.println("onPostExecute==>>" + wsDataCommunication.getSuccessFlag());
                if (wsDataCommunication.getSuccessFlag()) {
                    flagSuccess = true;
                    db.deleteTable("TrackingInventory");
                }
            }
        }

    }
}
