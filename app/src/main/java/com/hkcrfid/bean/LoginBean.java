package com.hkcrfid.bean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
public class LoginBean implements Serializable
{
private static final long serialVersionUID = -723583058586873479L;
private Integer loginId=0;
public Integer getLoginId()
{
 return loginId;
}
public void  setLoginId(Integer loginId)
{
 this.loginId=loginId;
}
private String  userName=null;
public String getUserName()
{
 return userName;
}
public void  setUserName(String userName)
{
 this.userName=userName;
}

public String  createdOnUtc=null;
public String  modifyOnUtc=null;


private String  userLoginId=null;
public String getUserLoginId()
{
 return userLoginId;
}
public void  setUserLoginId(String userLoginId)
{
 this.userLoginId=userLoginId;
}
private String  password=null;
public String getPassword()
{
 return password;
}
public void  setPassword(String password)
{
 this.password=password;
}
private Integer roleId=0;
public Integer getRoleId()
{
 return roleId;
}
public void  setRoleId(Integer roleId)
{
 this.roleId=roleId;
}
private Integer createdBy=0;
public Integer getCreatedBy()
{
 return createdBy;
}
public void  setCreatedBy(Integer createdBy)
{
 this.createdBy=createdBy;
}
private String  createdOn=null;
public String getCreatedOn()
{
 return createdOn;
}
public void  setCreatedOn(String createdOn)
{
 this.createdOn=createdOn;

}
private Integer modifyBy=0;
public Integer getModifyBy()
{
 return modifyBy;
}
public void  setModifyBy(Integer modifyBy)
{
 this.modifyBy=modifyBy;
}
private String  modifyOn=null;
public String getModifyOn()
{
 return modifyOn;
}
public void  setModifyOn(String modifyOn)
{
 this.modifyOn=modifyOn;

}
private String  active=null;
public String getActive()
{
 return active;
}
public void  setActive(String active)
{
 this.active=active;
}

public static List<LoginBean> prepareBean(org.json.simple.JSONArray jSONArray)
{
ArrayList<LoginBean>beans=null;
for(int i=0;jSONArray!=null && i<jSONArray.size();i++)
{

		if(i==0)
		{
		 beans=new ArrayList<LoginBean>();
		}

		LoginBean bean=new LoginBean();
		org.json.simple.JSONObject json1=(org.json.simple.JSONObject)jSONArray.get(i);
		if(json1.containsKey("loginId"))
		{
		 bean.setLoginId(Integer.valueOf(""+(java.lang.Long)json1.get("loginId")));
		}
		if(json1.containsKey("userName"))
		{
		 bean.setUserName((String)json1.get("userName"));
		}
		if(json1.containsKey("userLoginId"))
		{
		 bean.setUserLoginId((String)json1.get("userLoginId"));
		}
		if(json1.containsKey("password"))
		{
		 bean.setPassword((String)json1.get("password"));
		}
		if(json1.containsKey("roleId"))
		{
		 bean.setRoleId((int)json1.get("roleId"));
		}
		if(json1.containsKey("createdBy"))
		{
		 bean.setCreatedBy(Integer.valueOf(""+(java.lang.Long)json1.get("createdBy")));
		}

		if(json1.containsKey("modifyBy"))
		{
		 bean.setModifyBy(Integer.valueOf(""+(java.lang.Long)json1.get("modifyBy")));
		}


		if(json1.containsKey("createdOnUtc"))
		{
			bean.setCreatedOn((String)json1.get("createdOnUtc"));
		}
		if(json1.containsKey("modifyOnUtc"))
		{
			bean.setModifyOn((String)json1.get("modifyOnUtc"));
		}
		if(json1.containsKey("active"))
		{
		  bean.setActive(""+(java.lang.Boolean)json1.get("active"));
		}

	beans.add(bean);
}


return beans;
}

}
