package com.hkcrfid.bean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
public class DeviceLocationBean implements Serializable
{
private static final long serialVersionUID = -723583058586873479L;
private Integer deviceLocationId=0;
public Integer getDeviceLocationId()
{
 return deviceLocationId;
}
public void  setDeviceLocationId(Integer deviceLocationId)
{
 this.deviceLocationId=deviceLocationId;
}

	private String  createdOnUtc=null;
	private String  modifyOnUtc=null;

	public void setCreatedOnUtc(String createdOnUtc) {
		this.createdOnUtc = createdOnUtc;
	}

	public void setModifyOnUtc(String modifyOnUtc) {
		this.modifyOnUtc = modifyOnUtc;
	}
private String  deviceLocation=null;
public String getDeviceLocation()
{
 return deviceLocation;
}
public void  setDeviceLocation(String deviceLocation)
{
 this.deviceLocation=deviceLocation;
}
private Integer deviceLocationParentId=0;
public Integer getDeviceLocationParentId()
{
 return deviceLocationParentId;
}
public void  setDeviceLocationParentId(Integer deviceLocationParentId)
{
 this.deviceLocationParentId=deviceLocationParentId;
}
private String  useFor=null;
public String getUseFor()
{
 return useFor;
}
public void  setUseFor(String useFor)
{
 this.useFor=useFor;
}
private Integer createdBy=0;
public Integer getCreatedBy()
{
 return createdBy;
}
public void  setCreatedBy(Integer createdBy)
{
 this.createdBy=createdBy;
}
private String  createdOn=null;
public String getCreatedOn()
{
 return createdOn;

}
public void  setCreatedOn(String createdOn)
{
 this.createdOn=createdOn;

}
private Integer modifyBy=0;
public Integer getModifyBy()
{
 return modifyBy;
}
public void  setModifyBy(Integer modifyBy)
{
 this.modifyBy=modifyBy;
}
private String  modifyOn=null;
public String getModifyOn()
{
 return modifyOn;
}
public void  setModifyOn(String modifyOn)
{
 this.modifyOn=modifyOn;

}
private String  active=null;
public String getActive()
{
 return active;
}
public void  setActive(String active)
{
 this.active=active;
}

public static List<DeviceLocationBean> prepareBean(org.json.simple.JSONArray jSONArray)
{
ArrayList<DeviceLocationBean>beans=null;
for(int i=0;jSONArray!=null && i<jSONArray.size();i++)
{

		if(i==0)
		{
		 beans=new ArrayList<DeviceLocationBean>();
		}

		DeviceLocationBean bean=new DeviceLocationBean();
		org.json.simple.JSONObject json1=(org.json.simple.JSONObject)jSONArray.get(i);
		if(json1.containsKey("deviceLocationId"))
		{
		 bean.setDeviceLocationId((int)json1.get("deviceLocationId"));
		}
		if(json1.containsKey("deviceLocation"))
		{
		 bean.setDeviceLocation((String)json1.get("deviceLocation"));
		}
		if(json1.containsKey("deviceLocationParentId"))
		{
		 bean.setDeviceLocationParentId((int)json1.get("deviceLocationParentId"));
		}
		if(json1.containsKey("useFor"))
		{
		 bean.setUseFor((String)json1.get("useFor"));
		}
		if(json1.containsKey("createdBy"))
		{
		 bean.setCreatedBy((int)json1.get("createdBy"));
		}

		if(json1.containsKey("modifyBy"))
		{
		 bean.setModifyBy((int)json1.get("modifyBy"));
		}

		if(json1.containsKey("createdOnUtc"))
		{
			bean.setCreatedOn((String)json1.get("createdOnUtc"));
		}
		if(json1.containsKey("modifyOnUtc"))
		{
			bean.setModifyOn((String)json1.get("modifyOnUtc"));
		}
		if(json1.containsKey("active"))
		{
		 bean.setActive((String)json1.get("active"));
		}
beans.add(bean);
}


return beans;
}

}
