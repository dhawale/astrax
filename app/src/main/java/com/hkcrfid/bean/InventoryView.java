package com.hkcrfid.bean;

import com.android.internal.util.Predicate;

import java.util.List;

/**
 * Created by Shree on 10/14/2017.
 */

public class InventoryView {
    public ItemDetailsBean getItemDetailsBean() {
        return itemDetailsBean;
    }

    public void setItemDetailsBean(ItemDetailsBean itemDetailsBean) {
        this.itemDetailsBean = itemDetailsBean;
    }

    private ItemDetailsBean itemDetailsBean = null;

    public ItemLocationBean getLastItemLocationBean() {
        return lastItemLocationBean;
    }

    public void setLastItemLocationBean(ItemLocationBean lastItemLocationBean) {
        this.lastItemLocationBean = lastItemLocationBean;
    }

    private ItemLocationBean lastItemLocationBean = null;

    public boolean isItemBeanSelected() {
        return itemBeanSelected;
    }

    public void setItemBeanSelected(boolean itemBeanSelected) {
        this.itemBeanSelected = itemBeanSelected;
    }

    private boolean itemBeanSelected = false;

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    private Integer createdBy = 0;

    public String getCreatedOnUtc() {
        return createdOnUtc;
    }

    public void setCreatedOnUtc(String createdOnUtc) {
        this.createdOnUtc = createdOnUtc;
    }

    private String createdOnUtc = null;

    private static synchronized Predicate<InventoryView>  equals2InventoryView(final int id) {
        return new Predicate<InventoryView>() {

            public boolean apply(InventoryView dataPoint) {
                return dataPoint.getItemDetailsBean().getItemId() == id;
            }
        };
    }

    public static synchronized void removeItem(List<InventoryView> listRemove, List<InventoryView> newList)
    {
        try {
            for(InventoryView newBean: newList) {
                if (listRemove != null && listRemove.size() > 0) {
                    for (InventoryView view : listRemove) {
                        if (equals2InventoryView(newBean.getItemDetailsBean().getItemId()).apply(view)) {
                            listRemove.remove(view);
                            break;
                        }
                    }
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }
/*
    public static void removeNotFound(String[] list, HashMap<String, String> epcMap,List<InventoryView> currentScannedList)
    {
        for(int i=0;list!=null && i<list.length;i++)
        {
            boolean success=false;
            for(int j=0;currentScannedList!=null && j<currentScannedList.size();j++)
            {
                if(currentScannedList.get(j).getItemDetailsBean().getItemEpcId().equals(list[i]))
                {
                    success=true;
                    break;
                }
            }
            if(!success)
            {
                if(epcMap.containsKey(list[i])) {
                    epcMap.remove(list[i]);
                    System.out.println("list[i]"+list[i]);
                }
            }
        }
    }
    */
    public static synchronized void addNewItem(List<InventoryView> listAdd, List<InventoryView> currentScannedList)
    {
        try {

            for(InventoryView newBean: currentScannedList) {
                boolean success=false;
                InventoryView beanAdd=newBean;;
                if (listAdd != null && listAdd.size() > 0) {
                    for (InventoryView view : listAdd) {

                        if (equals2InventoryView(newBean.getItemDetailsBean().getItemId()).apply(view)) {
                            success=true;
                            break;
                        }
                    }

                    if(!success)
                    {
                        listAdd.add(0,beanAdd);
                    }
                }else if(listAdd!=null )
                {
                    listAdd.add(0,beanAdd);
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }
}