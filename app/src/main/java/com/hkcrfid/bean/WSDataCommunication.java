package com.hkcrfid.bean;

import java.util.List;

public class WSDataCommunication<T> {


    private List<T> list = null;
    private Boolean successFlag = null;
    private String message = null;
    private String callingPage = null;
    private String operation = null;

    public List<T> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }

    public Boolean getSuccessFlag() {
        return successFlag;
    }

    public void setSuccessFlag(Boolean successFlag) {
        this.successFlag = successFlag;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCallingPage() {
        return callingPage;
    }

    public void setCallingPage(String callingPage) {
        this.callingPage = callingPage;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

}
