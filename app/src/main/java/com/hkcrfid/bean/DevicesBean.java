package com.hkcrfid.bean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
public class DevicesBean implements Serializable
{
private static final long serialVersionUID = -723583058586873479L;
private Integer deviceId=0;
public Integer getDeviceId()
{
 return deviceId;
}
public void  setDeviceId(Integer deviceId)
{
 this.deviceId=deviceId;
}
private String  deviceName=null;
public String getDeviceName()
{
 return deviceName;
}
public void  setDeviceName(String deviceName)
{
 this.deviceName=deviceName;
}
private Integer deviceParentId=0;
public Integer getDeviceParentId()
{
 return deviceParentId;
}
public void  setDeviceParentId(Integer deviceParentId)
{
 this.deviceParentId=deviceParentId;
}
private String  deviceIP=null;
public String getDeviceIP()
{
 return deviceIP;
}
public void  setDeviceIP(String deviceIP)
{
 this.deviceIP=deviceIP;
}

public String  createdOnUtc=null;
public String  modifyOnUtc=null;

private Integer deviceLocationId=0;
public Integer getDeviceLocationId()
{
 return deviceLocationId;
}
public void  setDeviceLocationId(Integer deviceLocationId)
{
 this.deviceLocationId=deviceLocationId;
}
private String  deviceSerialNo=null;
public String getDeviceSerialNo()
{
 return deviceSerialNo;
}
public void  setDeviceSerialNo(String deviceSerialNo)
{
 this.deviceSerialNo=deviceSerialNo;
}
private Integer createdBy=0;
public Integer getCreatedBy()
{
 return createdBy;
}
public void  setCreatedBy(Integer createdBy)
{
 this.createdBy=createdBy;
}
private String  createdOn=null;
public String getCreatedOn()
{
 return createdOn;
}
public void  setCreatedOn(String createdOn)
{
 this.createdOn=createdOn;

}
private Integer modifyBy=0;
public Integer getModifyBy()
{
 return modifyBy;
}
public void  setModifyBy(Integer modifyBy)
{
 this.modifyBy=modifyBy;
}
private String  modifyOn=null;
public String getModifyOn()
{
 return modifyOn;
}
public void  setModifyOn(String modifyOn)
{
 this.modifyOn=modifyOn;


}
private String  active=null;
public String getActive()
{
 return active;
}
public void  setActive(String active)
{
 this.active=active;
}

public static List<DevicesBean> prepareBean(org.json.simple.JSONArray jSONArray)
{
ArrayList<DevicesBean>beans=null;
for(int i=0;jSONArray!=null && i<jSONArray.size();i++)
{

		if(i==0)
		{
		 beans=new ArrayList<DevicesBean>();
		}

		DevicesBean bean=new DevicesBean();
		org.json.simple.JSONObject json1=(org.json.simple.JSONObject)jSONArray.get(i);
		if(json1.containsKey("deviceId"))
		{
		 bean.setDeviceId((int)json1.get("deviceId"));
		}
		if(json1.containsKey("deviceName"))
		{
		 bean.setDeviceName((String)json1.get("deviceName"));
		}
		if(json1.containsKey("deviceParentId"))
		{
		 bean.setDeviceParentId((int)json1.get("deviceParentId"));
		}
		if(json1.containsKey("deviceIP"))
		{
		 bean.setDeviceIP((String)json1.get("deviceIP"));
		}
		if(json1.containsKey("deviceLocationId"))
		{
		 bean.setDeviceLocationId((int)json1.get("deviceLocationId"));
		}
		if(json1.containsKey("deviceSerialNo"))
		{
		 bean.setDeviceSerialNo((String)json1.get("deviceSerialNo"));
		}
		if(json1.containsKey("createdBy"))
		{
		 bean.setCreatedBy((int)json1.get("createdBy"));
		}

		if(json1.containsKey("modifyBy"))
		{
		 bean.setModifyBy((int)json1.get("modifyBy"));
		}
		if(json1.containsKey("createdOnUtc"))
		{
			bean.setCreatedOn((String)json1.get("createdOnUtc"));
		}
		if(json1.containsKey("modifyOnUtc"))
		{
			bean.setModifyOn((String)json1.get("modifyOnUtc"));
		}

		if(json1.containsKey("active"))
		{
		 bean.setActive((String)json1.get("active"));
		}
beans.add(bean);
}


return beans;
}

}
