package com.hkcrfid.bean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
public class ItemDetailsBean implements Serializable
{
private static final long serialVersionUID = -723583058586873479L;
private Integer itemId=0;
public Integer getItemId()
{
 return itemId;
}
public void  setItemId(Integer itemId)
{
 this.itemId=itemId;
}
private String  itemCode=null;
public String getItemCode()
{
 return itemCode;
}
public void  setItemCode(String itemCode)
{
 this.itemCode=itemCode;
}
private String  description=null;
public String getDescription()
{
 return description;
}
public void  setDescription(String description)
{
 this.description=description;
}

public ItemDetailsBean getItemParent() {
    return itemParent;
}

public void setItemParent(ItemDetailsBean itemParent) {
    this.itemParent = itemParent;
}

private ItemDetailsBean itemParent=null;


public String getPurchaseDate()
{
return purchaseDate;
}
public void  setPurchaseDate(String purchaseDate)
{
this.purchaseDate=purchaseDate;

}

public static long getSerialVersionUID() {
return serialVersionUID;
}

public String  warrantyDateUtc=null;
public String  purchaseDateUtc=null;
public String  warrantyDate=null;
public String  purchaseDate=null;
public String getWarrantyDate()
{
return warrantyDate;
}
public void  setWarrantyDate(String warrantyDate)
{
this.warrantyDate=warrantyDate;
}
private String  itemEpcId=null;
public String getItemEpcId()
{
 return itemEpcId;
}
public void  setItemEpcId(String itemEpcId)
{
 this.itemEpcId=itemEpcId;
}
private String  itemBarCodeID=null;
public String getItemBarCodeID()
{
 return itemBarCodeID;
}
public void  setItemBarCodeID(String itemBarCodeID)
{
 this.itemBarCodeID=itemBarCodeID;
}
private String  remark=null;
public String getRemark()
{
 return remark;
}
public void  setRemark(String remark)
{
 this.remark=remark;
}
private Integer lastLocationID=0;
public Integer getLastLocationID()
{
 return lastLocationID;
}
public void  setLastLocationID(Integer lastLocationID)
{
 this.lastLocationID=lastLocationID;
}


private Integer currentLocationId=0;
public Integer getCurrentLocationId()
{
 return currentLocationId;
}
public void  setCurrentLocationId(Integer currentLocationId)
{
 this.currentLocationId=currentLocationId;
}
private String  status=null;
public String getStatus()
{
 return status;
}
public void  setStatus(String status)
{
 this.status=status;
}
private String  manufactuare=null;
public String getManufactuare()
{
 return manufactuare;
}
public void  setManufactuare(String manufactuare)
{
 this.manufactuare=manufactuare;
}
private String  price=null;
public String getPrice()
{
 return price;
}
public void  setPrice(String price)
{
 this.price=price;
}
private Integer createdBy=0;
public Integer getCreatedBy()
{
 return createdBy;
}
public void  setCreatedBy(Integer createdBy)
{
 this.createdBy=createdBy;
}
private String  createdOn=null;
public String getCreatedOn()
{
 return createdOn;
}
public void  setCreatedOn(String createdOn)
{
 this.createdOn=createdOn;

}
public String  createdOnUtc=null;
public String  modifyOnUtc=null;



private Integer modifyBy=0;
public Integer getModifyBy()
{
 return modifyBy;
}
public void  setModifyBy(Integer modifyBy)
{
 this.modifyBy=modifyBy;
}
private String  modifyOn=null;
public String getModifyOn()
{
 return modifyOn;
}
public void  setModifyOn(String modifyOn)
{
 this.modifyOn=modifyOn;

}
private String  newItemReg=null;

    private String  dateOfAssetDepositeUTC=null;
    private String  dateOfAssetCollectUTC=null;

    public String getWarrantyDateUtc() {
        return warrantyDateUtc;
    }

    public void setWarrantyDateUtc(String warrantyDateUtc) {
        this.warrantyDateUtc = warrantyDateUtc;
    }

    public String getPurchaseDateUtc() {
        return purchaseDateUtc;
    }

    public void setPurchaseDateUtc(String purchaseDateUtc) {
        this.purchaseDateUtc = purchaseDateUtc;
    }

    public String getCreatedOnUtc() {
        return createdOnUtc;
    }

    public void setCreatedOnUtc(String createdOnUtc) {
        this.createdOnUtc = createdOnUtc;
    }

    public String getModifyOnUtc() {
        return modifyOnUtc;
    }

    public void setModifyOnUtc(String modifyOnUtc) {
        this.modifyOnUtc = modifyOnUtc;
    }

    public String getDateOfAssetDepositeUTC() {
        return dateOfAssetDepositeUTC;
    }

    public void setDateOfAssetDepositeUTC(String dateOfAssetDepositeUTC) {
        this.dateOfAssetDepositeUTC = dateOfAssetDepositeUTC;
    }

    public String getDateOfAssetCollectUTC() {
        return dateOfAssetCollectUTC;
    }

    public void setDateOfAssetCollectUTC(String dateOfAssetCollectUTC) {
        this.dateOfAssetCollectUTC = dateOfAssetCollectUTC;
    }

    public String getSectionNumber() {
        return sectionNumber;
    }

    public void setSectionNumber(String sectionNumber) {
        this.sectionNumber = sectionNumber;
    }

    public String getNameOFAssetDepositorDate() {
        return nameOFAssetDepositorDate;
    }

    public void setNameOFAssetDepositorDate(String nameOFAssetDepositorDate) {
        this.nameOFAssetDepositorDate = nameOFAssetDepositorDate;
    }

    public String getComplaintBy() {
        return complaintBy;
    }

    public void setComplaintBy(String complaintBy) {
        this.complaintBy = complaintBy;
    }

    public String getCriminalName() {
        return criminalName;
    }

    public void setCriminalName(String criminalName) {
        this.criminalName = criminalName;
    }

    public String getAssetDescription() {
        return assetDescription;
    }

    public void setAssetDescription(String assetDescription) {
        this.assetDescription = assetDescription;
    }

    public String getCourtRemarkAssetPrezenty() {
        return courtRemarkAssetPrezenty;
    }

    public void setCourtRemarkAssetPrezenty(String courtRemarkAssetPrezenty) {
        this.courtRemarkAssetPrezenty = courtRemarkAssetPrezenty;
    }

    public String getCourtCommandNumberDate() {
        return courtCommandNumberDate;
    }

    public void setCourtCommandNumberDate(String courtCommandNumberDate) {
        this.courtCommandNumberDate = courtCommandNumberDate;
    }

    public String getAssetExit() {
        return assetExit;
    }

    public void setAssetExit(String assetExit) {
        this.assetExit = assetExit;
    }

    public String getNameOfAssetCollect() {
        return nameOfAssetCollect;
    }

    public void setNameOfAssetCollect(String nameOfAssetCollect) {
        this.nameOfAssetCollect = nameOfAssetCollect;
    }

    private String sectionNumber=null;
    private String  nameOFAssetDepositorDate=null;
    private String  complaintBy=null;
    private String  criminalName=null;
    private String  assetDescription=null;
    private String  courtRemarkAssetPrezenty=null;
    private String  courtCommandNumberDate=null;
    private String  assetExit=null;
    private String  nameOfAssetCollect=null;



    public  String getNewItemReg()
{
	return newItemReg;
}
public void setNewItemReg(String newItemReg)
{
	this.newItemReg=newItemReg;
}

	private String  active=null;
	public String getActive()
	{
		return active;
	}
	public void  setActive(String active)
	{
		this.active=active;
	}
public static List<ItemDetailsBean> prepareBean(org.json.simple.JSONArray jSONArray)
{

ArrayList<ItemDetailsBean>beans=null;
for(int i=0;jSONArray!=null && i<jSONArray.size();i++)
{

		if(i==0)
		{
		 beans=new ArrayList<ItemDetailsBean>();
		}

		org.json.simple.JSONObject json1=(org.json.simple.JSONObject)jSONArray.get(i);
	    beans.add(getBeanFromJson(json1));
}


return beans;
}

public static ItemDetailsBean getBeanFromJson(org.json.simple.JSONObject json1)
{

    System.out.println(json1.toJSONString());
    ItemDetailsBean bean=new ItemDetailsBean();
    if(json1.containsKey("itemId"))
    {
        bean.setItemId(Integer.valueOf(""+(java.lang.Long)json1.get("itemId")));
    }
    if(json1.containsKey("itemCode"))
    {
        bean.setItemCode((String)json1.get("itemCode"));
    }
    if(json1.containsKey("description"))
    {
        bean.setDescription((String)json1.get("description"));
    }
    if(json1.containsKey("itemParent"))
    {
        org.json.simple.JSONObject itemParent=(org.json.simple.JSONObject)json1.get("itemParent");
        if(itemParent!=null )
        {
            bean.setItemParent(getBeanFromJson(itemParent));
        }

    }
    if(json1.containsKey("purchaseDateUtc"))
    {
        bean.setPurchaseDate((String)json1.get("purchaseDateUtc"));
    }
    if(json1.containsKey("warrantyDateUtc"))
    {
        bean.setWarrantyDate((String)json1.get("warrantyDateUtc"));
    }
    if(json1.containsKey("itemEpcId"))
    {
        bean.setItemEpcId((String)json1.get("itemEpcId"));
    }
    if(json1.containsKey("itemBarCodeID"))
    {
        bean.setItemBarCodeID((String)json1.get("itemBarCodeID"));
    }
    if(json1.containsKey("remark"))
    {
        bean.setRemark((String)json1.get("remark"));
    }
    if(json1.containsKey("lastLocationID"))
    {
        bean.setLastLocationID(Integer.valueOf(""+(java.lang.Long)json1.get("lastLocationID")));
    }
    if(json1.containsKey("currentLocationId"))
    {
        bean.setCurrentLocationId(Integer.valueOf(""+(java.lang.Long)json1.get("currentLocationId")));
    }
    if(json1.containsKey("status"))
    {
        bean.setStatus((String)json1.get("status"));
    }
    if(json1.containsKey("manufactuare"))
    {
        bean.setManufactuare((String)json1.get("manufactuare"));
    }
    if(json1.containsKey("price"))
    {
        bean.setPrice((String)json1.get("price"));
    }
    if(json1.containsKey("createdBy"))
    {
        bean.setCreatedBy(Integer.valueOf(""+(java.lang.Long)json1.get("createdBy")));
    }
    if(json1.containsKey("createdOnUtc"))
    {
        bean.setCreatedOn((String)json1.get("createdOnUtc"));
       // System.out.println(bean.getItemCode()+"getCreatedOn dowload "+bean.getCreatedOn());
    }
    if(json1.containsKey("modifyOnUtc"))
    {
        bean.setModifyOn((String)json1.get("modifyOnUtc"));
       // System.out.println(bean.getItemCode()+"getModifyOn dowload "+bean.getModifyOn());
    }
    if(json1.containsKey("modifyBy"))
    {
        bean.setModifyBy(Integer.valueOf(""+(java.lang.Long)json1.get("modifyBy")));
    }

    if(json1.containsKey("active"))
    {
        bean.setActive(""+(java.lang.Boolean)json1.get("active"));
    }

    if(json1.containsKey("sectionNumber"))
    {
        bean.setSectionNumber(""+(java.lang.String)json1.get("sectionNumber"));
    }

    if(json1.containsKey("nameOFAssetDepositorDate"))
    {
        bean.setNameOFAssetDepositorDate(""+(java.lang.String)json1.get("nameOFAssetDepositorDate"));
    }

    if(json1.containsKey("complaintBy"))
    {
        bean.setComplaintBy(""+(java.lang.String)json1.get("complaintBy"));
    }


    if(json1.containsKey("criminalName"))
    {
        bean.setCriminalName(""+(java.lang.String)json1.get("criminalName"));
    }
    if(json1.containsKey("assetDescription"))
    {
        bean.setAssetDescription(""+(java.lang.String)json1.get("assetDescription"));
    }

    if(json1.containsKey("courtRemarkAssetPrezenty"))
    {
        bean.setCourtRemarkAssetPrezenty(""+(java.lang.String)json1.get("courtRemarkAssetPrezenty"));
    }

    if(json1.containsKey("courtCommandNumberDate"))
    {
        bean.setCourtCommandNumberDate(""+(java.lang.String)json1.get("courtCommandNumberDate"));
    }


    if(json1.containsKey("assetExit"))
    {
        bean.setAssetExit(""+(java.lang.String)json1.get("assetExit"));
    }

    if(json1.containsKey("nameOfAssetCollect"))
    {
        bean.setNameOfAssetCollect(""+(java.lang.String)json1.get("nameOfAssetCollect"));
    }

    if(json1.containsKey("dateOfAssetDepositeUTC"))
    {
        bean.setDateOfAssetDepositeUTC(""+(java.lang.String)json1.get("dateOfAssetDepositeUTC"));
    }

    if(json1.containsKey("dateOfAssetCollectUTC"))
    {
        bean.setDateOfAssetCollectUTC(""+(java.lang.String)json1.get("dateOfAssetCollectUTC"));
    }


    return bean;
}

}
