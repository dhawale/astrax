package com.hkcrfid.bean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
public class RoleTypeBean implements Serializable
{
private static final long serialVersionUID = -723583058586873479L;
private Integer roleId=0;
public Integer getRoleId()
{
 return roleId;
}
public void  setRoleId(Integer roleId)
{
 this.roleId=roleId;
}
private String  roleName=null;
public String getRoleName()
{
 return roleName;
}
public void  setRoleName(String roleName)
{
 this.roleName=roleName;
}
public String  createdOnUtc=null;
public String  modifyOnUtc=null;
private Integer createdBy=0;
public Integer getCreatedBy()
{
 return createdBy;
}
public void  setCreatedBy(Integer createdBy)
{
 this.createdBy=createdBy;
}
private String  createdOn=null;
public String getCreatedOn()
{
 return createdOn;
}
public void  setCreatedOn(String createdOn)
{
 this.createdOn=createdOn;

}
private Integer modifyBy=0;
public Integer getModifyBy()
{
 return modifyBy;
}
public void  setModifyBy(Integer modifyBy)
{
 this.modifyBy=modifyBy;
}
private String  modifyOn=null;
public String getModifyOn()
{
 return modifyOn;
}
public void  setModifyOn(String modifyOn)
{
 this.modifyOn=modifyOn;

}
private String  active=null;
public String getActive()
{
 return active;
}
public void  setActive(String active)
{
 this.active=active;
}

public static List<RoleTypeBean> prepareBean(org.json.simple.JSONArray jSONArray)
{
ArrayList<RoleTypeBean>beans=null;
for(int i=0;jSONArray!=null && i<jSONArray.size();i++)
{

		if(i==0)
		{
		 beans=new ArrayList<RoleTypeBean>();
		}

		RoleTypeBean bean=new RoleTypeBean();
		org.json.simple.JSONObject json1=(org.json.simple.JSONObject)jSONArray.get(i);
		if(json1.containsKey("roleId"))
		{
		 bean.setRoleId((int)json1.get("roleId"));
		}
		if(json1.containsKey("roleName"))
		{
		 bean.setRoleName((String)json1.get("roleName"));
		}
		if(json1.containsKey("createdBy"))
		{
		 bean.setCreatedBy((int)json1.get("createdBy"));
		}

		if(json1.containsKey("modifyBy"))
		{
		 bean.setModifyBy((int)json1.get("modifyBy"));
		}

		if(json1.containsKey("active"))
		{
		 bean.setActive((String)json1.get("active"));
		}

		if(json1.containsKey("createdOnUtc"))
		{
			bean.setCreatedOn((String)json1.get("createdOnUtc"));
		}
		if(json1.containsKey("modifyOnUtc"))
		{
			bean.setModifyOn((String)json1.get("modifyOnUtc"));
		}
	beans.add(bean);
}


return beans;
}

}
