package com.hkcrfid.bean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
public class TrackingSessionBean implements Serializable
{
private static final long serialVersionUID = -723583058586873479L;
private Integer trackingSessionId=0;
public Integer getTrackingSessionId()
{
 return trackingSessionId;
}
public void  setTrackingSessionId(Integer trackingSessionId)
{
 this.trackingSessionId=trackingSessionId;
}
private String  trackingSessionName=null;
public String getTrackingSessionName()
{
 return trackingSessionName;
}
public void  setTrackingSessionName(String trackingSessionName)
{
 this.trackingSessionName=trackingSessionName;
}
private String  startDate=null;
public String getStartDate()
{
 return startDate;
}
public void  setStartDate(String startDate)
{
this.startDate=startDate;

}
private String  endDate=null;
public String getEndDate()
{
return endDate;
}
public void  setEndDate(String endDate)
{
this.endDate=endDate;


}
private Integer createdBy=0;
public Integer getCreatedBy()
{
 return createdBy;
}
public void  setCreatedBy(Integer createdBy)
{
 this.createdBy=createdBy;
}
public String  createdOnUtc=null;
public String  modifyOnUtc=null;

private String  createdOn=null;
public String getCreatedOn()
{
 return createdOn;
}
public void  setCreatedOn(String createdOn)
{
 this.createdOn=createdOn;

}
private Integer modifyBy=0;
public Integer getModifyBy()
{
 return modifyBy;
}
public void  setModifyBy(Integer modifyBy)
{
 this.modifyBy=modifyBy;
}
private String  modifyOn=null;
public String getModifyOn()
{
 return modifyOn;
}
public void  setModifyOn(String modifyOn)
{
 this.modifyOn=modifyOn;

}
private String  active=null;
public String getActive()
{
 return active;
}
public String  startDateUtc=null;
public String   endDateUtc=null;

public void  setActive(String active)
{
 this.active=active;
}

public static List<TrackingSessionBean> prepareBean(org.json.simple.JSONArray jSONArray)
{
ArrayList<TrackingSessionBean>beans=null;
for(int i=0;jSONArray!=null && i<jSONArray.size();i++)
{

		if(i==0)
		{
		 beans=new ArrayList<TrackingSessionBean>();
		}

		TrackingSessionBean bean=new TrackingSessionBean();
		org.json.simple.JSONObject json1=(org.json.simple.JSONObject)jSONArray.get(i);
		if(json1.containsKey("trackingSessionId"))
		{
		 	bean.setTrackingSessionId(Integer.valueOf(""+(java.lang.Long)json1.get("trackingSessionId")));
		}
		if(json1.containsKey("trackingSessionName"))
		{
		 bean.setTrackingSessionName((String)json1.get("trackingSessionName"));
		}
		if(json1.containsKey("startDateUtc"))
		{
		 bean.setStartDate((String)json1.get("startDateUtc"));
		}
		if(json1.containsKey("endDateUtc"))
		{
		 bean.setEndDate((String)json1.get("endDateUtc"));
		}
		if(json1.containsKey("createdBy"))
		{
			bean.setCreatedBy(Integer.valueOf(""+(java.lang.Long)json1.get("createdBy")));
		}

		if(json1.containsKey("modifyBy"))
		{
		 bean.setModifyBy(Integer.valueOf(""+(java.lang.Long)json1.get("modifyBy")));
		}

		if(json1.containsKey("createdOnUtc"))
		{
			bean.setCreatedOn((String)json1.get("createdOnUtc"));
		}
		if(json1.containsKey("modifyOnUtc"))
		{
			bean.setModifyOn((String)json1.get("modifyOnUtc"));
		}

		if(json1.containsKey("active"))
		{
			bean.setActive(""+(java.lang.Boolean)json1.get("active"));
		}
		beans.add(bean);
}


return beans;
}

}
