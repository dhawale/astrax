package com.hkcrfid.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TrackingInventoryBean implements Serializable {
    private static final long serialVersionUID = -723583058586873479L;
    public String createdOnUtc = null;
    private Integer trackingInventoryId = 0;
    private String itemEpcId = null;
    private String itemBarCodeID = null;
    private Integer trackingSessionId = 0;
    private Integer itemLocationId = 0;
    private Integer deviceId = 0;
    private String status = null;
    private Integer createdBy = 0;
    private String createdOn = null;
    private String deviceSerialNo = null;
    private Integer modifyBy = 0;
    private String modifyOn = null;
    private String active = null;
    private int itemId=0;

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getItemId() {
        return itemId;
    }

    public static List<TrackingInventoryBean> prepareBean(org.json.simple.JSONArray jSONArray) {
        ArrayList<TrackingInventoryBean> beans = null;
        for (int i = 0; jSONArray != null && i < jSONArray.size(); i++) {

            if (i == 0) {
                beans = new ArrayList<TrackingInventoryBean>();
            }

            TrackingInventoryBean bean = new TrackingInventoryBean();
            org.json.simple.JSONObject json1 = (org.json.simple.JSONObject) jSONArray.get(i);
            if (json1.containsKey("trackingInventoryId")) {
                bean.setTrackingInventoryId((int) json1.get("trackingInventoryId"));
            }
            if (json1.containsKey("trackingSessionId")) {
                bean.setTrackingSessionId((int) json1.get("trackingSessionId"));
            }
            if (json1.containsKey("itemLocationId")) {
                bean.setItemLocationId((int) json1.get("itemLocationId"));
            }
            if (json1.containsKey("deviceId")) {
                bean.setDeviceId((int) json1.get("deviceId"));
            }
            if (json1.containsKey("status")) {
                bean.setStatus((String) json1.get("status"));
            }
            if (json1.containsKey("createdBy")) {
                bean.setCreatedBy((int) json1.get("createdBy"));
            }

            if (json1.containsKey("modifyBy")) {
                bean.setModifyBy((int) json1.get("modifyBy"));
            }

            if (json1.containsKey("createdOnUtc")) {
                bean.setCreatedOn((String) json1.get("createdOnUtc"));
            }
            if (json1.containsKey("modifyOnUtc")) {
                bean.setModifyOn((String) json1.get("modifyOnUtc"));
            }

            if (json1.containsKey("active")) {
                bean.setActive("" + json1.get("active"));
            }
            if (json1.containsKey("deviceSerialNo")) {
                bean.setDeviceSerialNo((String) json1.get("deviceSerialNo"));
            }

            if (json1.containsKey("itemEpcId")) {
                bean.setItemEpcId((String) json1.get("itemEpcId"));
            }
            if (json1.containsKey("itemBarCodeID")) {
                bean.setItemBarCodeID((String) json1.get("itemBarCodeID"));
            }

            beans.add(bean);
        }


        return beans;
    }

    public Integer getTrackingInventoryId() {
        return trackingInventoryId;
    }

    public void setTrackingInventoryId(Integer trackingInventoryId) {
        this.trackingInventoryId = trackingInventoryId;
    }

    public String getItemEpcId() {
        return itemEpcId;
    }

    public void setItemEpcId(String itemEpcId) {
        this.itemEpcId = itemEpcId;
    }

    public String getItemBarCodeID() {
        return itemBarCodeID;
    }

    public void setItemBarCodeID(String itemBarCodeID) {
        this.itemBarCodeID = itemBarCodeID;
    }

    public Integer getTrackingSessionId() {
        return trackingSessionId;
    }

    public void setTrackingSessionId(Integer trackingSessionId) {
        this.trackingSessionId = trackingSessionId;
    }

    public Integer getItemLocationId() {
        return itemLocationId;
    }

    public void setItemLocationId(Integer itemLocationId) {
        this.itemLocationId = itemLocationId;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getDeviceSerialNo() {
        return deviceSerialNo;
    }

    public void setDeviceSerialNo(String deviceSerialNo) {
        this.deviceSerialNo = deviceSerialNo;
    }

    public Integer getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(Integer modifyBy) {
        this.modifyBy = modifyBy;
    }

    public String getModifyOn() {
        return modifyOn;
    }

    public void setModifyOn(String modifyOn) {
        this.modifyOn = modifyOn;

    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

}
