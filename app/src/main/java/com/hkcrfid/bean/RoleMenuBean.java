package com.hkcrfid.bean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
public class RoleMenuBean implements Serializable
{
private static final long serialVersionUID = -723583058586873479L;
private Integer menuID=0;
public Integer getMenuID()
{
 return menuID;
}
public void  setMenuID(Integer menuID)
{
 this.menuID=menuID;
}
private String  menuName=null;
public String getMenuName()
{
 return menuName;
}
public void  setMenuName(String menuName)
{
 this.menuName=menuName;
}
private Integer menuParentID=0;
public Integer getMenuParentID()
{
 return menuParentID;
}
public void  setMenuParentID(Integer menuParentID)
{
 this.menuParentID=menuParentID;
}
private Integer roleId=0;
public Integer getRoleId()
{
 return roleId;
}
public void  setRoleId(Integer roleId)
{
 this.roleId=roleId;
}

public String  createdOnUtc=null;
public String  modifyOnUtc=null;

private String  active=null;
public String getActive()
{
 return active;
}
public void  setActive(String active)
{
 this.active=active;
}
private String  insertRecord=null;
public String getInsertRecord()
{
 return insertRecord;
}
public void  setInsertRecord(String insertRecord)
{
 this.insertRecord=insertRecord;
}
private String  updateRecord=null;
public String getUpdateRecord()
{
 return updateRecord;
}
public void  setUpdateRecord(String updateRecord)
{
 this.updateRecord=updateRecord;
}
private String  disableRecord=null;
public String getDisableRecord()
{
 return disableRecord;
}
public void  setDisableRecord(String disableRecord)
{
 this.disableRecord=disableRecord;
}
private String  displayRecord=null;
public String getDisplayRecord()
{
 return displayRecord;
}
public void  setDisplayRecord(String displayRecord)
{
 this.displayRecord=displayRecord;
}
private Integer createdBy=0;
public Integer getCreatedBy()
{
 return createdBy;
}
public void  setCreatedBy(Integer createdBy)
{
 this.createdBy=createdBy;
}
private String  createdOn=null;
public String getCreatedOn()
{
 return createdOn;
}
public void  setCreatedOn(String createdOn)
{
 this.createdOn=createdOn;

}
private Integer modifyBy=0;
public Integer getModifyBy()
{
 return modifyBy;
}
public void  setModifyBy(Integer modifyBy)
{
 this.modifyBy=modifyBy;
}
private String  modifyOn=null;
public String getModifyOn()
{
 return modifyOn;
}
public void  setModifyOn(String modifyOn)
{
 this.modifyOn=modifyOn;

}

public static List<RoleMenuBean> prepareBean(org.json.simple.JSONArray jSONArray)
{
ArrayList<RoleMenuBean>beans=null;
for(int i=0;jSONArray!=null && i<jSONArray.size();i++)
{

		if(i==0)
		{
		 beans=new ArrayList<RoleMenuBean>();
		}

		RoleMenuBean bean=new RoleMenuBean();
		org.json.simple.JSONObject json1=(org.json.simple.JSONObject)jSONArray.get(i);
		if(json1.containsKey("menuID"))
		{
		 bean.setMenuID((int)json1.get("menuID"));
		}
		if(json1.containsKey("menuName"))
		{
		 bean.setMenuName((String)json1.get("menuName"));
		}
		if(json1.containsKey("menuParentID"))
		{
		 bean.setMenuParentID((int)json1.get("menuParentID"));
		}
		if(json1.containsKey("roleId"))
		{
		 bean.setRoleId((int)json1.get("roleId"));
		}
		if(json1.containsKey("active"))
		{
		 bean.setActive((String)json1.get("active"));
		}
		if(json1.containsKey("insertRecord"))
		{
		 bean.setInsertRecord((String)json1.get("insertRecord"));
		}
		if(json1.containsKey("updateRecord"))
		{
		 bean.setUpdateRecord((String)json1.get("updateRecord"));
		}
		if(json1.containsKey("disableRecord"))
		{
		 bean.setDisableRecord((String)json1.get("disableRecord"));
		}
		if(json1.containsKey("displayRecord"))
		{
		 bean.setDisplayRecord((String)json1.get("displayRecord"));
		}
		if(json1.containsKey("createdBy"))
		{
		 bean.setCreatedBy((int)json1.get("createdBy"));
		}

		if(json1.containsKey("modifyBy"))
		{
		 bean.setModifyBy((int)json1.get("modifyBy"));
		}

		if(json1.containsKey("createdOnUtc"))
		{
			bean.setCreatedOn((String)json1.get("createdOnUtc"));
		}
		if(json1.containsKey("modifyOnUtc"))
		{
			bean.setModifyOn((String)json1.get("modifyOnUtc"));
		}
beans.add(bean);
}


return beans;
}

}
