package com.hkcrfid.bean;
import java.io.Serializable;

public class DemoWS implements Serializable
{
    private static final long serialVersionUID = -723583058586873479L;
    private String firstName=null;
    private String lastName=null;
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}

