package com.hkcrfid.bean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
public class ItemLocationBean implements Serializable
{
private static final long serialVersionUID = -723583058586873479L;
private Integer itemLocationId=0;
public Integer getItemLocationId()
{
 return itemLocationId;
}
public void  setItemLocationId(Integer itemLocationId)
{
 this.itemLocationId=itemLocationId;
}
private String  itemLocationName=null;
public String getItemLocationName()
{
 return itemLocationName;
}
public void  setItemLocationName(String itemLocationName)
{
 this.itemLocationName=itemLocationName;
}

private ItemLocationBean itemLocationParent=null;
public ItemLocationBean getItemLocationParent() {
	return itemLocationParent;
}
public void setItemLocationParent(ItemLocationBean itemLocationParent) {
	this.itemLocationParent = itemLocationParent;
}
private String  itemLocationEpcId=null;
public String getItemLocationEpcId()
{
 return itemLocationEpcId;
}
public void  setItemLocationEpcId(String itemLocationEpcId)
{
 this.itemLocationEpcId=itemLocationEpcId;
}
private String  itemLocationBarCodeID=null;
public String getItemLocationBarCodeID()
{
 return itemLocationBarCodeID;
}
public void  setItemLocationBarCodeID(String itemLocationBarCodeID)
{
 this.itemLocationBarCodeID=itemLocationBarCodeID;
}

public String  createdOnUtc=null;
public String  modifyOnUtc=null;

private Integer createdBy=0;
public Integer getCreatedBy()
{
 return createdBy;
}
public void  setCreatedBy(Integer createdBy)
{
 this.createdBy=createdBy;
}
private String  createdOn=null;
public String getCreatedOn()
{
 return createdOn;
}
public void  setCreatedOn(String createdOn)
{
 this.createdOn=createdOn;

}
private Integer modifyBy=0;
public Integer getModifyBy()
{
 return modifyBy;
}
public void  setModifyBy(Integer modifyBy)
{
 this.modifyBy=modifyBy;
}
private String  modifyOn=null;
public String getModifyOn()
{
 return modifyOn;
}
public void  setModifyOn(String modifyOn)
{
 this.modifyOn=modifyOn;

}
private String  active=null;
public String getActive()
{
 return active;
}
public void  setActive(String active)
{
 this.active=active;
}

	private String  newItemLocationReg=null;
	public  String getNewItemLocationReg()
	{
		return newItemLocationReg;
	}
	public void setNewItemLocationReg(String newItemLocationReg)
	{
		this.newItemLocationReg=newItemLocationReg;
	}

public static List<ItemLocationBean> prepareBean(org.json.simple.JSONArray jSONArray)
{
ArrayList<ItemLocationBean>beans=null;
for(int i=0;jSONArray!=null && i<jSONArray.size();i++)
{

		if(i==0)
		{
		 beans=new ArrayList<ItemLocationBean>();
		}

		org.json.simple.JSONObject json1=(org.json.simple.JSONObject)jSONArray.get(i);

		beans.add(getBeanFromJson(json1));
}


return beans;
}

public static ItemLocationBean getBeanFromJson(org.json.simple.JSONObject json1)
{
	ItemLocationBean bean=new ItemLocationBean();
	if(json1.containsKey("itemLocationId"))
	{
		bean.setItemLocationId(Integer.valueOf(""+(java.lang.Long)json1.get("itemLocationId")));
	}
	if(json1.containsKey("itemLocationName"))
	{
		bean.setItemLocationName((String)json1.get("itemLocationName"));
	}

	if(json1.containsKey("itemLocationParent"))
	{

		org.json.simple.JSONObject itemLocationParent=(org.json.simple.JSONObject)json1.get("itemLocationParent");
		if(itemLocationParent!=null)
		{
			ItemLocationBean itemLocationBeanParent=getBeanFromJson(itemLocationParent);
			bean.setItemLocationParent(itemLocationBeanParent);
		}
	}

	if(json1.containsKey("itemLocationEpcId"))
	{
		bean.setItemLocationEpcId((String)json1.get("itemLocationEpcId"));
	}
	if(json1.containsKey("itemLocationBarCodeID"))
	{
		bean.setItemLocationBarCodeID((String)json1.get("itemLocationBarCodeID"));
	}
	if(json1.containsKey("createdBy"))
	{
		bean.setCreatedBy(Integer.valueOf(""+(java.lang.Long)json1.get("createdBy")));
	}
	if(json1.containsKey("createdOnUtc"))
	{
		bean.setCreatedOn((String)json1.get("createdOnUtc"));
	}
	if(json1.containsKey("modifyOnUtc"))
	{
		bean.setModifyOn((String)json1.get("modifyOnUtc"));
	}
	if(json1.containsKey("modifyBy"))
	{
		bean.setModifyBy(Integer.valueOf(""+(java.lang.Long)json1.get("modifyBy")));
	}

	if(json1.containsKey("active"))
	{
		bean.setActive(""+(java.lang.Boolean)json1.get("active"));
	}
	return bean;
}

}
